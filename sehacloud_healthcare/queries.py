from __future__ import unicode_literals
import frappe
from frappe.desk.reportview import get_match_cond
from frappe.desk.reportview import build_match_conditions, get_filters_cond

def get_practitioner_appointment_type(doctype, txt, searchfield, start, page_len, filters):
	parent = filters['parent']
	if parent and frappe.db.get_value("Healthcare Practitioner", parent, "healthcare_practitioner_type") == "External":
		query = """select name from `tabAppointment Type` where appointment_type like %(txt)s order by name"""
	else:
		query = """select appointment_type from `tabService Profile Appointment Type` where parent = '{parent}'
		and appointment_type like %(txt)s order by name"""

	#return frappe.db.sql(query.format(parent=parent, searchfield=searchfield))
	return frappe.db.sql(query.format(**{
		"parent": parent,
		"mcond": get_match_cond(doctype)
	}), {
		'txt': "%%%s%%" % txt,
		'_txt': txt.replace("%", ""),
		'start': start,
		'page_len': page_len
	})

def get_mode_of_payment(doctype, txt, searchfield, start, page_len, filters):
	company = filters['company']
	user = filters['user']
	# query = """select stage from `tabMode of Payment` where parent = '{parent}'
	# and stage like %(txt)s order by name"""
	from erpnext.stock.get_item_details import get_pos_profile
	pos_profile = get_pos_profile(company, user=user)
	if pos_profile:
		query = """select mode_of_payment from `tabSales Invoice Payment` where parent = '{parent}' order by name"""

		return frappe.db.sql(query.format(**{
			"parent": pos_profile.name,
			"mcond": get_match_cond(doctype)
		}), {
			'txt': "%%%s%%" % txt,
			'_txt': txt.replace("%", ""),
			'start': start,
			'page_len': page_len
		})
	else:
		return frappe.db.sql("select name from `tabMode of Payment`")

def get_practitioner_procedure_template(doctype, txt, searchfield, start, page_len, filters):
	parent = filters['parent']
	query = """
		select
			distinct t.name, t.abbr, t.template, t.medical_code
		from
			`tabService Profile Clinical Procedures` sp, `tabClinical Procedure Template` t
		where
			sp.parent = '{parent}' and sp.clinical_procedure_template = t.name
			and ( t.abbr like %(txt)s or t.name like %(txt)s or t.template like %(txt)s or t.medical_code like %(txt)s)
	"""

	# return frappe.db.sql(query.format(parent=parent, searchfield=searchfield))
	return frappe.db.sql(query.format(**{
		"parent": parent,
		"mcond": get_match_cond(doctype)
	}), {
		'txt': "%%%s%%" % txt,
		'_txt': txt.replace("%", ""),
		'start': start,
		'page_len': page_len
	})

def get_uom_have_conversion_factor(doctype, txt, searchfield, start, page_len, filters):
	item_code = filters['item_code']
	query = """
		select
			uom, conversion_factor
		from
			`tabUOM Conversion Detail`
		where
			parent = '{item_code}' and uom like %(txt)s order by name
	"""
	return frappe.db.sql(query.format(**{
		"item_code": item_code,
		"mcond": get_match_cond(doctype)
	}), {
		'txt': "%%%s%%" % txt,
		'_txt': txt.replace("%", ""),
		'start': start,
		'page_len': page_len
	})

def get_practitioner_list(doctype, txt, searchfield, start, page_len, filters=None):
	fields = ["name", "practitioner_name", "mobile_phone"]
	match_conditions = build_match_conditions("Healthcare Practitioner")
	match_conditions = "and {}".format(match_conditions) if match_conditions else ""

	if filters:
		filter_conditions = get_filters_cond(doctype, filters, [])
		match_conditions += "{}".format(filter_conditions)

	query = """select %s from `tabHealthcare Practitioner` where docstatus < 2
		and (%s like %s or first_name like %s"""

	if frappe.db.has_column("Healthcare Practitioner", "full_name_arabic"):
		query += """
			or full_name_arabic like %s
		"""

	query += """)"""

	query += """and active = 1
		{match_conditions}
		order by
		case when name like %s then 0 else 1 end,
		case when first_name like %s then 0 else 1 end,
		name, first_name limit %s, %s"""

	if frappe.db.has_column("Healthcare Practitioner", "full_name_arabic"):
		return frappe.db.sql(query.format(
				match_conditions=match_conditions) %
				(
					", ".join(fields),
					frappe.db.escape(searchfield),
					"%s", "%s", "%s", "%s", "%s", "%s", "%s"
				),
				(
					"%%%s%%" % frappe.db.escape(txt),
					"%%%s%%" % frappe.db.escape(txt),
					"%%%s%%" % frappe.db.escape(txt),
					"%%%s%%" % frappe.db.escape(txt),
					"%%%s%%" % frappe.db.escape(txt),
					start,
					page_len
				)
			)
	else:
		return frappe.db.sql(query.format(
				match_conditions=match_conditions) %
				(
					", ".join(fields),
					frappe.db.escape(searchfield),
					"%s", "%s", "%s", "%s", "%s", "%s"
				),
				(
					"%%%s%%" % frappe.db.escape(txt),
					"%%%s%%" % frappe.db.escape(txt),
					"%%%s%%" % frappe.db.escape(txt),
					"%%%s%%" % frappe.db.escape(txt),
					start,
					page_len
				)
			)

def get_package_item_list(doctype, txt, searchfield, start, page_len, filters):
	if filters:
		package = filters['parent']
		query = """select item_code from `tabHealthcare Package Item` where parent = '{parent}'
		and item_code like %(txt)s order by name"""

		#return frappe.db.sql(query.format(parent=parent, searchfield=searchfield))
		return frappe.db.sql(query.format(**{
			"parent": package,
			"mcond": get_match_cond(doctype)
		}), {
			'txt': "%%%s%%" % txt,
			'_txt': txt.replace("%", ""),
			'start': start,
			'page_len': page_len
		})
	return {}

def get_item_list(doctype, txt, searchfield, start, page_len, filters, as_dict=False):
	conditions = []
	from frappe.utils import nowdate
	description_cond = ''
	if frappe.db.count('Item', cache=True) < 50000:
		# scan description only if items are less than 50000
		description_cond = 'or tabItem.description LIKE %(txt)s'

	item_query = """select tabItem.name,
		if(length(tabItem.item_name) > 40,
			concat(substr(tabItem.item_name, 1, 40), "..."), item_name) as item_name,
		tabItem.item_group,
		if(length(tabItem.description) > 40, \
			concat(substr(tabItem.description, 1, 40), "..."), description) as decription,
		tabItem.manufacturer_code
		from tabItem
		where tabItem.docstatus < 2
			and tabItem.has_variants=0
			and tabItem.disabled=0
			and (tabItem.end_of_life > %(today)s or ifnull(tabItem.end_of_life, '0000-00-00')='0000-00-00')
			and (tabItem.`{key}` LIKE %(txt)s
				or tabItem.item_code LIKE %(txt)s
				or tabItem.item_group LIKE %(txt)s
				or tabItem.item_name LIKE %(txt)s"""

	if frappe.db.has_column("Item", "manufacturer_code"):
		item_query += """or tabItem.manufacturer_code LIKE %(txt)s"""

	item_query += """or tabItem.item_code IN (select parent from `tabItem Barcode` where barcode LIKE %(txt)s)
				{description_cond})
			{fcond} {mcond}
		order by
			if(locate(%(_txt)s, name), locate(%(_txt)s, name), 99999),
			if(locate(%(_txt)s, item_name), locate(%(_txt)s, item_name), 99999),
			idx desc,
			name, item_name
		limit %(start)s, %(page_len)s """

	return frappe.db.sql(item_query.format(
			key=searchfield,
			fcond=get_filters_cond(doctype, filters, conditions).replace('%', '%%'),
			mcond=get_match_cond(doctype).replace('%', '%%'),
			description_cond = description_cond),
			{
				"today": nowdate(),
				"txt": "%%%s%%" % txt,
				"_txt": txt.replace("%", ""),
				"start": start,
				"page_len": page_len
			}, as_dict=as_dict)
