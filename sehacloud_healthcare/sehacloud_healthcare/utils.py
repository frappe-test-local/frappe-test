# -*- coding: utf-8 -*-
# Copyright (c) 2018, SehaCloud and contributors
# For license information, please see license.txt

import frappe
import json
import math
from frappe.utils import getdate, nowtime, time_diff_in_seconds, datetime, get_time, date_diff, add_days, get_datetime, formatdate, to_timedelta, time_diff_in_hours
from frappe.utils.background_jobs import enqueue
from sehacloud_healthcare.sehacloud_healthcare.healthcare_service_to_invoice import manage_package_session
from erpnext.healthcare.utils import get_sales_invoice_for_healthcare_doc, sales_item_details_for_healthcare_doc, \
	get_procedure_delivery_item, item_reduce_procedure_rate, check_insurance_validity_on_item

def manage_sales_invoice_submit_cancel(doc, method):
	# manage_revenue_sharing(doc, method)
	manage_package_session(doc, method)

def manage_revenue_sharing(doc, method):
	if method == "on_submit":
		allocations = {}
		revenue_allocation = None
		#CREATE ALLOCATIONS
		for item in doc.items:
			if item.reference_dt and item.reference_dn and item.reference_dt != "Delivery Note" and not item.delivery_note:
				ref_doc = frappe.get_doc(item.reference_dt, item.reference_dn)
				item_group = item.item_group
				source = False
				practitioner = False
				if "Prescription" in item.reference_dt:
					source, practitioner = frappe.db.get_value("Patient Encounter",
						ref_doc.parent,	["source", "practitioner"])
				elif "Inpatient Occupancy" in item.reference_dt:
					source, practitioner = frappe.db.get_value("Inpatient Record",
						ref_doc.parent,	["source", "primary_practitioner"])
				else:
					practitioner = ref_doc.practitioner
					source = ref_doc.source
				if not source:
					continue
				#if doc.ref_practitioner:
				#	source = "Referral"
				#	practitioner = doc.ref_practitioner
				#else:
				#	continue

				#TODO Extract Method to create revenue allocation
				if source == "External Referral":
					for dist in doc.practitioner_revenue_distributions:
						if dist.reference_dt == item.reference_dt and dist.reference_dn == item.reference_dn and \
							dist.item_code == item.item_code and item.amount > 0 and dist.amount > 0:
								revenue_allocation = None
								#TODO Enbale Mode of Sharing
								percentage = dist.amount / item.amount * 100
								if percentage > 0:
									if not revenue_allocation or revenue_allocation.practitioner != dist.practitioner:
										revenue_allocation = get_revenue_allocation(allocations, dist.practitioner)
									revenue_allocation.append("revenue_items", {
										"item": item.item_code,
										"reference_dt": item.reference_dt,
										"reference_dn": item.reference_dn,
										"invoice_amount": item.amount,
										"percentage": percentage,
										"amount": item.amount * percentage * .01
									})
									allocations[dist.practitioner] = revenue_allocation
				else:
					if not practitioner or not source or not item_group:
						continue
					percentage = get_revenue_percentage(source, practitioner, item_group)
					if percentage > 0:
						if not revenue_allocation or revenue_allocation.practitioner != practitioner:
							revenue_allocation = get_revenue_allocation(allocations, practitioner)
						revenue_allocation.append("revenue_items", {
							"item": item.item_code,
							"reference_dt": item.reference_dt,
							"reference_dn": item.reference_dn,
							"invoice_amount": item.amount,
							"percentage": percentage,
							"amount": item.amount * percentage * .01
						})
						allocations[practitioner] = revenue_allocation

		for practitioner in allocations:
			allocation = allocations[practitioner]
			entry = None
			if not allocation.revenue_items:
				continue
			# CREATE ADDITIONAL SLAARY OR PURCHASE INVOICE FOR EACH PRACTITIONER
			else:
				ptype, emp, sup = frappe.db.get_value("Healthcare Practitioner",
					practitioner,	["healthcare_practitioner_type", "employee", "supplier"])

				total_amount = get_total_revenue_share(allocation)
				if total_amount:
					if ptype == "External":
						inv_item = frappe.db.get_value("Healthcare Settings",
							None, "revenue_booking_item")

						if not sup or not inv_item:
							frappe.throw("Missing required configurations to process revenue allocation, Supplier / Revenue Booking Item")
						entry = create_purchase_invoice(doc, sup, inv_item, total_amount)
					else:
						sc = frappe.db.get_value("Healthcare Settings", None,
							"revenue_booking_compnent")

						if not emp or not sc:
							frappe.throw("Missing required configurations to process revenue allocation, Employee / Salary Component")
						entry = create_additional_salary(doc.company, emp, sc, total_amount)
					if entry:
						allocation.sales_invoice = doc.name
						allocation.patient = doc.patient
						allocation.allocation_date = getdate()
						allocation.allocation_time = nowtime()
						allocation.reference_dt = entry.doctype
						allocation.reference_dn = entry.name
						allocation.total_amount = total_amount
						allocation.insert(ignore_permissions=1)
						if allocation.practitioner == practitioner:
							allocation.status = "Completed"
							allocation.submit()
						else:
							# mismatch in data! alert user role
							allocation.status = "Pending"
							allocation.save()
	elif method == "on_cancel":
		# cancel related Additional Salary or Purchase Invoice
		allocations = frappe.get_all("Healthcare Service Revenue Allocation",
			{"sales_invoice": doc.name}, ["name", "reference_dt", "reference_dn"])
		if allocations:
			for alloc in allocations:
				try:
					frappe.get_doc("Healthcare Service Revenue Allocation", alloc.name).cancel()
					frappe.get_doc(alloc.reference_dt, alloc.reference_dn).cancel()
				except Exception:
					# log error and warn
					frappe.throw("Did not Cancel, failed to process revenue allocation lnked to this Invoice")

def get_total_revenue_share(alloc):
	amount = 0
	for item in alloc.revenue_items:
		amount += item.amount
	return amount

def get_revenue_allocation(allocations, practitioner):
	if allocations.get(practitioner) and allocations[practitioner]:
		return allocations[practitioner]
	else:
		revenue_alloc = frappe.new_doc("Healthcare Service Revenue Allocation")
		revenue_alloc.practitioner = practitioner
		return revenue_alloc

def get_revenue_percentage(source, practitioner, item_group):
	percentage = 0
	if source == "Direct":
		percentage = frappe.db.get_value("Service Profile Item Groups",
			{
				"parenttype": "Healthcare Practitioner",
				"parent": practitioner,
				"item_group": item_group
			}, "direct_percentage")
	elif source == "Referral":
		percentage = frappe.db.get_value("Service Profile Item Groups",
			{
				"parenttype": "Healthcare Practitioner",
				"parent": practitioner,
				"item_group": item_group
			}, "referral_percentage")
	elif source == "External Referral":
		pass

	if percentage:
		return percentage
	else:
		return 0

def create_purchase_invoice(doc, supplier, item, amount):
	item_name, item_uom = frappe.db.get_value("Item", {"name": item},
		["item_name", "stock_uom"])
	pi = frappe.new_doc("Purchase Invoice")
	pi.posting_date = getdate()
	pi.supplier = supplier
	pi.company = doc.company
	pi.sch_sales_invoice=doc.name
	pi.update_stock = 0
	pi.is_paid = 0
	pi.append("items", {
			"item_code": item,
			"item_name": item_name,
			"qty": 1,
			"stock_uom": item_uom,
			"rate": amount
		})
	default_taxes_and_charge = frappe.db.exists(
		"Purchase Taxes and Charges Template",
		{
			"is_default": True,
			'company': doc.company
		}
	)
	if default_taxes_and_charge:
		pi.taxes_and_charges = default_taxes_and_charge
		taxes_and_charges = frappe.get_doc("Purchase Taxes and Charges Template", pi.taxes_and_charges)
		for tax in taxes_and_charges.taxes:
			tax_item = pi.append('taxes')
			tax_item.category = tax.category
			tax_item.add_deduct_tax = tax.add_deduct_tax
			tax_item.charge_type = tax.charge_type
			tax_item.included_in_print_rate = tax.included_in_print_rate
			tax_item.rate = tax.rate
			tax_item.account_head = tax.account_head
			tax_item.cost_center = tax.cost_center
			tax_item.description = tax.description
	pi.set_missing_values(True)
	pi.save(ignore_permissions=True)
	pi.submit()
	return pi

def create_additional_salary(company, employee, component, amount):
	additional_salary = frappe.get_doc({
		"doctype": "Additional Salary",
		"employee": employee,
		"payroll_date": getdate(),
		"amount": amount,
		"salary_component": component,
		"company": company
	}).insert(ignore_permissions=1)
	additional_salary.submit()
	return additional_salary

@frappe.whitelist()
def get_events(start, end, filters=None):

	"""Returns events for Gantt / Calendar view rendering.

	:param start: Start date-time.
	:param end: End date-time.
	:param filters: Filters (JSON).
	"""
	from six import string_types
	if isinstance(filters, string_types):
		filters = json.loads(filters)

	if not filters['practitioner']:
		return

	extend_query = False
	if filters['service_unit']:
		allow_overlap = frappe.get_value('Healthcare Service Unit', filters['service_unit'], 'overlap_appointments')
		if not allow_overlap:
			extend_query = """
				and service_unit = %(service_unit)s
			"""
	appointment_query = """
	select
		pa.name, pa.patient, pa.source, pa.referring_practitioner, pa.practitioner, pa.appointment_type,
        pa.status, pa.modality, pa.service_unit, pa.patient_name, pa.patient_sex, pa.patient_age, pa.insurance,
        pa.insurance_company_name, pa.duration, timestamp(pa.appointment_date, pa.appointment_time) as 'start',
        at.color, hsu.overlap_appointments
    from
		`tabPatient Appointment` pa left join
        `tabAppointment Type` at on pa.appointment_type=at.name left join
        `tabHealthcare Service Unit` hsu on pa.service_unit=hsu.name
    where
		(pa.appointment_date between %(start)s and %(end)s)
        and pa.status != 'Cancelled' and
        pa.docstatus < 2 and practitioner = %(practitioner)s order by start"""

	if filters['service_unit'] and extend_query:
		appointment_query += extend_query

	appointments = frappe.db.sql(appointment_query.format(),
		{"start": getdate(start), "end": getdate(end), "practitioner": filters['practitioner'], "service_unit": filters['service_unit']}, as_dict=True, update={"allDay": 0})

	for item in appointments:
		item.end = item.start + datetime.timedelta(minutes = item.duration)

	# Absent events
	absent_events = frappe.db.sql("""
		select
			name,healthcare_practitioner_name, event, from_time, to_time, from_date, to_date, duration, service_unit, service_unit, repeat_this_event, repeat_on, repeat_till,
			monday, tuesday, wednesday, thursday, friday, saturday, sunday, color
		from
			`tabPractitioner Event`
		where
			practitioner = %(practitioner)s and present != 1 and
			(
				(repeat_this_event = 1 and (from_date<=%(end)s and ifnull(repeat_till, "3000-01-01")>=%(start)s))
				or
				(repeat_this_event != 1 and (from_date<=%(end)s and to_date>=%(start)s))
			)
	""".format(),{"start":getdate(start), "end":getdate(end), "practitioner":filters['practitioner']}, as_dict=True)

	event_query = """
		select
			name, healthcare_practitioner_name, event, from_time, to_time, from_date, to_date, duration, service_unit, repeat_this_event, repeat_on,
			repeat_till, practitioner, appointment_type, color,
			monday, tuesday, wednesday, thursday, friday, saturday, sunday
		from
			`tabPractitioner Event`
		where
			present = 1 and
			(
				(repeat_this_event = 1 and (from_date<=%(end)s and ifnull(repeat_till, "3000-01-01")>=%(start)s))
				or
				(repeat_this_event != 1 and (from_date<=%(end)s and to_date>=%(start)s))
			)
			and practitioner = %(practitioner)s
	"""

	if filters['service_unit']:
		event_query += """
			and service_unit = %(service_unit)s
		"""

	present_events = frappe.db.sql(event_query.format(),{"start":getdate(start), "end":getdate(end),
	"practitioner":filters['practitioner'], "service_unit": filters['service_unit']}, as_dict=True)
	data = []
	length_bw_start_end = date_diff(end, start)+1
	events_list = []
	absent_events_list = []
	if absent_events:
		for i in range(0, length_bw_start_end):
			add_events = get_events_by_repeat_on(absent_events, add_days(start, i))
			if add_events:
				absent_events_list.append(add_events)
	if present_events:
		for i in range(0, length_bw_start_end):
			add_events = get_events_by_repeat_on(present_events, add_days(start, i))
			if add_events:
				events_list.append(add_events)

	if events_list and len(events_list) > 0:
		for present_event in events_list:
			if len(present_event) > 0:
				for present_event_details in present_event:
					total_time_diff = time_diff_in_seconds(present_event_details.to_time, present_event_details.from_time)/60
					from_time = present_event_details.from_time
					slot_name = present_event_details.event
					from_date = present_event_details.from_date
					for x in range(0, int(total_time_diff), present_event_details.duration):
						to_time = from_time + datetime.timedelta(seconds=present_event_details.duration*60)
						from frappe.utils import get_time
						start = datetime.datetime.combine(present_event_details.from_date, get_time(from_time))
						end = datetime.datetime.combine(present_event_details.from_date, get_time(to_time))
						data.append({'end': end, 'color': present_event_details.color if present_event_details.color else '#cef6d1',
							'doctype': 'Practitioner Event', 'title': present_event_details.event +" - "+ present_event_details.healthcare_practitioner_name,
							'duration':present_event_details.duration, 'name': present_event_details.name, 'start': start,
							'allDay': 0, 'practitioner': present_event_details.practitioner,'practitioner_name': present_event_details.healthcare_practitioner_name, 'service_unit': present_event_details.service_unit,
							'appointment_type': present_event_details.appointment_type})
						from_time = to_time

	prev_app_start = False
	prev_app_end = False
	prev_item_start = False
	prev_item_end = False
	remove = []
	allow_overlap = False
	overlap_data = []
	for item in data:
		if item['service_unit']:
			allow_overlap = frappe.get_value('Healthcare Service Unit', item['service_unit'], 'overlap_appointments')
		count = 0
		actual_item = item.copy()
		for appointment in appointments:
			if appointment['start'] == item['start']:
				count += 1
				if item['doctype'] != 'Patient Appointment' and appointment['end'] < item['end']:
					new_item = item.copy()
					new_item['start'] = appointment['end']
					new_item['color'] = item['color']
					# TODO: required?
					# new_item['duration'] = item['duration] - appointment['duration']
					data.append(new_item)
					item['end'] = appointment['end']
				if allow_overlap and count > 1:
					new_item = set_appointment_item(actual_item.copy(), appointment)
					overlap_data.append(new_item)
				else:
					item = set_appointment_item(item, appointment)
			if get_datetime(appointment['start']) <= get_datetime(item['start']) and get_datetime(appointment['end']) >= get_datetime(item['end']):
				if prev_app_start != appointment['start'] and prev_app_end != appointment['end'] \
					and prev_item_start != item['start'] and prev_item_end != item['end']:
					item['start'] = appointment['start']
					item['end'] = appointment['end']
					prev_app_start = appointment['start']
					prev_app_end = appointment['end']
					prev_item_start = actual_item['start']
					prev_item_end = actual_item['end']
				elif item['doctype'] != "Patient Appointment":
					remove.append(item)

			elif get_datetime(appointment['start']) < get_datetime(item['start']) and get_datetime(appointment['end']) > get_datetime(item['start']):
				item['start'] = appointment['end']

		for absent_event in absent_events_list:
			if absent_event and len(absent_event) > 0:
				weekdays = ["monday", "tuesday", "wednesday", "thursday", "friday", "saturday", "sunday"]
				for absent_event_details in absent_event:
					repeat_untill = getdate(absent_event_details['to_date'])
					if absent_event_details["repeat_this_event"] == 1:
						repeat_untill = getdate(absent_event_details['repeat_till']) if absent_event_details['repeat_till'] else getdate("3000-01-01")
					if getdate(absent_event_details["from_date"]) <= getdate(item["end"]) and repeat_untill >= getdate(item['start']):
						if get_time(absent_event_details['from_time']) <= get_time(item['start']) and get_time(absent_event_details['to_time']) >= get_time(item['end']):
							if absent_event_details["repeat_this_event"] == 1 and absent_event_details["repeat_on"] == 'Every Day':
								if absent_event_details[weekdays[getdate(item['start']).weekday()]]:
									item['color'] = absent_event_details.color if absent_event_details.color else "#F5F5F5"
									item['doctype'] = "None"
									item['title'] =  absent_event_details.event
							else:
								item['color'] = absent_event_details.color if absent_event_details.color else "#F5F5F5"
								item['doctype'] = "None"
								item['title'] =  absent_event_details.event

		if allow_overlap and count > 0:
			service_unit_capacity= frappe.get_value('Practitioner Event', actual_item['name'], 'total_service_unit_capacity')
			if service_unit_capacity and  int(service_unit_capacity) > count:
				overlap_data.append(actual_item)

	for item in remove:
		if item in data:
			data.remove(item)

	if overlap_data:
		for item in overlap_data:
			data.append(item)
	return data

def set_appointment_item(item, appointment):
	item['color'] = appointment['color']
	item['doctype'] = 'Patient Appointment'
	item['appointment_name'] = appointment['name']
	item['overlap_appointments'] = appointment['overlap_appointments']
	title = ""
	if appointment['patient_age']:
		title = ", "+appointment['patient_age']
	patient_name = appointment['patient_name'] if appointment['patient_name'] else appointment['patient']
	patient_sex = (str(appointment['patient_sex'])) if appointment['patient_sex'] else ""
	item['title'] = appointment['name']+"-"+appointment['status']+": "+patient_name+", "+patient_sex+title
	if appointment['modality']:
		item['title'] += ", Modality: "+appointment['modality']
	if appointment['service_unit']:
		item['title'] += ", Service Unit: "+appointment['service_unit']
	if appointment['insurance_company_name']:
		item['title'] += ", Insurance: "+ appointment['insurance_company_name']
	if appointment['source'] == "External Referral" and appointment["referring_practitioner"]:
		item['title'] += ", Referring Practitioner:  "+frappe.get_value("Healthcare Practitioner", appointment['referring_practitioner'], "practitioner_name")
	return item

# Get events by repeat_on
def get_events_by_repeat_on(events_list, date):
	add_events = []
	# remove_events = []
	weekdays = ["monday", "tuesday", "wednesday", "thursday", "friday", "saturday", "sunday"]
	if events_list:
		for event in events_list:
			repeat_till = getdate(event.repeat_till) if event.repeat_till else getdate('3000-01-01')
			if event.repeat_this_event and (repeat_till >= getdate(date) >= getdate(event.from_date)):
				event.from_date = getdate(date)
				if event.repeat_on == "Every Day":
					if event[weekdays[getdate(date).weekday()]]:
						add_events.append(event.copy())
					# remove_events.append(event.copy())

				if event.repeat_on=="Every Week":
					if getdate(event.from_date).weekday() == getdate(date).weekday():
						add_events.append(event.copy())
					# remove_events.append(event.copy())

				if event.repeat_on=="Every Month":
					if getdate(event.from_date).day == getdate(date).day:
						add_events.append(event.copy())
					# remove_events.append(event.copy())

				if event.repeat_on=="Every Year":
					if getdate(event.from_date).strftime("%j") == getdate(date).strftime("%j"):
						add_events.append(event.copy())
					# remove_events.append(event.copy())
			elif event.from_date == getdate(date):
				add_events.append(event.copy())


		# for e in remove_events:
		# 	events_list.remove(e)
		# events_list = events_list + add_events

		# return events_list

		return add_events if add_events else False

@frappe.whitelist()
def book_appointment_from_desk(appointment_details):
	appointment_details = json.loads(appointment_details)
	event_to_time = frappe.db.get_value("Practitioner Event", appointment_details['practitioner_event'], 'to_time')
	appointment_time = to_timedelta(appointment_details['appointment_time'])
	remaining_duration = (time_diff_in_hours(event_to_time, appointment_time))*60
	if appointment_details['duration'] > math.ceil(remaining_duration):
		frappe.throw("Appointment Duration Exceeds Practitioner Event To Time")
	else:
		appointment = frappe.new_doc("Patient Appointment")
		for key in appointment_details:
			if key == "appointment_date":
				appointment.set(key, getdate(appointment_details[key]))
			else:
				appointment.set(key, appointment_details[key] if appointment_details[key] else '')
		appointment.save(ignore_permissions=True)

@frappe.whitelist()
def update_appointment_from_desk(dialog):
	doc = json.loads(dialog)
	if doc['status'] == "Cancelled":
		if not ('cancellation_source' and 'cancellation_reason') in doc:
			frappe.throw("Cancellation Source and Cancellation Reason are mandatory fields")
		else:
			appointment = frappe.get_doc("Patient Appointment", doc['patient_appointment'])
			appointment.cancelled_by = doc['cancelled_by'] if 'cancelled_by' in doc else ''
			appointment.cancellation_notes = doc['cancellation_note'] if 'cancellation_note' in doc else ''
			appointment.cancellation_source = doc['cancellation_source'] if 'cancellation_source' in doc else ''
			appointment.cancellation_reason = doc['cancellation_reason'] if 'cancellation_reason' in doc else ''
			appointment.save(ignore_permissions = True)
	from erpnext.healthcare.doctype.patient_appointment.patient_appointment import update_status
	update_status(doc['patient_appointment'], doc['status'])

@frappe.whitelist()
def create_event_from_desk(details):
	details = json.loads(details)
	event = frappe.new_doc("Practitioner Event")
	for key in details:
		if key == "from_date":
			event.set(key, getdate(details[key]))
		else:
			event.set(key, details[key] if details[key] else '')
	event.save(ignore_permissions=True)

@frappe.whitelist()
def create_ini_file_for_radiology_appointments(doc, method):
	if doc.status in ["Open", "Scheduled"] and doc.radiology_procedure:
		if not doc.study_instance_uid:
			study_instance_uid = get_study_instance_uid(doc)
			frappe.db.set_value("Patient Appointment", doc.name, 'study_instance_uid', study_instance_uid)
		else:
			study_instance_uid = doc.study_instance_uid
		enqueue(enqueue_create_ini_file, appointment=doc, study_instance_uid=study_instance_uid, queue='default', timeout=6000)

def enqueue_create_ini_file(appointment, study_instance_uid):
	record_num = 1
	dob = frappe.get_value("Patient", appointment.patient, "dob")
	radiology_procedure = frappe.get_doc("Radiology Procedure", appointment.radiology_procedure)
	txt_to_write = """\n\n[RecordNum{0}]\nStudy Instance UID = {24}\nPatient Name = {1}\nPatient ID = {2}\nDate of Birth = {3}\nSex = {4}"""
	txt_to_write += """\nAccession Number = {5}\nReferring Physician = {6}\nRequesting Physician = {7}"""
	txt_to_write += """\nRequested Procedure ID = {8}\nRequested Procedure Description = {9}"""
	txt_to_write += """\nRequested Procedure Priority ={10}\nScheduled AE Station = {11}\nModality = {12}"""
	txt_to_write += """\nScheduled Start Date = {13}\nScheduled Start Time = {14}\nScheduled Procedure ID = {15}"""
	txt_to_write += """\nProcedure Code Value = {16}\nProcedure Code Meaning = {17}\nProcedure Code Scheme Designator = {18}"""
	txt_to_write += """\nProcedure Code Scheme Version = {19}\nProtocol Code Value = {20}\nProtocol Code Meaning = {21}"""
	txt_to_write += """\nProtocol Code Scheme Designator = {22}\nProtocol Code Scheme Version = {23}"""
	txt_to_write = txt_to_write.format(record_num, appointment.patient_name, appointment.patient, dob, appointment.patient_sex,
		radiology_procedure.accession_number, appointment.referring_practitioner, appointment.practitioner,
		radiology_procedure.schc_requested_procedure_id, radiology_procedure.schc_requested_procedure_description,
		radiology_procedure.schc_requested_procedure_priority, radiology_procedure.schc_scheduled_ae_station, appointment.modality,
		appointment.appointment_date, appointment.appointment_time, radiology_procedure.schc_scheduled_procedure_id,
		radiology_procedure.schc_procedure_code_value, radiology_procedure.schc_procedure_code_meaning, radiology_procedure.schc_procedure_code_scheme,
		radiology_procedure.schc_procedure_code_scheme_version, radiology_procedure.schc_protocol_code_value, radiology_procedure.schc_protocol_code_meaning,
		radiology_procedure.schc_protocol_code_scheme_designator, radiology_procedure.schc_protocol_code_scheme_version, study_instance_uid)
	if txt_to_write:
		import os
		private_file_path = os.path.join(frappe.get_site_path('private', 'files'), "healthcare")
		if not os.path.exists(private_file_path):
			os.mkdir(private_file_path)
		file_name = ("{0}.ini").format(appointment.name)
		path_file_name= os.path.join(private_file_path, file_name)
		f= open(path_file_name, "w+")
		f.write(txt_to_write)
		f.close()

		radiology_integration = frappe.get_doc("Radiology Integration", {'active': 1, 'default': 1})
		if radiology_integration:
			import paramiko
			from paramiko import SSHClient
			from frappe.utils.password import get_decrypted_password
			password = get_decrypted_password("Radiology Integration", radiology_integration.name, "wl_password", False)
			ssh = SSHClient()
			ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
			try:
				ssh.connect(hostname=radiology_integration.wl_hostname, username=radiology_integration.wl_username, password=password)
			except Exception as e:
				raise e
			remote_path_file_name = os.path.join(radiology_integration.wl_remote_path, file_name)
			ftp_client=ssh.open_sftp()
			try:
				ftp_client.put(path_file_name, remote_path_file_name)
			except Exception as e:
				raise e
			ftp_client.close()

@frappe.whitelist()
def appointment_hook_action(doc, method):
	if method == 'after_insert':
		# set_study_instance_uid(doc, method)
		auto_invoice_hook_action(doc, method)

def encounter_on_submit_cancel(doc, method):
	if method == 'on_submit':
		auto_invoice_hook_action(doc, method)
		update_encounter_in_appointment(doc, doc.name)
	elif method == 'on_cancel':
		update_encounter_in_appointment(doc)

def update_encounter_in_appointment(doc, encounter=''):
	if doc.appointment:
		frappe.db.set_value("Patient Appointment", doc.appointment, 'schc_encounter', encounter)

def update_item_prescription_details_in_insurance_approval(doc, method):
	if doc and doc.reference_dt == "Patient Encounter":
		patient_encounter = frappe.get_doc("Patient Encounter", doc.reference_dn)
		if patient_encounter:
			if patient_encounter.item_prescription:
				for item in patient_encounter.item_prescription:
					is_valid = check_insurance_validity_on_item(patient_encounter.insurance, item.item_code,  patient_encounter.encounter_date)
					if is_valid:
						item_line = doc.append("items")
						item_line.item = item.item_code
						item_line.requested_quantity = item.quantity
			doc.save(ignore_permissions=True)
@frappe.whitelist()
def auto_invoice_hook_action(doc, method):
	if frappe.db.get_value("Healthcare Settings", None, "auto_invoice_inpatient") == '1' and doc.inpatient_record:
		from sehacloud_healthcare.sehacloud_healthcare.healthcare_service_to_invoice import include_package_in_auto_invoice
		include_package_in_auto_invoice(doc)
		revenue_distributions_on_healthcare_doc(doc)


def revenue_distributions_on_healthcare_doc(doc):
	if doc.invoiced!=1:
		sales_invoice = get_sales_invoice_for_healthcare_doc(doc.doctype, doc.name)
		if sales_invoice and sales_invoice.items and sales_invoice.docstatus < 1 \
		and sales_invoice.items[0].reference_dt==doc.doctype and sales_invoice.items[0].reference_dn==doc.name:
			sales_invoice = set_revenue_distributions_in_invoice(sales_invoice)
			sales_invoice.save(ignore_permissions=True)

def set_revenue_distributions_in_invoice(sales_invoice):
	distributions = get_revenue_sharing_distribution(sales_invoice.items)
	if distributions and len(distributions)>0:
		sales_invoice.set("practitioner_revenue_distributions", [])
		total_doctors_charges = 0
		for distribution in distributions:
			if isinstance(distribution, dict):
				distribution = frappe._dict(distribution)
			dist_item = sales_invoice.append("practitioner_revenue_distributions")
			dist_item.practitioner = distribution.practitioner
			dist_item.item_code = distribution.item_code
			dist_item.item_amount = distribution.item_amount
			dist_item.amount = distribution.amount
			dist_item.reference_dt = distribution.reference_dt
			dist_item.reference_dn = distribution.reference_dn
			dist_item.mode_of_sharing = "Fixed"
			total_doctors_charges += distribution.amount
		sales_invoice.total_doctors_charges = total_doctors_charges
	return sales_invoice

@frappe.whitelist()
def set_study_instance_uid(doc, method):
	if doc.status not in ["Cancelled", "Closed"] and doc.radiology_procedure:
		if not doc.study_instance_uid:
			doc.study_instance_uid = get_study_instance_uid(doc)
			doc.save(ignore_permissions=True)

def get_study_instance_uid(appointment):
	import random
	study_instance_uid = "1.2.400"
	study_instance_uid += ".3"+appointment.name.strip("OP-")
	study_instance_uid += ".4"+formatdate(appointment.appointment_date, "dd.5mm.6yyyy")
	study_instance_uid += ".7"+get_time(appointment.appointment_time).strftime("%H.8%M.9%S")
	study_instance_uid += "."+str(random.randint(1000000000000000, 9999999999999999))
	return study_instance_uid

@frappe.whitelist()
def get_app_data(args):
	args = json.loads(args)
	if not 'doctype' in args or not args['doctype']:
		return False
	if not 'app_key' in args or not args['app_key']:
		return False
	doctype = args['doctype']
	if 'app_key' in args:
		app_key = args['app_key']
	fields=[]
	if 'fields' in args and args['fields']:
		fields = args['fields']
	filters={}
	if 'filters' in args:
		filters = args["filters"]
	limit_start=0
	if 'limit_start' in args:
		limit_start = args["limit_start"]
	limit_page_length=20
	if 'limit_page_length' in args:
		limit_page_length = args["limit_page_length"]
	ignore_permissions=False
	if 'ignore_permissions' in args:
		ignore_permissions = args["ignore_permissions"]
	order_by="modified desc"
	if 'order_by' in args:
		order_by = args["order_by"]
	data = {}
	fields.extend(["name"])
	result = frappe.get_list(doctype, fields=fields, filters=filters, order_by=order_by,
		limit_start=limit_start, limit_page_length=limit_page_length, ignore_permissions=ignore_permissions)
	if result:
		data['data'] = result
	return data

@frappe.whitelist()
def get_revenue_sharing_distribution(invoice_items):
	from six import string_types
	if isinstance(invoice_items, string_types):
		items = json.loads(invoice_items)
	else:
		items = invoice_items
	distributions = []
	for item in items:
		if isinstance(item, dict):
			item = frappe._dict(item)
		if item.reference_dt and item.reference_dn:
			not_share_revenue_dt = ["Inpatient Record Procedure", "Inpatient Occupancy", "Lab Prescription", "Procedure Prescription", "Radiology Procedure Prescription"]
			if not item.delivery_note and item.reference_dt != "Delivery Note" and item.reference_dt not in not_share_revenue_dt:
				ref_doc = frappe.get_doc(item.reference_dt, item.reference_dn)
				is_external = frappe.get_value("Healthcare Practitioner", ref_doc.referring_practitioner, "healthcare_practitioner_type")
				if ref_doc.source == "External Referral" and ref_doc.referring_practitioner and is_external=="External":
					distribution = {
					'item_code': item.item_code,
					'item_amount': item.amount,
					'reference_dt': item.reference_dt,
					'reference_dn': item.reference_dn,
					'amount': 0}
					if ref_doc.referring_practitioner and item.item_code and item.reference_dt and item.reference_dn:
						distribution['practitioner'] = ref_doc.referring_practitioner # Note: Procedures' referring practitioner should be primary practitioner
						if item.reference_dt == "Clinical Procedure" and item.reference_dn:
							doctor_fee = frappe.db.get_value("Clinical Procedure", item.reference_dn, 'schc_doctor_fee')
							if doctor_fee and doctor_fee > 0:
								distribution['amount'] = doctor_fee
						distributions.append(distribution)
					# Procedures can have a secondary practitioner too
					if item.reference_dt == "Clinical Procedure" and ref_doc.secondary_practitioner:
						is_external = frappe.get_value("Healthcare Practitioner", ref_doc.secondary_practitioner, "healthcare_practitioner_type")
						if is_external=="External":
							distribution_1 = {
							'item_code': item.item_code,
							'item_amount': item.amount,
							'reference_dt': item.reference_dt,
							'reference_dn': item.reference_dn,
							'amount': 0}
							distribution_1['practitioner'] = ref_doc.secondary_practitioner
							distributions.append(distribution_1)
	return distributions

@frappe.whitelist()
def get_taxes_on_package(invoice_items, posting_date):
	from six import string_types
	if isinstance(invoice_items, string_types):
		items = json.loads(invoice_items)
	else:
		items = invoice_items
	for item in items:
		if isinstance(item, dict):
			item = frappe._dict(item)
		if item.reference_dt and item.reference_dn and frappe.db.has_column(item.reference_dt, "schc_package_assignment"):
			schc_package_assignment = frappe.db.get_value(item.reference_dt, item.reference_dn, "schc_package_assignment")
			if schc_package_assignment:
				if frappe.db.exists("Healthcare Package Assignment",
					{
						'name': schc_package_assignment,
						'docstatus': 1
					}
				):
					package_assignment = frappe.get_doc("Healthcare Package Assignment", schc_package_assignment)
					if  package_assignment.taxes:
						return package_assignment.taxes, package_assignment.taxes_and_charges
	return []

@frappe.whitelist()
def create_procedure_on_package(patient_encounter, package_assignment):
	h_package = frappe.db.get_value("Healthcare Package Assignment", package_assignment, "package")
	package = frappe.get_doc("Healthcare Package", h_package)
	for item in package.items:
		template_id = exists_template_for_item(item.item_code)
		if template_id:
			create_procedure(template_id, patient_encounter)
			return True

def exists_template_for_item(item):
	return frappe.db.exists(
		"Clinical Procedure Template",
		{
			"item": item
		}
	)

def create_procedure(template_id, patient_encounter):
	from erpnext.healthcare.doctype.clinical_procedure.clinical_procedure import create_clinical_procedure_doc
	encounter = frappe.get_doc("Patient Encounter", patient_encounter)
	patient = encounter.patient
	procedure = create_clinical_procedure_doc(False, encounter.practitioner, frappe.get_doc("Patient", patient),
		frappe.get_doc("Clinical Procedure Template", template_id), source=encounter.source, ref_practitioner=encounter.referring_practitioner)
	procedure.save(ignore_permissions = True)

@frappe.whitelist()
def clinical_procedure_hook_action(doc, method):
	if doc.inpatient_record_procedure and doc.schc_patient_selling_rate and doc.schc_patient_selling_rate > 0 and \
		doc.status=="Completed" and doc.inpatient_record and doc.invoiced!=1 and \
			frappe.db.get_value("Healthcare Settings", None, "auto_invoice_inpatient") == '1':
				sales_invoice = get_sales_invoice_for_healthcare_doc(doc.doctype, doc.name)
				if sales_invoice and sales_invoice.items and sales_invoice.docstatus < 1:
					for item in sales_invoice.items:
						if not item.delivery_note and item.reference_dt == "Clinical Procedure" and item.reference_dn == doc.name:
							reduce_from_procedure_rate = 0
							if doc.consume_stock:
								delivery_note_items = get_procedure_delivery_item(doc.patient, doc.name)
								if delivery_note_items:
									for delivery_note_item in delivery_note_items:
										dn_item = frappe.get_doc("Delivery Note Item", delivery_note_item[0])
										reduce_from_procedure_rate += item_reduce_procedure_rate(dn_item, doc.items)
							item.rate = float(doc.schc_patient_selling_rate) - reduce_from_procedure_rate
					sales_invoice.save(ignore_permissions=True)
	from sehacloud_healthcare.sehacloud_healthcare.healthcare_service_to_invoice import include_package_in_auto_invoice
	include_package_in_auto_invoice(doc)
	revenue_distributions_on_procedure_auto_invoice(doc, method)

@frappe.whitelist()
def after_insert_clinical_procedure(doc, method):
	if doc.items and doc.consume_stock:
		amount = 0;
		for item in doc.items:
			if item.qty and item.valuation_rate:
				amount += item.qty*item.valuation_rate;
		doc.schc_total_valuation_rate = amount
		doc.save(ignore_permissions=True)
		doc.reload()

def revenue_distributions_on_procedure_auto_invoice(doc, method):
	if doc.status=="Completed" and doc.inpatient_record and doc.invoiced!=1 and \
		frappe.db.get_value("Healthcare Settings", None, "auto_invoice_inpatient") == '1':
			sales_invoice = get_sales_invoice_for_healthcare_doc("Clinical Procedure", doc.name)
			if sales_invoice and sales_invoice.items:
				distributions = get_revenue_sharing_distribution(sales_invoice.items)
				total_doctors_charges = 0
				if distributions and len(distributions)>0:
					sales_invoice.set("practitioner_revenue_distributions", [])
					for distribution in distributions:
						if isinstance(distribution, dict):
							distribution = frappe._dict(distribution)
						dist_item = sales_invoice.append("practitioner_revenue_distributions")
						dist_item.practitioner = distribution.practitioner
						dist_item.item_code = distribution.item_code
						dist_item.item_amount = distribution.item_amount
						dist_item.amount = distribution.amount
						dist_item.reference_dt = distribution.reference_dt
						dist_item.reference_dn = distribution.reference_dn
						dist_item.mode_of_sharing = "Fixed"
						total_doctors_charges += distribution.amount
				sales_invoice.total_doctors_charges = total_doctors_charges

				if doc.inpatient_record_procedure and doc.schc_doctor_fee:
					cost_center = False
					if doc.service_unit:
						cost_center = frappe.db.get_value("Healthcare Service Unit", doc.service_unit, "cost_center")
					item_line = sales_invoice.append("items")
					item_line.item_code = frappe.db.get_value("Healthcare Settings", None, "revenue_booking_item")
					item_details = sales_item_details_for_healthcare_doc(item_line.item_code, doc)
					item_line.item_name = item_details.item_name
					item_line.description = frappe.db.get_value("Item", item_line.item_code, "description")
					item_line.rate = doc.schc_doctor_fee
					item_line.qty = 1
					if doc.practitioner:
						from erpnext.healthcare.doctype.healthcare_settings.healthcare_settings import get_income_account
						income_account = get_income_account(doc.practitioner, doc.company)
						if income_account:
							item_line.income_account = income_account
					item_line.amount = item_line.rate*item_line.qty
					item_line.cost_center = cost_center if cost_center else ''
					item_line.reference_dt = "Inpatient Record Procedure" if doc.inpatient_record_procedure else ''
					item_line.reference_dn = doc.inpatient_record_procedure if doc.inpatient_record_procedure else ''
				sales_invoice.save(ignore_permissions=True)

@frappe.whitelist()
def get_number_in_arabic(number):
	if not number:
		return
	from sehacloud_healthcare.sehacloud_healthcare.number2word_in_arabic import number2word
	return number2word(number).validate()

def set_invoice_amount_in_word(doc, method):
	if doc.meta.get_field("in_words"):
		amount = abs(doc.grand_total if doc.is_rounded_total_disabled() else doc.rounded_total)
		doc.schc_amount_in_words_arabic = get_number_in_arabic(amount)

def is_rounded_total_disabled(doc):
	if doc.meta.get_field("disable_rounded_total"):
		return doc.disable_rounded_total
	else:
		return frappe.db.get_single_value("Global Defaults", "disable_rounded_total")

@frappe.whitelist()
def is_app_installed(app):
	if app in frappe.get_installed_apps():
		return True
	return False

def set_encounter_details_in_item_prescription(doc, method):
	if doc.item_prescription:
		for item in doc.item_prescription:
			item.insurance=doc.insurance
			item.insurance_company_name=doc.insurance_company_name
			item.insurance_approval_number=doc.insurance_approval_number
			item.insurance_remarks=doc.insurance_remarks
			item.practitioner=doc.practitioner
			item.source=doc.source
			item.referring_practitioner=doc.referring_practitioner
@frappe.whitelist()
def get_contact_details(mobile_no):
	if mobile_no:
		contact = frappe.db.exists("Contact",
		{
			'mobile_no': mobile_no
		})
		if contact:
			return frappe.get_doc("Contact", contact)
		else:
			return ""
def  validate_package_assignment(doc, method):
	if doc.schc_package_assignment:
		package_assignment_practitioner_level, patient = frappe.db.get_value("Healthcare Package Assignment", doc.schc_package_assignment, ["healthcare_practitioner_level", "patient"])
		if doc.patient!=patient:
			frappe.throw("Please Choose Right Package Assignment For The Patient")
		if package_assignment_practitioner_level:
			if doc.sch_healthcare_practitioner_level!=package_assignment_practitioner_level:
				frappe.throw("Missmatch Healthcare Practitioner Level")

@frappe.whitelist()
def change_customer_group_from_patient(customer_group, doc):
	args = json.loads(doc)
	doc = frappe._dict(args)
	frappe.db.set_value("Patient", doc.name, "customer_group", customer_group)
	frappe.db.set_value("Customer", doc.customer, "customer_group", customer_group)
	return
def encounter_validate(doc, method):
	doc.has_drug_prescription = True if doc.drug_prescription else False
	doc.has_item_prescription = True if doc.item_prescription else False
	doc.has_lab_prescription = True if doc.lab_test_prescription else False
	doc.has_procedure_prescription = True if doc.procedure_prescription else False
	doc.has_radiology_prescription = True if doc.radiology_procedure_prescription else False
