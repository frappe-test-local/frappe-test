// Copyright (c) 2016, SehaCloud and contributors
// For license information, please see license.txt
/* eslint-disable */

frappe.query_reports["Invoice Report"] = {
	"filters": [
		{
			"fieldname":"from_date",
			"label": __("From Date"),
			"fieldtype": "Date",
			"default": frappe.datetime.now_date(),
			// "width": "80"
		},
		{
			"fieldname":"to_date",
			"label": __("To Date"),
			"fieldtype": "Date",
			"default": frappe.datetime.now_date()
		},
		{
			"fieldname":"patient",
			"label": __("PID"),
			"fieldtype": "Link",
			"options": "Patient"
		},
		{
			"fieldname":"sales_invoice",
			"label": __("Sales Invoice"),
			"fieldtype": "Link",
			"options": "Sales Invoice"
		},
		{
			"fieldname":"provider",
			"label": __("Provider"),
			"fieldtype": "Link",
			"options": "Healthcare Practitioner"
		},
		{
			"fieldname":"mode_of_payment",
			"label": __("Mode of Payment"),
			"fieldtype": "Link",
			"options": "Mode of Payment"
		},
		{
			"fieldname":"owner",
			"label": __("Owner"),
			"fieldtype": "Link",
			"options": "User"
		}
	]
}
