from __future__ import unicode_literals
import frappe
from frappe.utils import getdate
from frappe import msgprint, _

def execute(filters=None):
	if not filters: filters = {}

	sales_invoice_list = get_sales_invoice(filters)
	columns = get_columns()

	if not sales_invoice_list:
		msgprint(_("No record found"))
		return columns, sales_invoice_list

	data = []
	for sales_invoice in sales_invoice_list:
		row = [sales_invoice.posting_date, sales_invoice.name, sales_invoice.patient, sales_invoice.patient_name,
		sales_invoice.ref_practitioner, sales_invoice.pract_name, sales_invoice.pract_level, sales_invoice.total,
		sales_invoice.total_taxes_and_charges, sales_invoice.discount_amount, sales_invoice.grand_total, sales_invoice.outstanding_amount,
		"", sales_invoice.owner, sales_invoice.jv_ref]
		data.append(row)

	return columns, data,


def get_columns():
	columns = [
		_("Date") + ":Date:120",
		_("Sales Invoice No.") + ":Link/Sales Invoice:150",
		_("Patient ID") + ":Link/Patient:150",
		_("Patient Name") + ":Data:120",
		_("Provider") + ":Link/Healthcare Practitioner:120",
		_("Provider Name") + ":Data:150",
		_("Price Level") + ":Data:120",
		_("Net Total") + ":Data:120",
		_("Tax Total") + ":Data:120",
		_("Discount") + ":Data:120",
		_("Grand Total") + ":Data:120",
		_("Outstanding Amount") + ":Data:120",
		_("Credit Amount") + ":Data:120",
		_("Owner") + ":Data:150",
		_("Voucher Type and No.") + ":Data:500"
	]

	return columns

def get_sales_invoice(filters):
	start = filters.from_date
	end = filters.to_date
	query = """
		select
			si.name, si.status, si.patient, si.patient_name, si.posting_date, si.ref_practitioner, si.total, si.total_taxes_and_charges, si.grand_total,
			si.outstanding_amount, si.paid_amount, si.discount_amount, si.owner, sp.mode_of_payment
		from
			`tabSales Invoice` si left join
			`tabSales Invoice Payment` sp on sp.parent=si.name
		where
			si.posting_date>=%(filters_from_date)s and si.posting_date<= %(filters_to_date)s and si.docstatus = 1
	"""
	if filters.patient:
		query += " and si.patient='{0}'".format(filters.patient)
	if filters.sales_invoice:
		query += " and si.name='{0}'".format(filters.sales_invoice)
	if filters.provider:
		query += " and si.ref_practitioner='{0}'".format(filters.provider)
	if filters.owner:
		query += " and si.owner='{0}'".format(filters.owner)
	if filters.mode_of_payment:
		query += " and sp.mode_of_payment='{0}'".format(filters.mode_of_payment)
	sales_invoice = frappe.db.sql(query.format(),{"filters_from_date":getdate(start), "filters_to_date":getdate(end)}, as_dict=True)
	for sales in sales_invoice:
		pract_level = frappe.db.get_value('Healthcare Practitioner', {'name': sales.ref_practitioner}, ['sch_healthcare_practitioner_level'])
		sales['pract_level'] = pract_level if pract_level else ''
		pract_name = frappe.db.get_value('Healthcare Practitioner', {'name': sales.ref_practitioner}, ['practitioner_name'])
		sales['pract_name'] = pract_name if pract_name else ''
		jv_query = """
			select
				concat("Journal Entry","-",je.name) as jv_ref
			from
				`tabJournal Entry` je left join
				`tabJournal Entry Account` ja on ja.parent=je.name
			where
				ja.reference_name=%(sales_id)s
		"""
		journal_entry = frappe.db.sql(jv_query.format(),{"sales_id":sales.name}, as_dict=True)
		jv_ref = False
		for jv in journal_entry:
			if jv_ref:
				jv_ref += ", "+jv.jv_ref
			else:
				jv_ref = jv.jv_ref

		sales['jv_ref'] = jv_ref if jv_ref else ''
		payment_query = """
			select
				concat("Payment Entry","-",pe.name) as pe_ref
			from
				`tabPayment Entry` pe left join
				`tabPayment Entry Reference` pr on pr.parent=pe.name
			where
				pr.reference_name=%(sales_id)s
		"""
		payment_entry = frappe.db.sql(payment_query.format(),{"sales_id":sales.name}, as_dict=True)
		pe_ref = False
		for pe in payment_entry:
			if pe_ref:
				pe_ref += ", "+pe.pe_ref
			else:
				pe_ref = pe.pe_ref
		sales['jv_ref'] += pe_ref if pe_ref else ''

	return sales_invoice
