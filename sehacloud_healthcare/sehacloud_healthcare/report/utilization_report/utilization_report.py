# Copyright (c) 2013, Frappe Technologies Pvt. Ltd. and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe, statistics
from datetime import timedelta
from frappe import msgprint, _
from frappe.utils import getdate, add_days


def execute(filters=None):
	if not filters or ( not filters.from_date or not filters.to_date):
		frappe.throw('Please set From and To date filters!')

	event_list = get_events(filters)
	columns = get_columns()

	appointments = get_appointment_details(filters)

	if not event_list:
		msgprint(_('No records found matching the filters you have set'))
		return columns, event_list

	data = []

	for pract_event in event_list:
		if pract_event['total_event_duration'] <= 0:
			continue

		if not any((data_row['department'] == pract_event['department'] and
					data_row['indent'] == 0) for data_row in data):
			group_row = get_data_row()
			group_row['trunk'] = pract_event['department']
			group_row['indent'] = 0
			group_row['department'] = pract_event['department']
			data.append(group_row)

		if not any((data_row['department'] == pract_event['department'] and
					data_row['practitioner_id'] == pract_event['practitioner'] and
					data_row['indent'] == 1) for data_row in data):
			group_row = get_data_row()
			group_row['trunk'] = pract_event['healthcare_practitioner_name']
			group_row['indent'] = 1
			group_row['department'] = pract_event['department']
			group_row['practitioner_id'] = pract_event['practitioner']
			data.append(group_row)

		# TODO: fixed, can use pract_event['total_service_unit_capacity']
		capacity = pract_event['total_service_unit_capacity'] if pract_event['total_service_unit_capacity'] else 1

		if not any((data_row['department'] == pract_event['department'] and
					data_row['practitioner_id'] == pract_event['practitioner'] and
					data_row['service_unit_capacity'] == capacity and # use capacity here as pract_event['total_service_unit_capacity'] can be None
					data_row['indent'] == 2) for data_row in data):
			group_row = get_data_row()
			group_row['trunk'] = 'Availability Group'
			group_row['indent'] = 2
			group_row['department'] = pract_event['department']
			group_row['practitioner_id'] = pract_event['practitioner']
			group_row['event_type'] = pract_event['event_type']
			group_row['slot'] = group_row['service_unit_capacity'] = capacity # keep an int copy of slot as service_unit_capacity
			data.append(group_row)

		# Build row
		# Event details
		row = get_data_row()
		row['trunk'] = pract_event['event_type']
		row['indent'] = 3 # TODO: use default
		row['department'] = pract_event['department']
		row['practitioner_id'] = pract_event['practitioner']
		row['event_type'] = pract_event['event_type']
		row['slot'] = row['service_unit_capacity'] = capacity # keep an int copy of slot as service_unit_capacity
		row['total_event_duration'] = pract_event['total_event_duration']

		# Update appointment details
		appointment_detail = next((item for item in appointments if item['practitioner_event'] == pract_event['name']), None)
		if appointment_detail: # else defaults to 0
			row['appointments_duration'] = appointment_detail['app_duration_total']
			row['visits_duration'] = appointment_detail['app_closed_duration_total']
			row['total_of_invoices_amount'] = appointment_detail['total_invoice_amt']

		row['admin_utilization'] = float('%0.2f' % (row['appointments_duration'] * 100 / float(pract_event['total_event_duration']) / int(capacity)))
		row['actual_utilization'] = float('%0.2f' % (row['visits_duration'] * 100 / float(pract_event['total_event_duration']) / int(capacity)))
		row['schedule_leakage'] = float('%0.2f' % max((100 - row['admin_utilization']), 0.0))
		row['appointments_leakage'] = float('%0.2f' % max((100 - row['actual_utilization']), 0.0))

		data.append(row)

	dept_groups = list(filter(lambda d: d['indent'] == 0, data))
	for row in dept_groups:
		row_list = list(filter(
			lambda d: d['department'] == row['department'] and d['indent'] == 3, data))
		if (len(row_list)):
			set_row_values(row, row_list)

	pract_groups = list(filter(lambda d: d['indent'] == 1, data))
	for row in pract_groups:
		row_list = list(filter(lambda d: d['department'] == row['department'] and
						d['practitioner_id'] == row['practitioner_id'] and d['indent'] == 3, data))
		if (len(row_list)):
			set_row_values(row, row_list)

	event_groups = list(filter(lambda d: d['indent'] == 2, data))
	for row in event_groups:
		row_list = list(filter(lambda d: d['department'] == row['department'] and
						d['practitioner_id'] == row['practitioner_id'] and
						d['service_unit_capacity'] == row['service_unit_capacity'] and d['indent'] == 3, data))
		if (len(row_list)):
			set_row_values(row, row_list)

	return columns, data


'''
Aggregate (and Avg) values in row_list and sets the results in dict
'''
def set_row_values(dict, row_list):
	if (len(row_list)):
		# aggregation
		dict['total_event_duration'] = sum(d['total_event_duration'] for d in row_list)
		dict['appointments_duration'] = sum(d['appointments_duration'] for d in row_list)
		dict['visits_duration'] = sum(d['visits_duration'] for d in row_list)
		dict['total_of_invoices_amount'] = sum(d['total_of_invoices_amount'] for d in row_list)
		# average
		dict['admin_utilization'] = statistics.mean(float(d['admin_utilization']) for d in row_list)
		dict['actual_utilization'] = statistics.mean(float(d['actual_utilization']) for d in row_list)
		dict['schedule_leakage'] = statistics.mean(float(d['schedule_leakage']) for d in row_list)
		dict['appointments_leakage'] = statistics.mean(float(d['appointments_leakage']) for d in row_list)

'''
Return all events for the filtered period
'''
def get_events(filters):
	start = filters.from_date
	end = filters.to_date

	event_query = """
		SELECT
			pe.name, pe.healthcare_practitioner_name, pe.practitioner, pe.event,
			pe.total_service_unit_capacity, pe.appointment_type, pe.from_time, pe.to_time,
			pe.from_date, pe.to_date, pe.duration, pe.service_unit, pe.repeat_this_event, pe.repeat_on,
			pe.repeat_till, pe.practitioner, pe.color, pe.event_type,
			pe.monday, pe.tuesday, pe.wednesday, pe.thursday, pe.friday, pe.saturday, pe.sunday,
			TIME_TO_SEC(TIMEDIFF(pe.to_time, pe.from_time)) / 60 AS event_duration,
			0 AS total_event_duration,
			hp.department, md.parent_medical_department
		FROM
			`tabPractitioner Event` pe LEFT JOIN
			`tabHealthcare Practitioner` hp ON pe.practitioner = hp.name LEFT JOIN
			`tabMedical Department` md ON md.name = hp.department
		WHERE
			present = 1 AND
			(
				(repeat_this_event = 1 AND (from_date <= %(end)s AND IFNULL(repeat_till, '3000-01-01') >= %(start)s))
				or
				(repeat_this_event != 1 AND (from_date <= %(end)s AND to_date >= %(start)s))
			)
	"""

	# Practitioner / Department filters
	if filters.practitioner:
		event_query += " AND practitioner IN ({})".format(", ".join(["'" + '%s' + "'"] * len(filters.get('practitioner')))%(tuple(filters.get('practitioner'))))
		# event_query += """ AND practitioner = %(practitioner)s"""
	elif filters.department:
		practitioner_list = ["%s"%(frappe.db.escape(d.name)) for d in frappe.get_list("Healthcare Practitioner", fields = ['name'], filters = {'department': ['in', filters.department]} )]
		event_query += " AND practitioner IN ({})".format(", ".join(['%s'] * len(practitioner_list))%(tuple(practitioner_list)))

	# Exclude Events filter
	if filters.exclude_events:
		event_query += " AND event_type NOT IN ({})".format(", ".join(["'" + '%s' + "'"] * len(filters.get('exclude_events')))%(tuple(filters.get('exclude_events'))))

	event_query += " ORDER BY hp.department, pe.practitioner, pe.total_service_unit_capacity, pe.event_type"

	present_events = frappe.db.sql(
		event_query.format(),
		{
			"start": getdate(start),
			"end": getdate(end),
			"practitioner": filters.practitioner
		},
		as_dict=True)

	# total_event_duration is fetched as zero, update duration based on event repeat
	update_event_duration_based_on_days(present_events, start, end)

	return present_events

'''
Return appointment / invoice amount grouped by practitioner event between filter dates
'''
def get_appointment_details(filters):
	if not filters.from_date or not filters.to_date :
		return None

	appointment_query = """
		SELECT
			SUM(IFNULL(pa.duration, 0)) AS app_duration_total,
			SUM(CASE WHEN pa.status IN ('Closed', 'Checked out') THEN duration ELSE 0 END) AS app_closed_duration_total,
			pa.practitioner_event AS practitioner_event,
			SUM(IFNULL(si.amount, 0)) AS total_invoice_amt
		FROM
			`tabPatient Appointment` pa LEFT JOIN
			`tabSales Invoice Item` si ON si.reference_dn = pa.name AND si.docstatus = 1
		WHERE
			appointment_date >= %(start_date)s
			AND appointment_date <= %(end_date)s
		GROUP BY pa.practitioner_event
	"""

	appointments = frappe.db.sql(
		appointment_query.format(),
		{
			"start_date": getdate(filters.from_date),
			"end_date": getdate(filters.to_date)
		},
		as_dict=True)

	return appointments


'''
Update events_list list's total_event_duration field
'''
def update_event_duration_based_on_days(events_list, start_date, end_date):
	if not events_list or not start_date or not end_date :
		return

	def daterange(start_date, end_date):
		for n in range(int((end_date - start_date).days)):
			yield start_date + timedelta(n)

	start_date = getdate(start_date)
	end_date = getdate(end_date)

	if end_date < start_date:
		return

	if start_date == end_date:
		set_total_event_duration_for_date(events_list, start_date)
	else:
		# repeat call for each date in daterange
		for date in daterange(start_date, add_days(end_date, 1)):
			set_total_event_duration_for_date(events_list, date)


'''
Helper to update_event_duration_based_on_days, sets total_event_duration for one Date
'''
def set_total_event_duration_for_date(events_list, date):
	if not events_list or not date :
		return

	weekdays = ["monday", "tuesday", "wednesday", "thursday", "friday", "saturday", "sunday"]

	# Repeat Events
	repeats = [event for event in events_list if event.repeat_this_event and \
		(getdate(event.repeat_till) >= getdate(event.from_date))]

	for event in repeats:
		event.total_service_unit_capacity = int(event.total_service_unit_capacity) if event.total_service_unit_capacity else 1
		if event.repeat_on == "Every Day" and event[weekdays[date.weekday()]]:
			event.total_event_duration += (event.event_duration)

		elif event.repeat_on =="Every Week" and weekdays[getdate(event.from_date).weekday()] == weekdays[date.weekday()]:
			event.total_event_duration += (event.event_duration)

		elif event.repeat_on =="Every Month" and getdate(event.from_date).day == date.day:
			event.total_event_duration += (event.event_duration)

		elif event.repeat_on =="Every Year" and getdate(event.from_date).strftime("%j") == date.strftime("%j"):
			# TODO: deal with leap year
			event.total_event_duration += (event.event_duration)

	# Non Repeat Events
	no_repeats = [event for event in events_list if (not event.repeat_this_event or \
		(event.repeat_this_event and event.repeat_on == '')) and \
		(getdate(event.from_date) == date)]

	for event in no_repeats:
		event.total_service_unit_capacity = int(event.total_service_unit_capacity) if event.total_service_unit_capacity else 1
		event.total_event_duration += (event.event_duration)


'''
Return a list with columns to be displayed in the report
'''
def get_columns():
	return [
		{
			'fieldname': 'trunk',
			'label': _('Practitioner'),
			'width': 200,
		},
		{
			'fieldname': 'slot',
			'label': _('Patient / Slot'),
			'fieldtype': 'Data',
			'width': 100
		},
		{
			'fieldname': 'total_event_duration',
			'label': _('Event Duration'),
			'fieldtype': 'Int',
			'width': 100
		},
		{
			'fieldname': 'appointments_duration',
			'label': _('Appointments Duration'),
			'fieldtype': 'Int',
			'width': 100
		},
		{
			'fieldname': 'visits_duration',
			'label': _('Visits Duration'),
			'fieldtype': 'Int',
			'width': 100
		},
		{
			'fieldname': 'admin_utilization',
			'label': _('Admin Utilization'),
			'fieldtype': 'Percent',
			'width': 100
		},
		{
			'fieldname': 'actual_utilization',
			'label': _('Actual Utilization'),
			'fieldtype': 'Percent',
			'width': 100
		},
		{
			'fieldname': 'schedule_leakage',
			'label': _('Schedule Leakage'),
			'fieldtype': 'Percent',
			'width': 100
		},
		{
			'fieldname': 'appointments_leakage',
			'label': _('Appointments Leakage'),
			'fieldtype': 'Percent',
			'width': 100
		},
		{
			'fieldname': 'total_of_invoices_amount',
			'label': _('Total of Invoices Amount'),
			'fieldtype': 'Currency',
			'width': 150
		}
	]


'''
Return a dict with keys initialized with defaults
'''
def get_data_row():
	return dict(zip(
		['trunk', 'department',  'practitioner', 'practitioner_id', 'event_type', 'slot',
		'total_event_duration', 'appointments_duration', 'visits_duration', 'admin_utilization', 'actual_utilization',
		'schedule_leakage', 'appointments_leakage', 'total_of_invoices_amount',
		'indent', 'service_unit_capacity'
		],
		['', '', '', '', '', '',
		0, 0, 0, 0, 0, 0, 0, 0.0,
		3, 1 # indent, service_unit_capacity
		]
	))
