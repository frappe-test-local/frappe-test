frappe.query_reports["Utilization Report"] = {
	"filters": [
		{
			"fieldname":"practitioner",
			"label": __("Practitioner"),
			"fieldtype": "MultiSelectList",
			get_data: function(data) {
				return frappe.db.get_link_options('Healthcare Practitioner', data);
			}
		},
		{
			"fieldname":"department",
			"label": __("Department"),
			"fieldtype": "MultiSelectList",
			get_data: function(data) {
				return frappe.db.get_link_options('Medical Department', data);
			}
		},
		{
			"fieldname":"from_date",
			"label": __("From Date"),
			"fieldtype": "Date",
			"default": frappe.datetime.now_date(),
		},
		{
			"fieldname":"to_date",
			"label": __("To Date"),
			"fieldtype": "Date",
			"default": frappe.datetime.now_date()
		},
		{
			"fieldname": "exclude_events",
			"label": __("Exclude Event Types"),
			"fieldtype": "MultiSelectList",
			get_data: function(data) {
				return frappe.db.get_link_options('Practitioner Event Type', data);
			}
		}
	]
};
