# Copyright (c) 2013, SehaCloud and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe
from frappe import msgprint, _

def execute(filters=None):
	if not filters: filters = {}

	appointment_list = get_appointments(filters)
	columns = get_columns()

	if not appointment_list:
		msgprint(_("No record found"))
		return columns, appointment_list

	data = []
	for appointment in appointment_list:
		encounter_id = frappe.db.exists(
			"Patient Encounter",
			{
				"appointment": appointment.name,
				"docstatus": ["<", 2]
			}
		)
		encounter_status = _("Pending")
		medical_code = ""
		ip_status = _("NA")
		if appointment.inpatient_record:
			ip_status = frappe.db.get_value("Inpatient Record", appointment.inpatient_record, "status")
		if encounter_id:
			encounter = frappe.get_doc("Patient Encounter", encounter_id)
			encounter_status = _("Submitted") if encounter.docstatus == 1 else _("Draft")
			for item in encounter.codification_table:
				medical_code += item.medical_code + ", "
			medical_code = medical_code.rstrip(", ")
		payment = _("Not Invoiced")
		if appointment.invoiced:
			payment = _("Unpaid")
			from erpnext.healthcare.utils import get_sales_invoice_for_healthcare_doc
			si = get_sales_invoice_for_healthcare_doc("Patient Appointment", appointment.name)
			if si:
				payment = si.status
		row = [ appointment.appointment_date, frappe.db.get_value("Healthcare Practitioner", appointment.practitioner, "practitioner_name"),
			appointment.appointment_type, appointment.patient_name, encounter_status, appointment.appointment_time, appointment.source,
			appointment.creation, medical_code, ip_status, payment]
		data.append(row)

	return columns, data


def get_columns():
	columns = [
		_("Date") + ":Date:120",
		_("Healthcare Practitioner") + ":Data:180",
		_("Appointment Type") + ":Link/Appointment Type:140",
		_("Patient Name") + ":Data:180",
		_("Encounter Status") + ":Data:120",
		_("Appointment Time") + ":Time:140",
		_("Referral Source") + ":Data:120",
		_("Referral Date") + ":Date:120",
		_("Service Code") + ":Data:120",
		_("Discharge") + ":Data:100",
		_("Payment") + ":Data:100",
	]

	return columns

def get_conditions(filters):
	conditions = ""
	if filters.get("from_date"):
		conditions += "and appointment_date >= %(from_date)s"
	if filters.get("to_date"):
		conditions += " and appointment_date <= %(to_date)s"

	return conditions

def get_appointments(filters):
	conditions = get_conditions(filters)
	return frappe.db.sql("""
		select
			*
		from
			`tabPatient Appointment`
		where
			docstatus<2 %s order by appointment_date desc, name desc""" %
		conditions, filters, as_dict=1)
