// Copyright (c) 2016, SehaCloud and contributors
// For license information, please see license.txt
/* eslint-disable */

frappe.query_reports["Appointment Report"] = {
	"filters": [
		{
			"fieldname":"from_date",
			"label": __("From Date"),
			"fieldtype": "Date",
			"default": frappe.datetime.now_date(),
			"width": "80"
		},
		{
			"fieldname":"to_date",
			"label": __("To Date"),
			"fieldtype": "Date",
			"default": frappe.datetime.now_date()
		}
	]
}
