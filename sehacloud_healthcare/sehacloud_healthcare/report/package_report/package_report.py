from __future__ import unicode_literals
import frappe
from frappe import msgprint, _

def execute(filters=None):
	if not filters: filters = {}

	package_assignment_list = get_package_assignment(filters)
	columns = get_columns()

	if not package_assignment_list:
		msgprint(_("No record found"))
		return columns, package_assignment_list

	data = []
	for package_assignment in package_assignment_list:
		row = [  package_assignment.package_name, package_assignment.package, package_assignment.patient, package_assignment.patient_name,
		package_assignment.from_date, package_assignment.to_date, package_assignment.package_price, package_assignment.availed_amount,
		package_assignment.balance_amount, package_assignment.item_details, package_assignment.balance_session, package_assignment.head_and_rate,
		package_assignment.paid_amount, package_assignment.payment_entry_no]
		data.append(row)

	return columns, data,


def get_columns():
	columns = [
		_("Package Name") + ":Data:120",
		_("Package ID") + ":Link/Healthcare Package:120",
		_("Patient ID") + ":Link/Patient:200",
		_("Patient Name") + ":Data:150",
		_("Purchased Date") + ":Data:120",
		_("Validity Date") + ":Data:100",
		_("Package Price") + ":Data:120",
		_("Availed Amount") + ":Data:120",
		_("Balance Amount") + ":Data:120",
		_("Items - (Visits Consumed/Units)") + ":Data:250",
		_("Items - (Visits Remaining/Units)") + ":Data:250",
		_("Tax") + ":Data:150",
		_("Total Payment") + ":Data:150",
		_("Payment Entry") + ":Data:500",
	]

	return columns

def get_package_assignment(filters):
	filters['docstatus'] = 1 # Fetch only submitted Healthcare Package Assignments
	package_assignment = frappe.db.get_list("Healthcare Package Assignment",
		fields=['patient', 'patient_name', 'from_date', 'package_name', 'package', 'to_date', 'package_price', 'availed_amount', 'balance_amount', 'name', 'paid_amount'],
			filters=filters)
	for package in package_assignment:
		item = False
		visits = False
		package_assignment_item = frappe.db.get_list("Healthcare Package Item", fields=['item_code', 'completed_session', 'balance_sessions'], filters={"parent":package.name})
		for package_item in package_assignment_item:
			if package_item.item_code:
				if not item:
					item = package_item.item_code+" - ("+str(package_item.completed_session)+"/"+str(package_item.balance_sessions+package_item.completed_session)+")"
				else:
					item += ", "+package_item.item_code+" - ("+str(package_item.completed_session)+"/"+str(package_item.balance_sessions+package_item.completed_session)+")"
			if package_item.item_code:
				if not visits:
					visits = package_item.item_code+" - ("+str(package_item.balance_sessions)+"/"+str(package_item.balance_sessions+package_item.completed_session)+")"
				else:
					visits += ", "+package_item.item_code+" - ("+str(package_item.balance_sessions)+"/"+str(package_item.balance_sessions+package_item.completed_session)+")"

		package['item_details'] = item if item else ''
		package['balance_session'] = visits if visits else ''

	for package in package_assignment:
		tax = False
		tax_rate = frappe.db.get_list("Sales Taxes and Charges", fields=['account_head', 'rate'], filters={"parent":package.name})
		for taxes in tax_rate:
			if taxes.account_head and taxes.rate:
				if not tax:
					tax = str(taxes.account_head) +" - "+ str(taxes.rate)
				else:
					tax += ", "+str(taxes.account_head) + " - " + str(taxes.rate)
		package['head_and_rate'] = tax if tax else ''

	for package in package_assignment:
		payment_reference = False
		package_payment_reference_item = frappe.db.get_list("Package Payment Reference", fields=['package_payment_reference', 'mode_of_payment', 'payment_owner'], filters={"parent":package.name})
		for payment_entry in package_payment_reference_item:
			if payment_entry.package_payment_reference and payment_entry.mode_of_payment:
				payment_owner = ("-"+payment_entry.payment_owner) if payment_entry.payment_owner else ''
				if not payment_reference:
					payment_reference = payment_entry.package_payment_reference +" - "+ payment_entry.mode_of_payment+payment_owner
				else:
					payment_reference += ", "+payment_entry.package_payment_reference + " - " + payment_entry.mode_of_payment+payment_owner
		package['payment_entry_no'] = payment_reference if payment_reference else ''

	return package_assignment
