// Copyright (c) 2016, SehaCloud and contributors
// For license information, please see license.txt
/* eslint-disable */
frappe.query_reports["Package Report"] = {
	"filters": [
		{
			"fieldname":"from_date",
			"label": __("Date of Purchased"),
			"fieldtype": "Date",
			"default": frappe.datetime.now_date(),
			"width": "80"
		},
		{
			"fieldname":"patient",
			"label": __("PID"),
			"fieldtype": "Link",
			"options": "Patient"
		}
	],
}
