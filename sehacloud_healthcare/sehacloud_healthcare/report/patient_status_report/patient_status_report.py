# Copyright (c) 2013, Frappe Technologies Pvt. Ltd. and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe
from frappe import msgprint, _
from frappe.utils import getdate, time_diff_in_seconds, datetime, get_time, date_diff, add_days, flt
from collections import Counter
import datetime


def execute(filters=None):
	if not filters: filters = {}

	details = {}
	if filters and filters.from_date and filters.to_date:
		details = get_details(filters)
	columns = get_columns(filters)

	if not details:
		msgprint(_("No record found"))
		return columns, details

	data = []
	for patient in details:
		row = [patient["patient_name"], patient['appointment_count'], patient['encounter_count'], patient['cancelled'], patient['no_show'], patient['sales_invoice_count'],
		patient['total_billing']]

		data.append(row)

	return columns, data

def get_columns(filters):
	columns = [
		_("Name") + ":Data:200",
		_("Appointments") + ":Data:150",
		_("Encounters") + ":Data:150",
		_("Cancelled") + ":Data:150",
		_("No Show") + ":Data:150",
		_("Invoices") + ":Data:150",
		_("Total Amount of Invoices") + ":Currency:200",

	]
	return columns

def get_details(filters):
	start = filters.from_date
	end = filters.to_date
	date_range = get_date_range(start, end)
	app_query = """
		select
			pa.name, pa.status, pa.patient, pa.patient_name, pe.name as encounter, pe.patient as enc_patient
		from
			`tabPatient Appointment` pa left join
			`tabPatient Encounter` pe on pe.appointment=pa.name
		where
			pa.appointment_date>=%(filters_from_date)s and pa.appointment_date<= %(filters_to_date)s
	"""
	appointments = frappe.db.sql(app_query.format(),{"filters_from_date":getdate(start), "filters_to_date":getdate(end)}, as_dict=True)
	appointment_count = Counter(apps['patient'] for apps in appointments)
	status_count = Counter((app_status['patient'], app_status['status']) for app_status in appointments)
	encounter_count = Counter((enc['enc_patient']) for enc in appointments)
	patient_real_list = []
	patient_list = [] # this list is for filtering repeating patients
	for app in appointments:
		if app['patient'] not in patient_list:
			patient_list.append(app['patient'])
			app['appointment_count'] = 0
			app['cancelled'] = 0
			app['no_show'] = 0
			app['encounter_count'] = 0
			app['sales_invoice_count'] = 0
			app['total_billing'] = 0
			app['appointment_count'] = 0
			if app['patient'] in appointment_count:
				app['appointment_count'] = appointment_count[app['patient']]
			if (app['patient'],'Cancelled') in status_count:
				app['cancelled'] = status_count[app['patient'],'Cancelled']
			if (app['patient'],'No show') in status_count:
				app['no_show'] = status_count[app['patient'],'No show']
			if (app['patient']) in encounter_count:
				app['encounter_count'] = encounter_count[app['patient']]
			patient_real_list.append(app)

	for app in patient_real_list:
		invoice_list = frappe.db.get_list("Sales Invoice", fields=['name', 'patient', 'grand_total'], filters={'patient':app['patient'], 'docstatus':1,  "posting_date": ['in', date_range]})
		if invoice_list:
			invoice_count = Counter((invoice['patient']) for invoice in invoice_list)
			if app['patient'] in invoice_count:
				app['sales_invoice_count'] = invoice_count[app['patient']]
		doc = frappe.get_doc('Patient', app['patient'])
		amount = get_patient_billing_info(doc, start, end, date_range)
		app['total_billing'] = amount['total_billing']
	return patient_real_list

def get_patient_billing_info(patient, start, end, date_range):
	from erpnext.accounts.utils import get_balance_on
	filters = {'docstatus': 1, 'customer': patient.customer, "posting_date": ['in', date_range]}

	company = False
	fields = ["patient", "sum(grand_total) as grand_total", "sum(base_grand_total) as base_grand_total"]
	patient_grand_total = frappe.get_all("Sales Invoice", filters=filters, fields=fields)
	filters['status'] = ['not in', 'Paid']
	fields = ["patient", "sum(outstanding_amount) as outstanding_amount"]
	patient_total_unpaid = frappe.get_all("Sales Invoice", filters=filters, fields=fields)

	if not company:
		company = frappe.defaults.get_user_default('company')
	if not company:
		company = frappe.db.get_value("Global Defaults", None, "default_company")

	company_default_currency = frappe.db.get_value("Company", company, 'default_currency')
	from erpnext.accounts.party import get_party_account_currency
	party_account_currency = get_party_account_currency("Customer", patient.customer, company)

	if party_account_currency==company_default_currency:
		billing_this_year = flt(patient_grand_total[0]["base_grand_total"])
	else:
		billing_this_year = flt(patient_grand_total[0]["grand_total"])
	total_unpaid = flt(patient_total_unpaid[0]["outstanding_amount"])

	info = {}
	info["total_billing"] = flt(billing_this_year) if billing_this_year else 0
	info["currency"] = party_account_currency
	info["total_unpaid"] = flt(total_unpaid) if total_unpaid else 0
	info["party_balance"] = get_balance_on(party_type="Customer", party=patient.customer)
	return info

def get_date_range(start, end):
	date_diff = []
	end_date = end
	if start and end:
		if start == end:
			date_diff.append(str(start))
		else:
			start = datetime.datetime.strptime(start, "%Y-%m-%d")
			end = datetime.datetime.strptime(end, "%Y-%m-%d")
			date_array = (start + datetime.timedelta(days=x) for x in range(0, (end-start).days))
			for date_object in date_array:
				date_diff.append(date_object.strftime("%Y-%m-%d"))
			date_diff.append(str(end_date))
	return date_diff
