// Copyright (c) 2016, SehaCloud and contributors
// For license information, please see license.txt
/* eslint-disable */

frappe.query_reports["Collection Report"] = {
	"filters": [
		{
			"fieldname":"from_date",
			"label": __("From Date"),
			"fieldtype": "Date",
			"default": frappe.datetime.now_date(),
			// "width": "80"
		},
		{
			"fieldname":"to_date",
			"label": __("To Date"),
			"fieldtype": "Date",
			"default": frappe.datetime.now_date()
		},
		{
            "fieldname":"mode_of_payment",
            "label": __("Mode of Payment"),
            "fieldtype": "MultiSelectList",
            get_data: function(txt) {
                return frappe.db.get_link_options('Mode of Payment', txt);
            }
        },
		{
			"fieldname":"patient",
			"label": __("PID"),
			"fieldtype": "Link",
			"options": "Patient"
		},
		{
			"fieldname":"owner",
			"label": __("Owner"),
			"fieldtype": "Link",
			"options": "User"
		},
		{
			"fieldname":"invoice_package",
			"label": __("Invoice/Package Number"),
			"fieldtype": "Data",
		}
	]
}
