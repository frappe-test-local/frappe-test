from __future__ import unicode_literals
import frappe
from frappe.utils import getdate
from frappe import msgprint, _

def execute(filters=None):
	if not filters: filters = {}

	data_list = get_data(filters)
	columns = get_columns()

	if not data_list:
		msgprint(_("No record found"))
		return columns, data_list

	data = []
	prev_data_ls = {}
	for data_ls in data_list:
		row = [data_ls['date'], data_ls['patient'], data_ls['customer'], data_ls['type'], data_ls['name'], data_ls['mode_of_payment'], data_ls['total'], data_ls['reference_type'], data_ls['reference_name'], data_ls['p_date'], data_ls['owner']]
		data.append(row)
		if data_ls['unallocated_amount'] > 0 and data_ls['reference_name'] and not any((item[4] == data_ls['name'] and item[8] == '')  for item in data):
			row = [data_ls['date'], data_ls['patient'], data_ls['customer'], data_ls['type'], data_ls['name'], data_ls['mode_of_payment'], data_ls['unallocated_amount'], '', '', '', data_ls['owner']]
			data.append(row)
	return columns, data,


def get_columns():
	columns = [
        {
            'fieldname': 'date',
            'label': _('Date'),
            'fieldtype': 'Date',
			'width': 120
        },
        {
            'fieldname': 'patient',
            'label': _('Patient'),
            'fieldtype': 'Link',
            'options': 'Patient',
			'width': 150
        },
		{
            'fieldname': 'customer',
            'label': _('Customer'),
            'fieldtype': 'Link',
            'options': 'Customer',
			'width': 150
        },
		{
            'fieldname': 'type',
            'label': _('Type'),
            'fieldtype': 'Data',
			'width': 100
        },
        {
            'fieldname': 'name',
            'label': _('Receipt/ Voucher'),
            'fieldtype': 'Dynamic Link',
            'options': 'type',
			'width': 200
        },
        {
            'fieldname': 'mode_of_payment',
            'label': _('Mode of payment'),
            'fieldtype': 'Data',
			'width': 120
        },
		{
            'fieldname': 'total',
            'label': _('Amount'),
            'fieldtype': 'Data',
			'width': 120
        },
		{
            'fieldname': 'reference_type',
            'label': _('Reference Type'),
            'fieldtype': 'Data',
			'width': 100
        },
		{
            'fieldname': 'reference_name',
            'label': _('Invoice Number/ Package Number'),
			'fieldtype': 'Dynamic Link',
            'options': 'reference_type',
			'width': 150
        },
		{
            'fieldname': 'p_date',
            'label': _('Reference Date'),
            'fieldtype': 'Date',
			'width': 120
        },
		{
            'fieldname': 'owner',
            'label': _('Owner'),
            'fieldtype': 'Data',
			'width': 150
        }
    ]

	return columns

def get_data(filters):
	start = filters.from_date
	end = filters.to_date
	query = """
		select
			pa.patient, pa.patient_name, pa.customer, pa.name as reference_name, pr.package_payment_reference as name, pr.mode_of_payment, pa.from_date as p_date, pa.to_date,
			"Journal Entry" as type, pa.owner, jv.posting_date as date, jv.total_debit as total, "Healthcare Package Assignment" as reference_type
		from
			`tabHealthcare Package Assignment` pa left join
			`tabPackage Payment Reference` pr on pr.parent=pa.name left join
			`tabJournal Entry` jv on jv.name=pr.package_payment_reference

		where
			pa.docstatus = 1 and pr.package_payment_reference != "None" and jv.docstatus = 1 and jv.posting_date>=%(filters_from_date)s and jv.posting_date<= %(filters_to_date)s
	"""
	if filters.mode_of_payment:
		mode_of_payment = ', '.join('"{0}"'.format(mode) for mode in filters.mode_of_payment)
		query += " and pr.mode_of_payment in ({0})".format(mode_of_payment)
	if filters.patient:
		customer  = frappe.db.get_value('Patient', {'name': filters.patient}, ['customer'])
		query += " and pa.customer='{0}'".format(customer)
	if filters.owner:
		query += " and pa.owner='{0}'".format(filters.owner)
	if filters.invoice_package:
		query += " and pa.name='{0}'".format(filters.invoice_package)
	package_assignment = frappe.db.sql(query.format(),{"filters_from_date":getdate(start), "filters_to_date":getdate(end)}, as_dict=True)
	for payment in package_assignment:
		payment['unallocated_amount'] = 0
	payment_query = """
		select
			pe.patient, pe.patient_name, pe.name, pe.posting_date as date, pe.mode_of_payment, "Payment Entry" as type, pe.paid_amount, pe.party as customer,
			pr.reference_name, pe.owner, pr.allocated_amount, pe.unallocated_amount
		from
			`tabPayment Entry` pe left join
			`tabPayment Entry Reference` pr on pr.parent=pe.name
		where
			pe.docstatus = 1 and pe.payment_type="Receive" and pe.posting_date>=%(filters_from_date)s and pe.posting_date<= %(filters_to_date)s
	"""
	if filters.mode_of_payment:
		mode_of_payment = ', '.join('"{0}"'.format(mode) for mode in filters.mode_of_payment)
		payment_query += " and pe.mode_of_payment in ({0})".format(mode_of_payment)
	if filters.patient:
		payment_query += " and pe.party='{0}'".format(customer)
	if filters.owner:
		payment_query += " and pe.owner='{0}'".format(filters.owner)
	if filters.invoice_package:
		payment_query += " and pr.reference_name='{0}'".format(filters.invoice_package)
	payment_entry = frappe.db.sql(payment_query.format(),{"filters_from_date":getdate(start), "filters_to_date":getdate(end)}, as_dict=True)
	for payment in payment_entry:
		posting_date = frappe.db.get_value('Sales Invoice', {'name': payment.reference_name}, ['posting_date'])
		payment['p_date'] = posting_date if posting_date else ''
		payment['reference_type'] = "Sales Invoice" if payment.reference_name else ''
		payment['total'] = payment['allocated_amount']  if payment.reference_name else payment['paid_amount']
		payment['unallocated_amount'] = payment.unallocated_amount

	package_assignment.extend(payment_entry)

	if filters.invoice_package:
		pass
	else:
		sales_query = """
			select
				si.patient, si.patient_name, si.name, si.customer, si.posting_date as date, si.owner, sp.mode_of_payment, sp.amount, "Sales Invoice" as type
			from
				`tabSales Invoice` si left join
				`tabSales Invoice Payment` sp on sp.parent=si.name
			where
				si.docstatus = 1 and si.is_pos = 1 and si.posting_date>=%(filters_from_date)s and si.posting_date<= %(filters_to_date)s
		"""
		if filters.mode_of_payment:
			mode_of_payment = ', '.join('"{0}"'.format(mode) for mode in filters.mode_of_payment)
			sales_query += " and sp.mode_of_payment in ({0})".format(mode_of_payment)
		if filters.patient:
			sales_query += " and si.customer='{0}'".format(customer)
		if filters.owner:
			sales_query += " and si.owner='{0}'".format(filters.owner)
		sales_invoice = frappe.db.sql(sales_query.format(),{"filters_from_date":getdate(start), "filters_to_date":getdate(end)}, as_dict=True)
		for payment in sales_invoice:
			payment['reference_name'] = ""
			payment['reference_type'] = ""
			payment['p_date'] = ""
			payment['total'] = payment['amount'] if payment['amount'] else ''
			payment['unallocated_amount'] = 0
		package_assignment.extend(sales_invoice)
	return package_assignment
