// Copyright (c) 2019, SehaCloud and contributors
// For license information, please see license.txt
frappe.listview_settings['Healthcare Package Assignment'] = {
	add_fields: ["name", "status"],
	get_indicator: function(doc) {
		if(doc.status=="Partial"){
			return [__("Partial"), "yellow", "status,=,Partial"];
		}
		if(doc.status=="Paid"){
			return [__("Paid"), "green", "status,=,Paid"];
		}
		if(doc.status=="Completed"){
			return [__("Completed"), "green", "status,=,Completed"];
		}
		if(doc.status=="Partial and Completed"){
			return [__("Partial and Completed"), "yellow", "status,=,Partial and Completed"];
		}
		if(doc.status=="Unpaid and Completed"){
			return [__("Unpaid and Completed"), "red", "status,=,Unpaid and Completed"];
		}
		if(doc.status=="Discontinued"){
			return [__("Discontinued"), "orange", "status,=,Discontinued"];
		}
	},
};
