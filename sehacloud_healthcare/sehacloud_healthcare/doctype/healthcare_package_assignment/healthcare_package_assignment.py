# -*- coding: utf-8 -*-
# Copyright (c) 2019, SehaCloud and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe, json
from frappe import _
from frappe.utils import nowdate, flt
from erpnext.accounts.party import get_party_account
from frappe.model.document import Document
from erpnext.healthcare.doctype.patient_appointment.patient_appointment import update_status
from erpnext.accounts.doctype.tax_rule.tax_rule import get_tax_template
from erpnext.accounts.utils import update_reference_in_payment_entry

class HealthcarePackageAssignment(Document):

	def on_update_after_submit(self):
		if self.items and self.status != 'Discontinued':
			self.updating_balance()
		if self.status == 'Discontinued' and self.outstanding_amount:
			adjust_outstanding_amount_jv(self)
			self.db_set("outstanding_amount", 0)

	def on_cancel(self):
		if frappe.db.exists("Patient Appointment", {"schc_package_assignment": self.name , "invoiced":1 }):
			frappe.throw(_("There is a invoiced Appointment in this Package Assignment"))
		if frappe.db.exists("Patient Appointment", {"schc_package_assignment": self.name ,'status':['!=', 'Cancelled'] }):
			frappe.throw(_('There are <a href="#List/Patient Appointment/List">Appointments</a> in this Package Assignment'))
		if frappe.db.exists("Clinical Procedure", {"schc_package_assignment": self.name , "invoiced":1 }):
			frappe.throw(_("There is a invoiced Clinical Procedure in this Package Assignment"))
		frappe.db.set_value(self.doctype, self.name, "status", "Cancelled")
		if self.package_payment_reference:
			for payment_reference in self.package_payment_reference:
				if payment_reference.package_payment_reference:
					frappe.get_doc('Journal Entry', payment_reference.package_payment_reference).cancel()
				if payment_reference.package_transfer_reference:
					frappe.get_doc('Journal Entry', payment_reference.package_transfer_reference).cancel()
		submitted_jv = ""
		initial_completed_jv = ""
		credit_balance_jv = ""
		adjust_jv = ""
		if self.submitted_reference:
			submitted_jv = self.submitted_reference
			frappe.get_doc('Journal Entry',self.submitted_reference).cancel()
		if self.initial_completed_session_jv_reference:
			initial_completed_jv = self.initial_completed_session_jv_reference
			frappe.get_doc('Journal Entry',self.initial_completed_session_jv_reference).cancel()
		if self.patient_credit_balance_payment:
			credit_balance_jv = self.patient_credit_balance_payment
			frappe.get_doc('Journal Entry',self.patient_credit_balance_payment).cancel()
		if self.adjust_outstanding_amount_jv:
			adjust_jv = self.adjust_outstanding_amount_jv
			frappe.get_doc('Journal Entry',self.adjust_outstanding_amount_jv).cancel()
		if self.package_account_transfer_reference:
			for account_transfer_reference in self.package_account_transfer_reference:
				if account_transfer_reference.package_account_transfer_reference:
					jv = frappe.get_doc('Journal Entry', account_transfer_reference.package_account_transfer_reference)
					if jv and jv.docstatus != 2:
						jv.cancel()
		frappe.db.set_value(self.doctype, self.name, "submitted_reference","")
		frappe.db.set_value(self.doctype, self.name, "patient_credit_balance_payment","")
		frappe.db.set_value(self.doctype, self.name, "initial_completed_session_jv_reference","")
		frappe.db.set_value(self.doctype, self.name, "adjust_outstanding_amount_jv","")
		frappe.db.set_value(self.doctype, self.name, "paid_amount",0)
		self.reload()
		self.db_set("submitted_reference", submitted_jv)
		self.db_set("patient_credit_balance_payment", credit_balance_jv)
		self.db_set("initial_completed_session_jv_reference", initial_completed_jv)
		self.db_set("adjust_outstanding_amount_jv", adjust_jv)

	def updating_balance(self):
		total_completed_amount = 0
		total_balance_amount = 0
		is_completed=True
		status=self.status
		for item in self.items:
			per_session_amount = item.session_total / item.no_of_session
			item.balance_sessions = item.no_of_session - item.completed_session
			item.availed_amount = per_session_amount * item.completed_session
			item.balance_amount = item.session_total - item.availed_amount
			if item.balance_amount>0 or item.balance_sessions>0:
				is_completed=False
			item_update_query = """
				update
					`tabHealthcare Package Item`
				set
					balance_sessions = %s,
					availed_amount = %s,
					balance_amount = %s
				where
					name = %s
			"""
			frappe.db.sql(item_update_query,
				(item.balance_sessions, item.availed_amount, item.balance_amount, item.name))
			total_completed_amount += item.availed_amount
			total_balance_amount += item.balance_amount
		self.balance_amount = self.package_price - total_balance_amount
		self.availed_amount = total_completed_amount
		if is_completed:
			if self.outstanding_amount == 0:
				status="Completed"
			elif self.outstanding_amount > 0 and self.paid_amount >0:
				status="Partial and Completed"
			else:
				status="Unpaid and Completed"
		else:
			if self.outstanding_amount == 0:
				status="Paid"
			elif self.outstanding_amount > 0 and self.paid_amount >0:
				status="Partial"
			else:
				status="Submitted"
		query = """
			update
				`tabHealthcare Package Assignment`
			set
				availed_amount = %s,
				balance_amount = %s,
				status=%s
			where
				name = %s
		"""
		frappe.db.sql(query, (total_completed_amount, total_balance_amount, status, self.name))

	def on_submit(self):
		if self.items:
			self.updating_balance()
		if self.availed_amount:
			self.create_journal_entry_for_completed_session()

		if self.allocate_patient_credit_balance and self.patient_credit_balance >0:
			allocated_amount = self.patient_credit_balance if self.patient_credit_balance <= self.package_price else self.package_price
			transfer_jv = create_journal_entry_credit_balance_submit(self, allocated_amount, self.from_date)
			frappe.db.set_value(self.doctype, self.name, "patient_credit_balance_payment",transfer_jv.name)
			unallocated_entries = self.get_unallocated_payment_entries()
			reconciled_amount = 0
			if unallocated_entries:
				for unallocated_entry in unallocated_entries:
					payment_entry = frappe.get_doc("Payment Entry", unallocated_entry.reference_name)
					unallocated_amount = payment_entry.unallocated_amount if payment_entry.unallocated_amount <= allocated_amount else allocated_amount
					if reconciled_amount > 0 and reconciled_amount < allocated_amount:
						unallocated_amount = payment_entry.unallocated_amount if payment_entry.unallocated_amount <= allocated_amount - reconciled_amount else allocated_amount - reconciled_amount
						d = self.get_payment_details(unallocated_entry, unallocated_amount, transfer_jv)
						update_reference_in_payment_entry(d, payment_entry)
					elif reconciled_amount <= 0:
						d = self.get_payment_details(unallocated_entry, unallocated_amount, transfer_jv)
						update_reference_in_payment_entry(d, payment_entry)
					reconciled_amount = reconciled_amount + unallocated_amount
			outstanding_amount = self.package_price - allocated_amount
			payment_status = "Partial" if (outstanding_amount > 0) else "Paid"
			frappe.db.set_value(self.doctype, self.name, "paid_amount", allocated_amount)
			frappe.db.set_value(self.doctype, self.name, "status", payment_status)
			frappe.db.set_value(self.doctype, self.name, "outstanding_amount", outstanding_amount  if (outstanding_amount > 0) else 0)
			if outstanding_amount > 0:
				self.create_journal_entry_submit(outstanding_amount)
		else:
			self.create_journal_entry_submit(self.package_price)
			frappe.db.set_value(self.doctype, self.name, "status", "Submitted")
			frappe.db.set_value(self.doctype, self.name, "outstanding_amount", self.package_price)
		self.reload()


	def create_journal_entry_submit(self, amount):
		party_type = "Customer"
		party_account = get_party_account(party_type, self.customer, self.company)
		journal_entry = frappe.new_doc('Journal Entry')
		journal_entry.voucher_type = 'Journal Entry'
		journal_entry.naming_series = self.journal_voucher_series
		journal_entry.company = self.company
		journal_entry.posting_date =  self.from_date
		accounts = []
		accounts.append({
			"account": frappe.db.get_value("Healthcare Package", self.package, 'temporary_package_income_account'),
			"credit_in_account_currency": amount
		})
		accounts.append({
			"account": party_account,
			"debit_in_account_currency": amount,
			"party_type": "Customer",
			"party": self.customer
		})
		# if self.taxes:
		# 	for tax in self.taxes:
		# 		accounts.append({
		# 			"account": tax.account_head,
		# 			"credit_in_account_currency": tax.tax_amount
		# 		})
		journal_entry.set("accounts", accounts)
		journal_entry.save(ignore_permissions = True)
		journal_entry.submit()
		frappe.db.set_value(self.doctype, self.name, "submitted_reference",journal_entry.name)

	def create_journal_entry_payment(self, paid_amount=False, account_paid_to=False, mode_of_payment=False, reference_no=False,
	reference_date=False, posting_date=False, payment_jv = None, transfer_jv = None):
		self.reload()
		party_type = "Customer"
		party_account = get_party_account(party_type, self.customer, self.company)
		if not paid_amount:
			paid_amount = self.package_price
		if not account_paid_to:
			account_paid_to = self.account_paid_to
		self.outstanding_amount = self.outstanding_amount - float(paid_amount)
		self.paid_amount = self.paid_amount + float(paid_amount)
		journal_entry = frappe.new_doc('Journal Entry')
		if payment_jv:
			journal_entry.amended_from = payment_jv
		journal_entry.voucher_type = 'Journal Entry'
		journal_entry.naming_series = self.journal_voucher_series
		journal_entry.company = self.company
		journal_entry.posting_date =  posting_date

		journal_entry.set("accounts", [
			{
				"account": account_paid_to,
				"debit_in_account_currency": paid_amount
			},
			{
				"account": party_account,
				"credit_in_account_currency": paid_amount,
				"party_type": "Customer",
				"party": self.customer
			}
		])
		if reference_no and reference_date:
			journal_entry.cheque_no=reference_no
			journal_entry.cheque_date=reference_date
		journal_entry.save(ignore_permissions = True)
		journal_entry.submit()
		payment_status = "Paid" if (self.outstanding_amount == 0) else "Partial"
		self.status = payment_status
		payment_reference = self.append("package_payment_reference")
		payment_reference.package_payment_reference = journal_entry.name
		payment_reference.posting_date =  posting_date
		payment_reference.mode_of_payment = mode_of_payment
		payment_reference.paid_amount = paid_amount
		payment_reference.reference_no = reference_no
		payment_reference.reference_date = reference_date
		payment_reference.payment_owner = journal_entry.owner
		payment_reference.jv_status = 'Submitted'
		self.updating_balance()
		new_transfer_jv = self.create_journal_entry_transfer(paid_amount, posting_date, transfer_jv)
		payment_reference.package_transfer_reference = new_transfer_jv.name
		self.save(ignore_permissions=True)
		self.reload()
		return journal_entry

	def create_journal_entry_transfer(self, paid_amount, posting_date, transfer_jv = None):
		journal_entry = frappe.new_doc('Journal Entry')
		if transfer_jv:
			journal_entry.amended_from = transfer_jv
		journal_entry.voucher_type = 'Journal Entry'
		journal_entry.company = self.company
		journal_entry.naming_series = self.journal_voucher_series
		journal_entry.posting_date =  posting_date
		journal_entry.set("accounts", [
			{
				"account": self.package_advance,
				"credit_in_account_currency": paid_amount
			},
			{
				"account": frappe.db.get_value("Healthcare Package", self.package, 'temporary_package_income_account'),
				"debit_in_account_currency": paid_amount,
			}
		])
		journal_entry.save(ignore_permissions = True)
		journal_entry.submit()
		return journal_entry

	def create_journal_entry_for_completed_session(self):
		journal_entry = frappe.new_doc('Journal Entry')
		journal_entry.voucher_type = 'Journal Entry'
		journal_entry.naming_series = self.journal_voucher_series
		journal_entry.company = self.company
		journal_entry.posting_date =  self.from_date

		accounts = []
		accounts.append({
				"account": get_party_account("Customer", self.customer, self.company),
				"credit_in_account_currency": self.availed_amount,
				"party_type": "Customer",
				"party": self.customer
			}
		)
		accounts.append({
			"account": self.package_advance,
			"debit_in_account_currency": self.availed_amount
		})
		accounts.append({
				"account": get_party_account("Customer", self.customer, self.company),
				"debit_in_account_currency": self.availed_amount,
				"party_type": "Customer",
				"party": self.customer
			}
		)
		accounts.append({
			"account": self.income_account if self.income_account else frappe.db.get_value("Company", self.company, "default_income_account"),
			"credit_in_account_currency": self.availed_amount
		})
		journal_entry.set("accounts", accounts)
		journal_entry.save(ignore_permissions = True)
		journal_entry.submit()
		frappe.db.set_value(self.doctype, self.name, "initial_completed_session_jv_reference",journal_entry.name)
		#for saving no of sessions completed without any process
		if self.items:
			for item in self.items:
				if item.completed_session >0:
					frappe.db.set_value(item.doctype, item.name, "initial_completed_session", item.completed_session)

	def get_payment_details(self, unallocated_entry, allocated_amount, transfer_jv):
		party_type = "Customer"
		party_account = get_party_account(party_type, self.customer, self.company)
		party = self.customer
		voucher_detail_no = ''
		journal_entry = frappe.get_doc("Journal Entry", transfer_jv.name)
		return frappe._dict({
			'voucher_type': unallocated_entry.reference_type,
			'voucher_no' : unallocated_entry.reference_name,
			'voucher_detail_no' : voucher_detail_no,
			'against_voucher_type' : journal_entry.voucher_type,
			'against_voucher'  : journal_entry.name,
			'account' : party_account,
			'party_type': party_type,
			'party': party,
			'is_advance' : '',
			'dr_or_cr' : 'credit_in_account_currency',
			'unadjusted_amount' : flt(unallocated_entry.amount),
			'allocated_amount' : flt(allocated_amount),
			'grand_total': journal_entry.total_debit,
			'outstanding_amount': journal_entry.total_debit
		})
	def get_unallocated_payment_entries(self):
		party_type = "Customer"
		party_account = get_party_account(party_type, self.customer, self.company)
		party = self.customer
		party_account_field = "paid_from" if party_type == "Customer" else "paid_to"
		payment_type = "Receive" if party_type == "Customer" else "Pay"
		unallocated_payment_entries = frappe.db.sql("""
				select "Payment Entry" as reference_type, name as reference_name,
				remarks, unallocated_amount as amount
				from `tabPayment Entry`
				where
					{0} = %s and party_type = %s and party = %s and payment_type = %s
					and docstatus = 1 and unallocated_amount > 0
			""".format(party_account_field), (party_account, party_type, party, payment_type), as_dict=1)
		return unallocated_payment_entries
@frappe.whitelist()
def book_all_appointments(appointments_table):
	appointments_table = json.loads(appointments_table)
	for appointment in appointments_table:
		new_appointment = frappe.new_doc("Patient Appointment")
		for key in appointment:
			new_appointment.set(key, appointment[key] if appointment[key] else '')
		new_appointment.status = "Scheduled"
		new_appointment.save(ignore_permissions = True)
		frappe.msgprint(_("Appointment booked"), alert=True)


@frappe.whitelist()
def create_refunding_journal_entry_on_cancel(doc, refundable_amount=False, account_paid_to=False, reference_no=False, reference_date=False, posting_date=False):
	doc = json.loads(doc)
	dict = frappe._dict(doc)
	availed_tax=0
	accounts = []
	# accounts, availed_tax = get_tax_refund(dict, accounts)
	party_type = "Customer"
	party_account = get_party_account(party_type, dict.customer, dict.company)
	journal_entry = frappe.new_doc('Journal Entry')
	journal_entry.voucher_type = 'Journal Entry'
	journal_entry.naming_series = dict.journal_voucher_series
	journal_entry.company = dict.company
	journal_entry.posting_date = posting_date
	debit_account = dict.unavailed_package_balance_account if dict.status == "Expired" else frappe.db.get_value("Healthcare Package", dict.package, "temporary_package_income_account")
	accounts.append({
		"account": debit_account,
		"debit_in_account_currency": refundable_amount
	})
	accounts.append({
		"account": party_account,
		"credit_in_account_currency": refundable_amount,
		"party_type": "Customer",
		"party": dict.customer
	})
	journal_entry.set("accounts", accounts)
	journal_entry.save(ignore_permissions = True)
	journal_entry.submit()
	create_refunding_journal_entry_payment(dict, refundable_amount, account_paid_to, reference_no, reference_date, availed_tax, posting_date)
	if dict.status != "Expired":
		create_journal_entry_refund_transfer(dict, refundable_amount, posting_date)

def create_refunding_journal_entry_payment(dict, refundable_amount, account_paid_to, reference_no, reference_date, availed_tax, posting_date):
	party_type = "Customer"
	party_account = get_party_account(party_type, dict.customer, dict.company)
	journal_entry = frappe.new_doc('Journal Entry')
	journal_entry.voucher_type = 'Journal Entry'
	journal_entry.naming_series = dict.journal_voucher_series
	journal_entry.company = dict.company
	journal_entry.posting_date =  posting_date
	accounts = []
	accounts.append({
		"account": party_account,
		"debit_in_account_currency": refundable_amount,
		"party_type": "Customer",
		"party": dict.customer
	})
	accounts.append({
		"account": account_paid_to,
		"credit_in_account_currency": refundable_amount,
	})
	journal_entry.set("accounts", accounts)
	if reference_no and reference_date:
		journal_entry.cheque_no=reference_no
		journal_entry.cheque_date=reference_date
	journal_entry.save(ignore_permissions = True)
	journal_entry.submit()

def get_tax_refund(dict, accounts):
	total_availed_tax = 0
	tax_amount = 0
	party_account = get_party_account("Customer", dict.customer, dict.company)
	for tax in dict.taxes:
		taxs = frappe._dict(tax)
		tax_amount = taxs.tax_amount - taxs.sales_invoice_tax_amount
		total_availed_tax += taxs.sales_invoice_tax_amount
		accounts.append({
			"account": taxs.account_head,
			"debit_in_account_currency": tax_amount
		})
		accounts.append({
			"account": party_account,
			"credit_in_account_currency": tax_amount,
			"party_type": "Customer",
			"party": dict.customer
		})

	return accounts, total_availed_tax

def create_journal_entry_refund_transfer(dict, refundable_amount,  posting_date):
	journal_entry = frappe.new_doc('Journal Entry')
	journal_entry.voucher_type = 'Journal Entry'
	journal_entry.company = dict.company
	journal_entry.naming_series = dict.journal_voucher_series
	journal_entry.posting_date =  posting_date
	journal_entry.set("accounts", [
		{
			"account": dict.package_advance,
			"debit_in_account_currency": refundable_amount
		},
		{
			"account": frappe.db.get_value("Healthcare Package", dict.package, 'temporary_package_income_account'),
			"credit_in_account_currency": refundable_amount,
		}
	])
	journal_entry.save(ignore_permissions = True)
	journal_entry.submit()

@frappe.whitelist()
def appointment_cancellation(doc, cancellation_source, cancellation_reason, cancelled_by, cancellation_notes):
	doc = json.loads(doc)
	dict = frappe._dict(doc)
	patient_appointments = frappe.get_list("Patient Appointment",{'patient': dict.patient, 'status': ["!=", "Cancelled"], 'invoiced': ["!=", "1"], 'schc_package_assignment': dict.name })
	for appointment in patient_appointments:
		appointment_ref = frappe.get_doc('Patient Appointment', appointment.name)
		appointment_ref.cancellation_source = cancellation_source
		appointment_ref.cancellation_reason = cancellation_reason
		appointment_ref.cancelled_by = cancelled_by
		appointment_ref.cancellation_notes = cancellation_notes
		appointment_ref.save(ignore_permissions = True)
		cancel_appointment = update_status(appointment.name, "Cancelled")

@frappe.whitelist()
def get_mode_of_payment_from_pos_profile():
	mode_of_payment=frappe.db.sql("""
		select si.mode_of_payment
		from `tabPOS Profile` pos left join `tabSales Invoice Payment` si on  si.parent=pos.name left join
		`tabPOS Profile User` pou on pou.parent=pos.name
		where pou.user='{0}'
	""".format(frappe.session.user), as_dict=True)
	mode_of_payment_list = []
	for mode in mode_of_payment:
		mode_of_payment_list.append(mode["mode_of_payment"])
	return mode_of_payment_list if mode_of_payment_list else False

@frappe.whitelist()
def get_payment_tax_amount(doc, paid_amount):
	from frappe.utils import cint, flt
	paid_amount = float(paid_amount)
	current_tax_amount = 0.0
	tax_amount_and_account = {}
	if doc.taxes:
		i = 0
		for tax in doc.taxes:
			if tax.charge_type == "Actual":
				# distribute the tax amount proportionally to each item row
				actual = flt(tax.tax_amount, tax.precision("tax_amount"))
				current_tax_amount = actual

			elif tax.charge_type == "On Net Total":
				current_tax_amount = (tax.rate / 100.0) * paid_amount
			elif tax.charge_type == "On Previous Row Amount":
				current_tax_amount = (tax.rate / 100.0) * \
					doc.get("taxes")[cint(tax.row_id) - 1].tax_amount_for_current_item
			elif tax.charge_type == "On Previous Row Total":
				current_tax_amount = (tax.rate / 100.0) * \
					doc.get("taxes")[cint(tax.row_id) - 1].grand_total_for_current_item
			tax.tax_amount_for_current_item = current_tax_amount

			if i==0:
				tax.grand_total_for_current_item = flt(paid_amount + current_tax_amount)
			else:
				tax.grand_total_for_current_item = \
					flt(doc.get("taxes")[i-1].grand_total_for_current_item + current_tax_amount)
			i += 1
			if tax.account_head in tax_amount_and_account:
				tax_amount_and_account[tax.account_head] += current_tax_amount
			else:
				tax_amount_and_account[tax.account_head] = current_tax_amount
	return tax_amount_and_account

@frappe.whitelist()
def expiring_package():
	from frappe.utils import add_days, today
	try:
		package_list = frappe.db.get_list("Healthcare Package Assignment",
			{'to_date': add_days(today(), -1), 'status': ['not in', 'Completed, Cancelled, Discontinued, Expired'],
			'docstatus': 1})
		for package in package_list:
			package_assignment = frappe.get_doc('Healthcare Package Assignment', package.name)
			if package_assignment.unavailed_package_balance_account and package_assignment.balance_amount>0:
				transfer_balance_to_anavail_account(package_assignment)
			frappe.db.set_value("Healthcare Package Assignment", package.name, 'status', "Expired")
	except Exception:
		frappe.log_error(frappe.get_traceback())

@frappe.whitelist()
def get_patient_taxes_and_charges(patient, company, date = None):
	customer_group, customer = frappe.db.get_value("Patient", patient, ['customer_group', 'customer'])
	taxes_and_chages_template = None

	if customer and customer_group:
		tax_category = frappe.db.get_value("Customer", customer, 'tax_category')
		taxes_and_chages_template = get_tax_template(
											posting_date = date or frappe.utils.nowdate(),
											args = {'tax_type': 'Sales',
													'customer_group': customer_group,
													'company': company,
													'tax_category': tax_category if tax_category else ''
												})
	if not taxes_and_chages_template: # If no tax rule present, fetch default template
		taxes_and_chages_template = frappe.db.get_value('Sales Taxes and Charges Template', {"is_default": 1, "company": company})
	return taxes_and_chages_template

def transfer_balance_to_anavail_account(package):
	availed_tax = 0
	# get_tax_refund(package, [])[1]
	amount = package.package_price -(package.availed_amount+availed_tax)
	if amount > 0:
		create_journal_entry_refund_transfer(package, amount, package.to_date)
		journal_entry = frappe.new_doc('Journal Entry')
		journal_entry.voucher_type = 'Journal Entry'
		journal_entry.naming_series = package.journal_voucher_series
		journal_entry.company = package.company
		journal_entry.posting_date = package.to_date
		accounts.append({
			"account": package.temporary_package_income_account,
			"debit_in_account_currency": amount
		})
		accounts.append({
			"account": package.unavailed_package_balance_account,
			"credit_in_account_currency": amount,
		})
		journal_entry.save(ignore_permissions = True)
		journal_entry.submit()

@frappe.whitelist()
def update_payment_reference(name, posting_date=False, paid_amount=False, mode_of_payment=False, account_paid_to=False, reference_no=False, reference_date=False, child=False):
	doc = frappe.get_doc('Healthcare Package Assignment', name)
	child_doc = frappe.get_doc('Package Payment Reference', child)
	transfer_child = frappe.db.get_value('Package Account Transfer Reference', {"package_account_transfer_reference": child_doc.package_transfer_reference}, 'name')
	fl_paid_amount = 0
	if child_doc.paid_amount:
		fl_paid_amount = float(child_doc.paid_amount)
	# cancel payment
	if child_doc:
		jv_payment = frappe.get_doc('Journal Entry', child_doc.package_payment_reference)
		frappe.db.set_value(child_doc.doctype, child_doc.name, "package_payment_reference", '') #unlink
		jv_payment.cancel()

		jv_transfer = frappe.get_doc('Journal Entry', child_doc.package_transfer_reference)
		frappe.db.set_value(child_doc.doctype, child_doc.name, "package_transfer_reference", '') #unlink
		if transfer_child:
			frappe.db.set_value('Package Account Transfer Reference', transfer_child, "package_account_transfer_reference", '')
		jv_transfer.cancel()
	# reset payment_data in doc
		frappe.db.set_value(doc.doctype, doc.name, "paid_amount", doc.paid_amount-fl_paid_amount)
		frappe.db.set_value(doc.doctype, doc.name, "outstanding_amount", doc.outstanding_amount+fl_paid_amount)
		frappe.db.sql("""delete from `tabPackage Payment Reference` where name = %s""", (child_doc.name))

	if float(paid_amount) > doc.package_price:
		frappe.throw(_("Payment Amount Exceeded the Package Price"))
	else:
		doc.create_journal_entry_payment(paid_amount, account_paid_to, mode_of_payment, reference_no, reference_date, posting_date, jv_payment.name, jv_transfer.name)

def adjust_outstanding_amount_jv(self):
	party_type = "Customer"
	party_account = get_party_account(party_type, self.customer, self.company)
	journal_entry = frappe.new_doc('Journal Entry')
	journal_entry.voucher_type = 'Journal Entry'
	journal_entry.naming_series = self.journal_voucher_series
	journal_entry.company = self.company
	journal_entry.posting_date =  self.from_date
	accounts = []
	accounts.append({
		"account": frappe.db.get_value("Healthcare Package", self.package, 'temporary_package_income_account'),
		"debit_in_account_currency": self.outstanding_amount,
	})
	accounts.append({
		"account": party_account,
		"credit_in_account_currency": self.outstanding_amount,
		"party_type": "Customer",
		"party": self.customer
	})
	journal_entry.set("accounts", accounts)
	journal_entry.save(ignore_permissions = True)
	journal_entry.submit()
	frappe.db.set_value(self.doctype, self.name, "adjust_outstanding_amount_jv",journal_entry.name)

def create_journal_entry_credit_balance_submit(self, paid_amount, posting_date):
	party_type = "Customer"
	party_account = get_party_account(party_type, self.customer, self.company)
	journal_entry = frappe.new_doc('Journal Entry')
	journal_entry.voucher_type = 'Journal Entry'
	journal_entry.naming_series = self.journal_voucher_series
	journal_entry.company = self.company
	journal_entry.posting_date =  posting_date
	accounts = []
	accounts.append({
		"account": self.package_advance,
		"credit_in_account_currency": paid_amount
	})
	accounts.append({
		"account": party_account,
		"debit_in_account_currency": paid_amount,
		"party_type": "Customer",
		"party": self.customer
	})
	journal_entry.set("accounts", accounts)
	journal_entry.save(ignore_permissions = True)
	journal_entry.submit()
	return journal_entry

@frappe.whitelist()
def cancel_existing_payment_reference(child):
	child_doc = frappe.get_doc('Package Payment Reference', child)
	if child_doc and child_doc.docstatus < 2:
		jv_payment = frappe.get_doc('Journal Entry', child_doc.package_payment_reference)
		frappe.db.set_value(child_doc.doctype, child_doc.name, "package_payment_reference", '') #unlink
		jv_payment.cancel() #cancel
		frappe.db.set_value(child_doc.doctype, child_doc.name, "cancelled_payment_reference", jv_payment.name) #linkcancelled

		jv_transfer = frappe.get_doc('Journal Entry', child_doc.package_transfer_reference)
		frappe.db.set_value(child_doc.doctype, child_doc.name, "package_transfer_reference", '') #unlink
		jv_transfer.cancel() #cancel
		frappe.db.set_value(child_doc.doctype, child_doc.name, "cancelled_transfer_reference", jv_transfer.name) #linkcancelled
		frappe.db.set_value(child_doc.doctype, child_doc.name, "jv_status", 'Cancelled')
		#calculating current outstanding_amount
		outstanding_amount = float(frappe.db.get_value("Healthcare Package Assignment", child_doc.parent, 'outstanding_amount')) + float(child_doc.paid_amount)
		paid_amount = float(frappe.db.get_value("Healthcare Package Assignment", child_doc.parent, 'paid_amount')) - float(child_doc.paid_amount)
		frappe.db.set_value(child_doc.parenttype, child_doc.parent, "outstanding_amount", outstanding_amount)
		frappe.db.set_value(child_doc.parenttype, child_doc.parent, "paid_amount", paid_amount)
		status = ''
		if frappe.db.get_value("Healthcare Package Assignment", child_doc.parent, 'outstanding_amount') == 0:
			status="Paid"
		elif frappe.db.get_value("Healthcare Package Assignment", child_doc.parent, 'outstanding_amount') > 0 and frappe.db.get_value("Healthcare Package Assignment", child_doc.parent, 'paid_amount') >0:
			status="Partial"
		else:
			status="Submitted"
		frappe.db.set_value(child_doc.parenttype, child_doc.parent, "status", status)
