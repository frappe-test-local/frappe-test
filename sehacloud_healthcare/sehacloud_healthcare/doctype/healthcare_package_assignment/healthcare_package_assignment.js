// Copyright (c) 2019, SehaCloud and contributors
// For license information, please see license.txt

frappe.ui.form.on('Healthcare Package Assignment', {
	setup:function(frm){
		frm.set_query("account_paid_to", function() {
			var account_types = in_list(["Receive"], "Receive") ?
				["Bank", "Cash"] : [frappe.boot.party_account_types["Customer"]];

			return {
				filters: {
					"account_type": ["in", account_types],
					"is_group": 0,
					"company": frm.doc.company
				}
			}
		});
		frm.set_query("package", function() {
			return {
				filters: {
					is_active: true
				}
			};
		});
		frm.set_query("package_advance", function() {
			return {
				filters: {
					'root_type': 'Liability',
					'company': frm.doc.company,
					"is_group": 0
				}
			};
		});
	},
	onload:function(frm){
		if(frm.doc.__islocal){
			frm.clear_table('package_payment_reference')
			frm.refresh_field('package_payment_reference')
			frm.clear_table('package_account_transfer_reference')
			frm.refresh_field('package_account_transfer_reference')
			frm.set_value('submitted_reference', '');
			frm.set_value('initial_completed_session_jv_reference', '');
			frm.set_value('patient_credit_balance_payment', '');
			frm.set_value('submitted_reference', '');
			frm.set_value('adjust_outstanding_amount_jv', '');
		}
	},
	refresh:function(frm){
		if (frm.doc.docstatus == 1 && frm.doc.outstanding_amount > 0  && frm.doc.status != "Discontinued") {
			frm.add_custom_button(__("Payment"), function() {
				payment(frm)
			}, __("Make"));
			frm.page.set_inner_btn_group_as_primary(__("Make"));
		}
		if (frm.doc.docstatus == 1  && frm.doc.status != "Discontinued" && frm.doc.status != "Completed" &&  frm.doc.status != "Expired" && frm.doc.status != "Unpaid and Completed" && frm.doc.status != "Partial and Completed") {
			frm.add_custom_button(__("Book Appointments"), function() {
				book_appointment(frm)
			});
		}
		if(frm.doc.__islocal){
			frappe.db.get_value('Sales Taxes and Charges Template', {is_default: 1}, 'name', (r) => {
				if(r && r.name){
					frm.set_value("taxes_and_charges",r.name);
				}
			});
		}
		if (frm.doc.status == "Discontinued"  && frm.doc.is_refundable == 1 && frm.doc.paid_amount - frm.doc.availed_amount > 0 && frm.doc.refunded != 1) {
			frm.add_custom_button(__("Refund"), function() {
				refund_process(frm)
			}, __("Make"));
			frm.page.set_inner_btn_group_as_primary(__("Make"));
		}
		if(frappe.user_roles.includes('System Manager') && frm.doc.docstatus != 1){
			frappe.meta.get_docfield('Healthcare Package Item','completed_session', frm.doc.name).read_only = 0
		}
		if(frappe.user_roles.includes('Accounts Manager') && frm.doc.docstatus == 1){
			frappe.meta.get_docfield('Package Payment Reference','edit', frm.doc.name).hidden = 0
			frappe.meta.get_docfield('Package Payment Reference','btn_cancel', frm.doc.name).hidden = 0
		}
		if (frm.doc.docstatus == 1 && frm.doc.status != "Discontinued" && frm.doc.status != "Completed") {
					frm.add_custom_button(__("Discontinue Package"), function() {
						var confirmation_msg = 'Do you want to discontinue ?'
						frappe.confirm( confirmation_msg,function(){
							window.close();
						if (frm.doc.paid_amount >= frm.doc.availed_amount){
						frappe.db.get_list('Patient Appointment', {
							filters: {
								schc_package_assignment: frm.doc.name,
								status:	['not in', ['Cancelled', 'No show']],
								invoiced:  ["!=", '1']
							},
						}).then(records => {
							if(records.length>0){
								cancellation_reasons(frm)
							}
							else{
								frm.set_value("status", "Discontinued");
								frm.save('Update');
							}
						})
						}
						else{
							frappe.throw(__('Cannot discontinue!!! <br/>Paid Amount is lesser than Availed Amount'))
						}
					})
					});
			}
		if (frm.doc.status == "Discontinued") {
		 frm.remove_custom_button('Discontinue Package');
	 	}

	},
	package: function(frm){
		if(frm.doc.package){
			frappe.meta.get_docfield('Healthcare Package Item','item_code', frm.doc.name).read_only = 1
			frappe.meta.get_docfield('Healthcare Package Item','session_rate', frm.doc.name).read_only = 1
			frappe.meta.get_docfield('Healthcare Package Item','healthcare_practitioner_level', frm.doc.name).read_only = 1
			frappe.meta.get_docfield('Healthcare Package Item','item_name', frm.doc.name).read_only = 1
			frappe.meta.get_docfield('Healthcare Package Item','no_of_session', frm.doc.name).read_only = 1
			frappe.call({
				"method": "frappe.client.get",
				args: {
					doctype: "Healthcare Package",
					name: frm.doc.package
				},
				callback: function (data) {
					frm.set_value("package_name", data.message.package_name);
					frm.set_value("department", data.message.department)
					frm.set_value("description", data.message.description);
					frm.set_value("duration", data.message.duration);
					frm.set_value("price_list",data.message.price_list);
					frm.set_value("package_advance",data.message.package_advance);
					frm.set_value("journal_voucher_series",data.message.journal_voucher_series);
					frm.set_value("is_refundable",data.message.is_refundable);
					frm.set_value("appointment_type",data.message.appointment_type);
					if(data.message.refunding_percentage){
						frm.set_value("refunding_percentage", data.message.refunding_percentage);
					}
					if(data.message.refunding_amount){
						frm.set_value("refunding_amount", data.message.refunding_amount);
					}
					if(data.message.refundable_charges_income_account){
						frm.set_value("refundable_charges_income_account", data.message.refundable_charges_income_account);
					}
					if(data.message.income_account){
						frm.set_value("income_account", data.message.income_account);
					}
					if(data.message.unavailed_package_balance_account){
						frm.set_value("unavailed_package_balance_account", data.message.unavailed_package_balance_account);
					}
					set_todate(frm);
					frm.doc.items = [];
					data.message.items.forEach(function(row){
						var item_fields = ['item_code', 'item_name', 'session_rate', 'no_of_session', 'session_amount', 'discount_amount',
							'discount_percentage', 'session_total', 'healthcare_practitioner_level']
						if((row["healthcare_practitioner_level"]==frm.doc.healthcare_practitioner_level)||row["healthcare_practitioner_level"]==''){
							var child_item=frappe.model.add_child(frm.doc, "Healthcare Package Item", "items")
						}
						$.each(item_fields, function(i, field) {
							if((row["healthcare_practitioner_level"]==frm.doc.healthcare_practitioner_level)||row["healthcare_practitioner_level"]==''){
								frappe.model.set_value(child_item.doctype, child_item.name, field, row[field]);
							}
						});
					});
					frm.refresh_fields()
				}
			});
		}
	},
	healthcare_practitioner_level: function(frm){
		if(frm.doc.package && frm.doc.docstatus < 1){
			frappe.call({
				"method": "frappe.client.get",
				args: {
					doctype: "Healthcare Package",
					name: frm.doc.package
				},
				callback: function (data) {
					frm.doc.items = [];
					data.message.items.forEach(function(row){
						var item_fields = ['item_code', 'item_name', 'session_rate', 'no_of_session', 'session_amount', 'discount_amount',
							'discount_percentage', 'session_total', 'healthcare_practitioner_level']
						if(row["healthcare_practitioner_level"]==frm.doc.healthcare_practitioner_level || row["healthcare_practitioner_level"]==''){
							var child_item=frappe.model.add_child(frm.doc, "Healthcare Package Item", "items")
						}
						$.each(item_fields, function(i, field) {
							if(row["healthcare_practitioner_level"]==frm.doc.healthcare_practitioner_level || row["healthcare_practitioner_level"]==''){
								frappe.model.set_value(child_item.doctype, child_item.name, field, row[field]);
							}
						});
						frm.refresh_fields()
					});
				}
			});
		}
	},
	from_date:function(frm){
		set_todate(frm);
	},
	taxes_and_charges: function(frm) {
		if(frm.doc.items){
			calculate_taxe_details(frm);
		}
	},
	patient:function(frm){
		if (frm.doc.patient){
			frappe.call({
				method: 'sehacloud_healthcare.sehacloud_healthcare.doctype.healthcare_package_assignment.healthcare_package_assignment.get_patient_taxes_and_charges',
				args: {
					'patient': frm.doc.patient,
					'company': frm.doc.company,
					'date':  frm.doc.from_date || frappe.datetime.now_datetime()
				},
				callback: (r) => {
					if(r && r.message){
						frm.set_value('taxes_and_charges', r.message);
					}
					else{
						frm.set_value('taxes_and_charges', '');
						frm.clear_table('taxes')
						frm.refresh_fields();
					}
				},
				freeze: true
			})
		}
	},
	customer:function(frm){
		if (frm.doc.patient){
			frappe.call({
				method: 'erpnext.accounts.utils.get_balance_on',
				args: {
					'party_type': "Customer",
					'party': frm.doc.customer
				},
				callback: (r) => {
					if(r && r.message && r.message < 0){
						frm.set_value('patient_credit_balance', Math.abs(r.message));
						frm.set_df_property("allocate_patient_credit_balance", "read_only", 0);
						frm.set_value("allocate_patient_credit_balance", 1);
					}
					else{
						frm.set_value('patient_credit_balance', 0);
						frm.set_df_property("allocate_patient_credit_balance", "read_only", 1);
						frm.set_value("allocate_patient_credit_balance", 0);
					}
				},
				freeze: true
			})
		}
	}
});
var set_todate = function(frm){
	if(frm.doc.from_date){
		var end_date = frappe.datetime.add_days(frm.doc.from_date, frm.doc.duration-1);
		frm.set_value("to_date", end_date);
	}
};

	var book_appointment = function(frm) {
		let selected_practitioner = '';
		let selected_department = '';
		let selected_service_unit = '';
		var d = new frappe.ui.Dialog({
			title: __("Book Appointments"),
			fields: [
				{ fieldtype: 'Link', options: 'Medical Department', reqd:1, fieldname: 'department', label: 'Medical Department',
				get_query: function () {
					return {
						filters: {
							"is_group": 0,
						}
					};
				}
				},
				{ fieldtype: 'Link', options: 'Appointment Type', reqd:1, fieldname: 'appointment_type', label: 'Appointment Type'},
				{ fieldtype: 'Int', fieldname: 'duration', label: 'Duration', read_only:  1},
				{ fieldtype: 'Link', options: 'Item', reqd:1, fieldname: 'schc_package_item', label: 'Package Item', reqd: true,
				get_query: function () {
					return {
						query : "sehacloud_healthcare.queries.get_package_item_list",
						filters: {parent :frm.doc.package}
					};
				}
				},
				{ fieldtype: 'Data', read_only:1, fieldname: 'item_name', depends_on:'eval:doc.schc_package_item', label: 'Item Name'},
				{ fieldtype: 'Int', read_only:1, fieldname: 'booked_appointments', label: 'Booked Appointments'},
				{ fieldtype: 'Column Break'},
				{ fieldtype: 'Link', options: 'Healthcare Practitioner', reqd:1, fieldname: 'practitioner', label: 'Healthcare Practitioner'},
				{ fieldtype: 'Select', options: '\nDirect\nReferral\nExternal Referral', fieldname: 'source', label: 'Source', reqd: true},
				{ fieldtype: 'Link', options: 'Healthcare Practitioner', fieldname: 'referring_practitioner', label: 'Referring Practitioner',
					depends_on:'eval:doc.source != `Direct`'},
				{ fieldtype: 'Link', options: 'Healthcare Service Unit', fieldname: 'service_unit', label: 'Service Unit',
					get_query: function () {
						return {
							filters: {
								"is_group": 0,
								"allow_appointments": 1
							}
						};
					}
				},
				{ fieldtype: 'Data', read_only:1, fieldname: 'remaining_appointment', label: 'Remaining Appointments'},
				{ fieldtype: 'Section Break'},
				{ fieldtype: 'HTML', fieldname: 'slots_html'}
			],
			primary_action_label: __("Book"),
			primary_action: function() {
				d.get_primary_btn().attr('disabled', true);
				var check_list = frm.procedure_templates_check_list.get_procedure_templates();
				var appointments_table = [];
				if(check_list && check_list.length > 0){
					$.each(check_list, function(i, appointment) {
						let appointment_time = appointment.start.slice(11);
						let appointment_date = appointment.start.slice(0, 10);
						appointments_table.push({
							"patient": frm.doc.patient,
							"department": d.get_value('department'),
							"practitioner": d.get_value('practitioner'),
							"source":d.get_value('source'),
							"referring_practitioner":d.get_value('referring_practitioner'),
							"appointment_type":d.get_value('appointment_type'),
							"service_unit":d.get_value('service_unit') ? d.get_value('service_unit') : appointment.service_unit,
							"company": frm.doc.company,
							"appointment_date": appointment_date,
							"appointment_time": appointment_time,
							"duration": d.get_value('duration') ? d.get_value('duration') : appointment.duration,
							"schc_package_assignment": frm.doc.name,
							"schc_package_item": d.get_value('schc_package_item'),
							"sch_healthcare_practitioner_level":frm.doc.healthcare_practitioner_level? frm.doc.healthcare_practitioner_level:'',
							"practitioner_event": appointment.practitioner_event ? appointment.practitioner_event : ''
						});
					})
					// add to booking table
				}
				if(appointments_table){
					frappe.call({
						method: "sehacloud_healthcare.sehacloud_healthcare.doctype.healthcare_package_assignment.healthcare_package_assignment.book_all_appointments",
						args:{
							appointments_table: appointments_table
						},
						callback: function(data) {
							if(!data.exc){
								d.hide();
								$(me.wrapper).empty();
								frm.refresh_fields();
							}
						},
						freeze: true,
						freeze_message: __("Booking Appointment......")
					});
				}
			}
		});
		frappe.db.get_value("Healthcare Settings", "", "default_practitioner_source", function(r) {
			if(r && r.default_practitioner_source){
				d.set_value("source", r.default_practitioner_source)
			}
			else{
				d.set_value("source", "");
			}
		});
		if(frm.doc.department){
			d.set_value("department", frm.doc.department)
			d.set_df_property("department", "read_only", 1);
		}
		else{
			d.set_value("department", "");
			d.set_df_property("department", "read_only", 0);
		}

		frappe.call({
			method: "sehacloud_healthcare.sehacloud_healthcare.doctype.healthcare_package.healthcare_package.get_package_item_list",
			args: {package: frm.doc.name},
			callback: function(r) {
				if(r && r.message && r.message.length == 1){
					d.set_value("schc_package_item", r.message[0][0]);
				}
			}
		});
		if(frm.doc.appointment_type){
			d.set_value("appointment_type", frm.doc.appointment_type);
			d.set_df_property("appointment_type", "read_only", 1);
		}
		else{
			d.set_value("appointment_type", "");
			d.set_df_property("appointment_type", "read_only", 0);
		}
		d.fields_dict["appointment_type"].df.onchange = () => {
			frappe.db.get_value("Appointment Type", d.get_value('appointment_type'), 'default_duration', function(r) {
				if(r && r.default_duration){
					d.set_values({
						'duration': r.default_duration
					});
				}
			});
		}
		d.fields_dict["department"].df.onchange = () => {
			if(d.get_value('department') && d.get_value('department') != selected_department){
				d.set_values({
					'practitioner': ''
				});
				selected_department = d.get_value('department');
				if(selected_department){
					d.fields_dict.practitioner.get_query = function() {
						return {
							filters: {
								"department": selected_department
							}
						};
					};
				}
			if (frm.doc.healthcare_practitioner_level){
				d.fields_dict.practitioner.get_query = function() {
					return {
						filters: {
							"sch_healthcare_practitioner_level": frm.doc.healthcare_practitioner_level,
							"department": d.get_value('department')
						}
					};
				};
			}
		}
		};
		d.fields_dict["schc_package_item"].df.onchange = () => {
			if(d.get_value('schc_package_item')){
				$.each(frm.doc.items, function(j, item){
					if(item.item_code == d.get_value('schc_package_item')){
						frappe.db.get_list('Patient Appointment', {
							filters: {
								schc_package_assignment: frm.doc.name,
								schc_package_item: d.get_value('schc_package_item'),
								status: ['!=', "Cancelled"]
							}
						}).then(records => {
							d.set_values({
								booked_appointments: records.length,
								remaining_appointment: item.no_of_session - records.length - item.initial_completed_session
							});
							var varble = 0;
							$.each(records, function(k, l){
								frappe.db.get_value("Patient Appointment", l.name, ['status','invoiced'], function(r) {
									if(r){
										if((r.status == 'No show' || r.status == 'Pending') && r.invoiced == 0){
											varble = varble+1
											d.set_values({
												booked_appointments: records.length-varble,
												remaining_appointment: item.no_of_session - (records.length-varble) - item.initial_completed_session
											});
										}
									}
								});
							});
						})
					}
				});
				frappe.db.get_value("Item", d.get_value('schc_package_item'), 'item_name', function(r) {
					if(r && r.item_name){
						d.set_values({
							'item_name': r.item_name
						});
					}
				});
			}
			else{
				d.set_values({
					booked_appointments: '',
					remaining_appointment: ''
				});
			}
		};
		d.fields_dict["source"].df.onchange = () => {
			if(d.get_value('source')=="Direct"){
					d.set_value("referring_practitioner", "");
			}
			if(d.get_value('source')=="Referral"){
				frappe.db.get_value("Healthcare Practitioner", d.get_value('practitioner'), 'healthcare_practitioner_type', function(r) {
					if(r && r.healthcare_practitioner_type && r.healthcare_practitioner_type=="Internal"){
						d.set_values({
							'referring_practitioner': d.get_value('practitioner')
						});
					}
					else{
						d.set_values({
							'referring_practitioner': ""
						});
					}
				});
			}
			else{
				d.set_values({
					'referring_practitioner': ""
				});
			}
		};

		var fd = d.fields_dict;
		d.fields_dict["practitioner"].df.onchange = () => {
			if(d.get_value('practitioner') && d.get_value('practitioner') != selected_practitioner){
				selected_practitioner = d.get_value('practitioner');
				d.fields_dict.slots_html.html("");
				var procedure_template_area = $('<div class="appointment_slots" style="min-height: 10px">')
				.appendTo(d.fields_dict.slots_html.wrapper);
				frm.procedure_templates_check_list = new frappe.ProcedureTemplateCheckList(procedure_template_area, frm, 0, d);
			}
			else if(!d.get_value('practitioner')) {
				d.fields_dict.slots_html.html(__("Select Healthcare Practitioner to begin.").bold());
			}
		};
		d.fields_dict["service_unit"].df.onchange = () => {
			if(selected_service_unit && d.get_value('service_unit') != selected_service_unit){
				selected_service_unit = d.get_value('service_unit');
				d.fields_dict.slots_html.html("");
				var procedure_template_area = $('<div class="appointment_slots" style="min-height: 10px">')
				.appendTo(d.fields_dict.slots_html.wrapper);
				frm.procedure_templates_check_list = new frappe.ProcedureTemplateCheckList(procedure_template_area, frm, 0, d);
			}
			else if(!d.get_value('practitioner')) {
				d.fields_dict.slots_html.html(__("Select Healthcare Practitioner to begin.").bold());
			}
			selected_service_unit = d.get_value('service_unit');
		};
		// disable dialog action
		d.get_primary_btn().attr('disabled', true);
		d.show();
		}
		var get_payment_mode_account = function(frm, mode_of_payment, callback) {
			if(!frm.doc.company) {
				frappe.throw(__("Please select the Company first"));
			}
			if(!mode_of_payment) {
				return;
			}
			return  frappe.call({
				method: "erpnext.accounts.doctype.sales_invoice.sales_invoice.get_bank_cash_account",
				args: {
					"mode_of_payment": mode_of_payment,
					"company": frm.doc.company
				},
				callback: function(r, rt) {
					if(r.message) {
						callback(r.message.account)
					}
				}
			});
		}

	frappe.ProcedureTemplateCheckList = Class.extend({
		init: function(wrapper, frm, disable, d) {
			var me = this;
			this.frm = frm;
			this.wrapper = wrapper;
			this.disable = disable;
			$(wrapper).html('<div class="help">' + __("Loading") + '...</div>');
			frappe.db.get_value("Healthcare Settings", "", "appointment_administrator", function(r) {
				if(r && r.appointment_administrator){
					me.show_procedure_templates(frm, d, r.appointment_administrator);
				}
				else{
					me.show_procedure_templates(frm, d, '');
				}
			});
		},
		show_procedure_templates: function(frm, d, appointment_administrator) {
			var me = this;
			frappe.call({
				method: "sehacloud_healthcare.sehacloud_healthcare.utils.get_events",
				args: {
					start: frappe.datetime.now_date(),
					end: frappe.datetime.add_days(frappe.datetime.now_date(), 14) > frm.doc.to_date ? frm.doc.to_date : frappe.datetime.add_days(frappe.datetime.now_date(), 14),
					filters: {"practitioner": d.get_value('practitioner'), "service_unit": d.get_value('service_unit')||""}
				},
				callback: function(r) {
					$(me.wrapper).empty();
					if(r.message && r.message.length > 0){
						var slot_html = "";
						var slot_date = "";
						var j = 0;

						$.each(r.message, function(i, template) {
							var template_display = __(template.start.slice(11))
							var disabled = "";
							if(template.doctype == "Patient Appointment"){
								if(template.service_unit){
									if(template.overlap_appointments == 1){
										disabled = "";
									}
									else{
										disabled = "disabled";
										template_display = template_display.fontcolor( "#bab8b8" )
									}
								}
								else{
									disabled = "disabled";
									template_display = template_display.fontcolor( "#bab8b8" )
								}
							}
							if(template.doctype == "None"){
									disabled = "disabled";
									template_display = template_display.strike()
									template_display = template_display.fontcolor( "#bab8b8" )
							}
							if((template.start) < frappe.datetime.now_datetime() && !frappe.user.has_role(appointment_administrator)){
								disabled = "disabled";
								template_display = template_display.fontcolor( "#bab8b8" )
							}
							if(slot_date != template.start.slice(0, 10)){
								var background_color = "";
								if(j%2 == 0){
									background_color = "#F5F5F5";
								}
								j += 1;
								if(slot_date){
									slot_html += '</div></div>'
								}
								slot_date = template.start.slice(0, 10);
								var months = ["January", "February", "March", "April", "May", "June",
								"July", "August", "September", "October", "November", "December"];
								var days = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];
								var date_month = months[frappe.datetime.str_to_obj(template.start).getMonth()];
								var date_year = frappe.datetime.str_to_obj(template.start).getFullYear();
								var date_date = frappe.datetime.str_to_obj(template.start).getDate();
								var date_day = days[frappe.datetime.str_to_obj(template.start).getDay()];
								slot_html += repl(`<div class="col-md-12 col-sm-12"><div class="procedure-template-date height_min col-md-2 col-sm-2">
								<time  class="icon">
								<em>%(date_month)s %(date_year)s</em>
								<strong>%(date_day)s</strong>
								<span>%(date_date)s</span>
								</time>
								</div>`,
								{date_month: date_month, date_year: date_year, date_day: date_day, date_date: date_date})
								slot_html += repl(`<div class="procedure-template-slots height_min col-md-10 col-sm-10" 	style="background-color:%(background_color)s">`,
								{background_color: background_color})
							}
							slot_html += repl('<div class="procedure-template col-md-4 col-sm-4" data-toggle="tooltip" title="%(tooltip_title)s" \
							start="%(start)s" duration="%(duration)s" appointment_type="%(appointment_type)s" service_unit="%(service_unit)s"  event_name="%(event_name)s">\
							<input type="checkbox" style="margin-top:15px; class="box" %(disabled)s> \
						 	%(template_display)s </input></div>',
							{
								start: template.start, template_display:template_display, disabled: disabled, duration:template.duration,
								appointment_type: template.appointment_type || '', service_unit: template.service_unit || '', event_name:template.name, tooltip_title:template.title
							});
						});
						$(me.wrapper).append(slot_html)
						$(me.wrapper).find('input[type="checkbox"]').change(function() {
							me.dialog_btn_disable_enable(frm, d);
						});
					}
					else{
						d.fields_dict.slots_html.html(__("There is no practitioner event for {0}", [d.get_value('practitioner')]).bold());
					}
				},
				freeze: true
			});
			me.dialog_btn_disable_enable(frm, d);
		},
		dialog_btn_disable_enable: function(frm, d) {
			var me = this;
			var opts = me.get_procedure_templates();
			var remaining_appointment = 0;
			$.each(frm.doc.items, function(j, item){
				if(item.item_code == d.get_value('schc_package_item')){
					var booked_appointments = 0;
					if(d.get_value('booked_appointments')){
						booked_appointments = d.get_value('booked_appointments')
					}
					remaining_appointment = item.no_of_session - booked_appointments - item.initial_completed_session;
				}
			});
			if(opts && opts.length > 0 && remaining_appointment >= opts.length){
				// enable dialog action
				d.get_primary_btn().attr('disabled', null);
				d.set_values({
					remaining_appointment: remaining_appointment - opts.length
				});
			}
			else{
				if(opts){
					d.set_values({
						remaining_appointment: remaining_appointment - opts.length
					});
				}
				// disable dialog action
				d.get_primary_btn().attr('disabled', true);
				if(remaining_appointment < opts.length){
					d.set_values({
						remaining_appointment: __('Limit exceed with {0}', [opts.length - remaining_appointment])
					});
				}
			}
		},
		get_procedure_templates: function() {
			var checked_templates = [];
			$(this.wrapper).find('[start]').each(function() {
				if($(this).find('input[type="checkbox"]:checked').length) {
					checked_templates.push({"start": $(this).attr('start'), "duration": $(this).attr('duration'),
						"appointment_type": $(this).attr('appointment_type') || '', "service_unit": $(this).attr('service_unit') || '', "practitioner_event":$(this).attr('event_name') || ''});
				}
			});
			return checked_templates;
		}
	});

	var payment = function(frm) {
		frappe.call({
			method: "sehacloud_healthcare.sehacloud_healthcare.doctype.healthcare_package_assignment.healthcare_package_assignment.get_mode_of_payment_from_pos_profile",
			callback: function(r) {
				var mode_of_payment_filter = (r.message && r.message.length>0) ? r.message : false
				var d = new frappe.ui.Dialog({
					title: __("Amount To Be Paid"),
					fields: [
						{ fieldtype: 'Date', fieldname: 'posting_date', label: 'Posting Date', default: frappe.datetime.now_date() },
						{ fieldtype: 'Data', fieldname: 'paid_amount', label: 'Paid Amount'},
						{ fieldtype: 'Link', fieldname: 'mode_of_payment', label: 'Mode of Payment',  options: 'Mode of Payment',
							get_query: function(){
								return{
									filters: mode_of_payment_filter ? {name: ["in", mode_of_payment_filter]} : {}
								};
							}
						},
						{ fieldtype: 'Data', fieldname: 'mode_of_payment_type', label: 'Payment Mode Type', hidden:1},
						{ fieldtype: 'Data', fieldname: 'reference_no', label: 'Cheque/Reference No', depends_on:'eval:doc.mode_of_payment_type == `Bank`'},
						{ fieldtype: 'Date', fieldname: 'reference_date', label: 'Cheque/Reference Date', depends_on:'eval:doc.mode_of_payment_type == `Bank`'},
						{ fieldtype: 'Column Break'},
						{ fieldtype: 'Data', read_only:1, fieldname: 'package_price', label: 'Package Price'},
						{ fieldtype: 'Data', read_only:1, fieldname: 'balance_due', label: 'Balance Due'},
						{ fieldtype: 'Link', read_only:1, fieldname: 'account_paid_to', label: 'Account Paid To',  options: 'Account'}
					],
					primary_action_label: __("Pay"),
					primary_action: function() {
						if(d.get_value('mode_of_payment')){
							frappe.model.get_value('Mode of Payment', {'name':d.get_value('mode_of_payment')}, 'type',
								function(data) {
									if(data.type != 'Cash' && !(d.get_value('paid_amount') > frm.doc.outstanding_amount)){
										if(d.get_value('reference_no') == '' || d.get_value('reference_date') == ''){
											frappe.throw(__("Cannot Pay, not all mandatory fields are set."))
										}
										payment_confirmation(frm, d)
									}
									else if (data.type == 'Cash' && !(d.get_value('paid_amount') > frm.doc.outstanding_amount)) {
										payment_confirmation(frm, d)
									}
									else if(d.get_value('paid_amount') > frm.doc.outstanding_amount){
										frappe.throw(__("Cannot Pay, Incorrect Amount"))
									}
								}
							);
						}
					}
				});
				d.show();
				d.set_values({
					'paid_amount': frm.doc.outstanding_amount,
					'package_price': frm.doc.package_price,
					'balance_due': frm.doc.outstanding_amount
				});
				var fd = d.fields_dict;
				d.fields_dict["paid_amount"].df.onchange = () => {
					if(d.get_value('paid_amount')){
						var amount_remaining = frm.doc.outstanding_amount - (d.get_value('paid_amount'));
						d.set_values({
							'balance_due': amount_remaining
						});
					}
				}
				d.fields_dict["mode_of_payment"].df.onchange = () => {
					// if(d.get_value('mode_of_payment') != 'Cash'){
					// 	d.set_df_property("reference_no", "reqd", 1);
					// 	d.set_df_property("reference_date", "reqd", 1);
					// }
					// else{
					// 	d.set_df_property("reference_no", "reqd", 0);
					// 	d.set_df_property("reference_date", "reqd", 0);
					// }
					if(d.get_value('mode_of_payment')){
						var mode_of_payment = d.get_value('mode_of_payment');
						get_payment_mode_account(frm, mode_of_payment, function(account){
							d.set_values({
								'account_paid_to': account
							});
						});
						frappe.model.get_value('Mode of Payment', {'name':d.get_value('mode_of_payment')}, 'type',
							function(data) {
								if(data.type){
									d.set_values({
										'mode_of_payment_type': data.type
									});
								}
							}
						);
					}
				}
			}
		});
	};

	var payment_confirmation = function(frm,d){
		var confirmation_msg = `<b>Amount Paid</b>:	${d.get_value('paid_amount')} &nbsp;&nbsp; <b>Balance</b>: ${d.get_value('balance_due')||0} &nbsp;&nbsp; <b>Mode Of Payment</b>: ${d.get_value('mode_of_payment')}
			<br/><br/><b>Do you want to confirm ?</b>`
		frappe.confirm(
			confirmation_msg,
			function(){
				window.close();
				d.hide();
				return frappe.call({
					method: "create_journal_entry_payment",
					doc: frm.doc,
					args: {
						paid_amount :	d.get_value('paid_amount'),
						account_paid_to: d.get_value('account_paid_to'),
						mode_of_payment: d.get_value('mode_of_payment'),
						reference_no: d.get_value('reference_no'),
						reference_date: d.get_value('reference_date'),
						posting_date: d.get_value('posting_date')
					},
					callback: function(r) {
						frappe.show_alert({message:__("Payment Successfully"), indicator:'green'});
						frm.refresh();
					},
					freeze: true,
					freeze_message: __("Payment Processing....")
				});
			}
		);
	}

	var refund_process = function(frm) {
		var refund_amount = "";
		if (frm.doc.refunding_percentage){
			refund_amount = (frm.doc.paid_amount - frm.doc.availed_amount)-(frm.doc.paid_amount*0.01*frm.doc.refunding_percentage)
		}
		else if (frm.doc.refunding_amount){
			refund_amount = (frm.doc.paid_amount - frm.doc.availed_amount) - frm.doc.refunding_amount
		}
		else {
			refund_amount = frm.doc.paid_amount - frm.doc.availed_amount
		}
		var tax_amt =0;
		var sales_tax =0;
		frm.doc.taxes.forEach(function(val, i){
			tax_amt += val.tax_amount
			sales_tax += val.sales_invoice_tax_amount
		});
		var tax_result = tax_amt - sales_tax
		var d = new frappe.ui.Dialog({
			title: __("Amount To Be Paid"),
			fields: [
					{ fieldtype: 'Date', fieldname: 'posting_date', label: 'Posting Date', default: frappe.datetime.now_date() },
				{ fieldtype: 'Link', fieldname: 'mode_of_payment', label: 'Mode of Payment',  options: 'Mode of Payment'},
				{ fieldtype: 'Data', fieldname: 'mode_of_payment_type', label: 'Payment Mode Type', hidden:1},
				{ fieldtype: 'Data', fieldname: 'mode_of_payment_type', label: 'Source of Cancellation', hidden:1},
				{ fieldtype: 'Data', fieldname: 'reference_no', label: 'Cheque/Reference No', depends_on:'eval:doc.mode_of_payment_type == `Bank`'},
				{ fieldtype: 'Date', fieldname: 'reference_date', label: 'Cheque/Reference Date', depends_on:'eval:doc.mode_of_payment_type == `Bank`'},
				{ fieldtype: 'Column Break'},
				{ fieldtype: 'Data', read_only:1, fieldname: 'refundable_amount', label: 'Refundable Amount'},
				{ fieldtype: 'Link', read_only:1, fieldname: 'account_paid_to', label: 'Account Paid To',  options: 'Account'}
			],
			primary_action_label: __("Pay"),
			primary_action: function() {
				if(d.get_value('mode_of_payment')){
					frappe.model.get_value('Mode of Payment', {'name':d.get_value('mode_of_payment')}, 'type',
						function(data) {
							if(data.type != 'Cash'){
								if(d.get_value('reference_no') == '' || d.get_value('reference_date') == ''){
									frappe.throw(__("Cannot Pay, not all mandatory fields are set."))
								}
								refund_payment_confirmation(frm, d, refund_amount)
							}
							else {
								refund_payment_confirmation(frm, d, refund_amount)
							}
						}
					);
				}
			}
		});
		d.show();
			d.set_values({
				'refundable_amount': refund_amount - sales_tax,
			});
			var fd = d.fields_dict;
			d.fields_dict["mode_of_payment"].df.onchange = () => {
			if(d.get_value('mode_of_payment')){
				var mode_of_payment = d.get_value('mode_of_payment');
				get_payment_mode_account(frm, mode_of_payment, function(account){
					d.set_values({
						'account_paid_to': account
					});
				});
				frappe.model.get_value('Mode of Payment', {'name':d.get_value('mode_of_payment')}, 'type',
					function(data) {
						if(data.type){
							d.set_values({
								'mode_of_payment_type': data.type
							});
						}
					}
				);
			}
		}
	};

	var refund_payment_confirmation = function(frm, d, refund_amount){
		d.hide();
		frappe.call({
			method: "sehacloud_healthcare.sehacloud_healthcare.doctype.healthcare_package_assignment.healthcare_package_assignment.create_refunding_journal_entry_on_cancel",
			args: {
				doc:frm.doc,
				refundable_amount :	d.get_value('refundable_amount'),
				account_paid_to: d.get_value('account_paid_to'),
				// mode_of_payment: d.get_value('mode_of_payment'),
				reference_no: d.get_value('reference_no'),
				reference_date: d.get_value('reference_date'),
				posting_date: d.get_value('posting_date')
			},
			callback: function(r) {
				frappe.show_alert({message:__("Payment Successfully"), indicator:'green'});
				frm.refresh();
				frm.set_value("refunded", 1);
				frm.save('Update');
			},
			freeze: true,
			freeze_message: __("Payment Processing....")
		});
		}

	var cancellation_reasons = function(frm) {
		frappe.model.with_doctype("Patient Appointment", function() {
		var cancelled_by_options = frappe.meta.get_docfield("Patient Appointment", "cancelled_by").options
		var d_reasons = new frappe.ui.Dialog({
			title: __("Cancellation Reasons"),
			fields: [
				{ fieldtype: 'Select', options: '\nPatient\nPractitioner\nFacility', reqd:1, fieldname: 'cancellation_source', label: 'Cancellation Source'},
				{ fieldtype: 'Link', options: 'Appointment Cancellation Reason', reqd:1, fieldname: 'cancellation_reason', label: 'Cancellation Reason',
					get_query:function () {
						return {
							filters: {
								"cancellation_source": d_reasons.get_value('cancellation_source')
							}
						};
					}
				},
				{ fieldtype: 'Column Break'},
				{ fieldtype: 'Select', options: cancelled_by_options, fieldname: 'cancelled_by', label: 'Cancelled By'},
				{ fieldtype: 'Small Text', fieldname: 'cancellation_notes', label: 'Cancellation Notes'},
			],
			primary_action_label: __("Cancel"),
			primary_action: function() {
				d_reasons.hide();
				d_reasons.get_primary_btn().attr('disabled', true);
				frappe.call({
					method:
					"sehacloud_healthcare.sehacloud_healthcare.doctype.healthcare_package_assignment.healthcare_package_assignment.appointment_cancellation",
					args: {
							doc:frm.doc,
						 	cancellation_source: d_reasons.get_value('cancellation_source'),
						  cancellation_reason: d_reasons.get_value('cancellation_reason'),
							cancelled_by:	d_reasons.get_value('cancelled_by'),
							cancellation_notes: d_reasons.get_value('cancellation_notes')
								},
				});
				frm.set_value("status", "Discontinued");
				frm.save('Update');
			}
		});
			// disable dialog action initially
			d_reasons.get_primary_btn().attr('disabled', true);
			d_reasons.show();
			d_reasons.$wrapper.find('.modal-dialog').css("width", "800px");

			d_reasons.fields_dict["cancellation_reason"].df.onchange = () => {
				d_reasons.get_primary_btn().attr('disabled', null);
			}
		});
		}
//taxes
var calculate_taxe_details = function(frm){
	if(frm.doc.taxes_and_charges) {
		return frm.call({
			method: "erpnext.controllers.accounts_controller.get_taxes_and_charges",
			args: {
				"master_doctype": "Sales Taxes and Charges Template",
				"master_name": frm.doc.taxes_and_charges,
			},
			callback: function(r) {
				if(!r.exc) {
					frm.set_value("taxes", r.message);
					if(frm.doc.items && frm.doc.items[0].item_code){
						initialize_taxes(frm);
						determine_exclusive_rate(frm);
						calculate_taxes(frm);
						calculate_totals(frm);
						_cleanup(frm);
					}
					frm.refresh_fields();
				}
			}
		});
	}
}

var calculate_taxes = function(frm) {
	frm.doc.rounding_adjustment = 0;
	var actual_tax_dict = {};

	// maintain actual tax rate based on idx
	$.each(frm.doc["taxes"] || [], function(i, tax) {
		if (tax.charge_type == "Actual") {
			actual_tax_dict[tax.idx] = flt(tax.tax_amount, precision("tax_amount", tax));
		}
	});

	$.each(frm.doc["items"] || [], function(n, item) {
		var item_tax_map = _load_item_tax_rate(item.session_rate);
		$.each(frm.doc["taxes"] || [], function(i, tax) {
			// tax_amount represents the amount of tax for the current step
			var current_tax_amount = get_current_tax_amount(frm, item, tax, item_tax_map);

			// Adjust divisional loss to the last item
			if (tax.charge_type == "Actual") {
				actual_tax_dict[tax.idx] -= current_tax_amount;
				if (n == frm.doc["items"].length - 1) {
					current_tax_amount += actual_tax_dict[tax.idx];
				}
			}

			// accumulate tax amount into tax.tax_amount
			if(tax.charge_type == "Actual"){
				tax.tax_amount = current_tax_amount;
			}
			else{
				tax.tax_amount += current_tax_amount;
			}

			// store tax_amount for current item as it will be used for
			// charge type = 'On Previous Row Amount'
			tax.tax_amount_for_current_item = current_tax_amount;

			// tax amount after discount amount
			tax.tax_amount_after_discount_amount += current_tax_amount;

			// for buying
			if(tax.category) {
				// if just for valuation, do not add the tax amount in total
				// hence, setting it as 0 for further steps
				current_tax_amount = (tax.category == "Valuation") ? 0.0 : current_tax_amount;

				current_tax_amount *= (tax.add_deduct_tax == "Deduct") ? -1.0 : 1.0;
			}

			// note: grand_total_for_current_item contains the contribution of
			// item's amount, previously applied tax and the current tax on that item
			if(i==0) {
				tax.grand_total_for_current_item = flt(item.session_amount + current_tax_amount);
			} else {
				tax.grand_total_for_current_item =
					flt(frm.doc["taxes"][i-1].grand_total_for_current_item + current_tax_amount);
			}

			// set precision in the last item iteration
			if (n == frm.doc["items"].length - 1) {
				round_off_totals(tax);

				// in tax.total, accumulate grand total for each item
				set_cumulative_total(frm, i, tax);
			}
		});
	});
}

var set_cumulative_total = function(frm, row_idx, tax) {
	var tax_amount = tax.tax_amount_after_discount_amount;
	if (tax.category == 'Valuation') {
		tax_amount = 0;
	}

	if (tax.add_deduct_tax == "Deduct") { tax_amount = -1*tax_amount; }

	if(row_idx==0) {
		tax.total = flt(frm.doc.rate + tax_amount, precision("total", tax));
	} else {
		tax.total = flt(frm.doc["taxes"][row_idx-1].total + tax_amount, precision("total", tax));
	}
}

var round_off_totals = function(tax) {
	tax.tax_amount = flt(tax.tax_amount, precision("tax_amount", tax));
	tax.tax_amount_after_discount_amount = flt(tax.tax_amount_after_discount_amount, precision("tax_amount", tax));
}

var get_current_tax_amount = function(frm, item, tax, item_tax_map) {
	var tax_rate = _get_tax_rate(tax, item_tax_map);
	var current_tax_amount = 0.0;

	if(tax.charge_type == "Actual") {
		// distribute the tax amount proportionally to each item row
		var actual = flt(tax.tax_amount, precision("tax_amount", tax));
		current_tax_amount = frm.doc.rate ?
			((item.session_total / frm.doc.rate) * actual) : 0.0;

	} else if(tax.charge_type == "On Net Total") {
		current_tax_amount = (tax_rate / 100.0) * item.session_total;
	} else if(tax.charge_type == "On Previous Row Amount") {
		current_tax_amount = (tax_rate / 100.0) *
			frm.doc["taxes"][cint(tax.row_id) - 1].tax_amount_for_current_item;

	} else if(tax.charge_type == "On Previous Row Total") {
		current_tax_amount = (tax_rate / 100.0) *
			frm.doc["taxes"][cint(tax.row_id) - 1].grand_total_for_current_item;
	}
	set_item_wise_tax(item, tax, tax_rate, current_tax_amount);

	return current_tax_amount;
}

var set_item_wise_tax = function(item, tax, tax_rate, current_tax_amount) {
	// store tax breakup for each item
	let tax_detail = tax.item_wise_tax_detail;
	let key = item.item_code || item.item_name;

	let item_wise_tax_amount = current_tax_amount;
	if (tax_detail && tax_detail[key])
		item_wise_tax_amount += tax_detail[key][1];

	tax_detail[key] = [tax_rate, flt(item_wise_tax_amount, precision("base_tax_amount", tax))];
};

var initialize_taxes = function(frm) {
	$.each(frm.doc["taxes"] || [], function(i, tax) {
		tax.item_wise_tax_detail = {};
		var tax_fields = ["total", "tax_amount_after_discount_amount",
			"tax_amount_for_current_item", "grand_total_for_current_item",
			"tax_fraction_for_current_item", "grand_total_fraction_for_current_item"];

		if (cstr(tax.charge_type) != "Actual") {
			tax_fields.push("tax_amount");
		}

		$.each(tax_fields, function(i, fieldname) { tax[fieldname] = 0.0; });

		if (cur_frm) {
			cur_frm.cscript.validate_taxes_and_charges(tax.doctype, tax.name);
			cur_frm.cscript.validate_inclusive_tax(tax);
		}
		frappe.model.round_floats_in(tax);
	});
}

var _cleanup = function(frm) {
	if(frm.doc["taxes"] && frm.doc["taxes"].length) {
		var temporary_fields = ["tax_amount_for_current_item", "grand_total_for_current_item",
			"tax_fraction_for_current_item", "grand_total_fraction_for_current_item"];

		if(!frappe.meta.get_docfield(frm.doc["taxes"][0].doctype, "tax_amount_after_discount_amount", frm.doctype)) {
			temporary_fields.push("tax_amount_after_discount_amount");
		}

		$.each(frm.doc["taxes"] || [], function(i, tax) {
			$.each(temporary_fields, function(i, fieldname) {
				delete tax[fieldname];
			});

			tax.item_wise_tax_detail = JSON.stringify(tax.item_wise_tax_detail);
		});
	}
}

cur_frm.cscript.validate_taxes_and_charges = function(cdt, cdn) {
	var d = locals[cdt][cdn];
	var msg = "";

	if(d.account_head && !d.description) {
		// set description from account head
		d.description = d.account_head.split(' - ').slice(0, -1).join(' - ');
	}

	if(!d.charge_type && (d.row_id || d.rate || d.tax_amount)) {
		msg = __("Please select Charge Type first");
		d.row_id = "";
		d.rate = d.tax_amount = 0.0;
	} else if((d.charge_type == 'Actual' || d.charge_type == 'On Net Total') && d.row_id) {
		msg = __("Can refer row only if the charge type is 'On Previous Row Amount' or 'Previous Row Total'");
		d.row_id = "";
	} else if((d.charge_type == 'On Previous Row Amount' || d.charge_type == 'On Previous Row Total') && d.row_id) {
		if (d.idx == 1) {
			msg = __("Cannot select charge type as 'On Previous Row Amount' or 'On Previous Row Total' for first row");
			d.charge_type = '';
		} else if (!d.row_id) {
			msg = __("Please specify a valid Row ID for row {0} in table {1}", [d.idx, __(d.doctype)]);
			d.row_id = "";
		} else if(d.row_id && d.row_id >= d.idx) {
			msg = __("Cannot refer row number greater than or equal to current row number for this Charge type");
			d.row_id = "";
		}
	}
	if(msg) {
		frappe.validated = false;
		refresh_field("taxes");
		frappe.throw(msg);
	}

}

cur_frm.cscript.validate_inclusive_tax = function(tax) {
	var actual_type_error = function() {
		var msg = __("Actual type tax cannot be included in Item rate in row {0}", [tax.idx])
		frappe.throw(msg);
	};

	var on_previous_row_error = function(row_range) {
		var msg = __("For row {0} in {1}. To include {2} in Item rate, rows {3} must also be included",
			[tax.idx, __(tax.doctype), tax.charge_type, row_range])
		frappe.throw(msg);
	};

	if(cint(tax.included_in_print_rate)) {
		if(tax.charge_type == "Actual") {
			// inclusive tax cannot be of type Actual
			actual_type_error();
		} else if(tax.charge_type == "On Previous Row Amount" &&
			!cint(frm.doc["taxes"][tax.row_id - 1].included_in_print_rate)
		) {
			// referred row should also be an inclusive tax
			on_previous_row_error(tax.row_id);
		} else if(tax.charge_type == "On Previous Row Total") {
			var taxes_not_included = $.map(frm.doc["taxes"].slice(0, tax.row_id),
				function(t) { return cint(t.included_in_print_rate) ? null : t; });
			if(taxes_not_included.length > 0) {
				// all rows above this tax should be inclusive
				on_previous_row_error(tax.row_id == 1 ? "1" : "1 - " + tax.row_id);
			}
		} else if(tax.category == "Valuation") {
			frappe.throw(__("Valuation type charges can not marked as Inclusive"));
		}
	}
}

var determine_exclusive_rate = function(frm) {
	var has_inclusive_tax = false;
	$.each(frm.doc["taxes"] || [], function(i, row) {
		if(cint(row.included_in_print_rate)) has_inclusive_tax = true;
	});
	if(has_inclusive_tax==false) return;

	$.each(frm.doc["items"] || [], function(n, item) {
		var item_tax_map = _load_item_tax_rate(item.session_rate);
		var cumulated_tax_fraction = 0.0;

		$.each(frm.doc["taxes"] || [], function(i, tax) {
			tax.tax_fraction_for_current_item = get_current_tax_fraction(frm, tax, item_tax_map);

			if(i==0) {
				tax.grand_total_fraction_for_current_item = 1 + tax.tax_fraction_for_current_item;
			} else {
				tax.grand_total_fraction_for_current_item =
					frm.doc["taxes"][i-1].grand_total_fraction_for_current_item +
					tax.tax_fraction_for_current_item;
			}

			cumulated_tax_fraction += tax.tax_fraction_for_current_item;
		});

		if(cumulated_tax_fraction) {
			item.session_total = flt(item.session_total / (1 + cumulated_tax_fraction));
			item.session_rate = item.qty ? flt(item.session_total / item.qty, precision("session_rate", item)) : 0;
		}
	});
}

var get_current_tax_fraction = function(frm, tax, item_tax_map) {
	// Get tax fraction for calculating tax exclusive amount
	// from tax inclusive amount
	var current_tax_fraction = 0.0;

	if(cint(tax.included_in_print_rate)) {
		var tax_rate = _get_tax_rate(tax, item_tax_map);

		if(tax.charge_type == "On Net Total") {
			current_tax_fraction = (tax_rate / 100.0);

		} else if(tax.charge_type == "On Previous Row Amount") {
			current_tax_fraction = (tax_rate / 100.0) *
				frm.doc["taxes"][cint(tax.row_id) - 1].tax_fraction_for_current_item;

		} else if(tax.charge_type == "On Previous Row Total") {
			current_tax_fraction = (tax_rate / 100.0) *
				frm.doc["taxes"][cint(tax.row_id) - 1].grand_total_fraction_for_current_item;
		}
	}

	if(tax.add_deduct_tax) {
		current_tax_fraction *= (tax.add_deduct_tax == "Deduct") ? -1.0 : 1.0;
	}
	return current_tax_fraction;
}

var _get_tax_rate = function(tax, item_tax_map) {
	return (Object.keys(item_tax_map).indexOf(tax.account_head) != -1) ?
		flt(item_tax_map[tax.account_head], precision("rate", tax)) : tax.rate;
}

var _load_item_tax_rate = function(item_tax_rate) {
	return item_tax_rate ? JSON.parse(item_tax_rate) : {};
}

var calculate_totals = function(frm) {
	// Changing sequence can cause rounding_adjustmentng issue and on-screen discrepency
	var tax_count = frm.doc["taxes"] ? frm.doc["taxes"].length : 0;
	frm.set_value("package_price", flt(tax_count ? frm.doc["taxes"][tax_count - 1].total : frm.doc.rate))
	frappe.model.set_value(frm.doc.doctype, frm.doc.name, "total_taxes_and_charges", flt(frm.doc.package_price - frm.doc.rate))
	// Round grand total as per precision
	frappe.model.round_floats_in(frm.doc, ["package_price"]);
}
//child table
frappe.ui.form.on('Healthcare Package Item',{
	session_rate:function(frm, cdt, cdn){
		if(frm.doc.doctype == "Healthcare Package Assignment"){
			set_session_amount(cdt, cdn);
		}
	},
	no_of_session:function(frm, cdt, cdn){
		if(frm.doc.doctype == "Healthcare Package Assignment"){
			set_session_amount(cdt, cdn);
		}
	},
	session_amount:function(frm,cdt,cdn){
		if(frm.doc.doctype == "Healthcare Package Assignment"){
			var  item=frappe.get_doc(cdt,cdn)
			set_session_discount(cdt,cdn,item.discount_percentage);
		}
	},
	discount_amount:function(frm,cdt,cdn) {
		if(frm.doc.doctype == "Healthcare Package Assignment"){
			var  item=frappe.get_doc(cdt,cdn)
			let disc_per= (item.discount_amount * 100)/item.session_amount
			frappe.model.set_value(item.doctype,item.name,"discount_percentage",disc_per)
			set_session_discount(cdt,cdn,disc_per);
		}
	},
	discount_percentage:function(frm,cdt,cdn) {
		if(frm.doc.doctype == "Healthcare Package Assignment"){
			var  item=frappe.get_doc(cdt,cdn)
			set_session_discount(cdt,cdn,item.discount_percentage);
		}
	},
	session_total:function(frm){
			set_total_package_price(frm);
			calculate_taxe_details(frm);
	},
});
var set_session_discount = function(cdt,cdn, discount_perc){
	var item = frappe.get_doc(cdt, cdn);
	let disc_amount=((item.session_amount * discount_perc)/100)
	let total=item.session_amount-disc_amount
	if(item.discount_amount >= 0){
		frappe.model.set_value(item.doctype,item.name,"discount_amount",disc_amount);
	}
	if(total >= 0){
		frappe.model.set_value(item.doctype,item.name,"session_total",total);
	}
}

var set_session_amount = function(cdt,cdn){
	var item = frappe.get_doc(cdt, cdn);
	if(item.no_of_session && item.session_rate){
		let total= item.no_of_session * item.session_rate
		frappe.model.set_value(item.doctype,item.name,"session_amount",total)
		frappe.model.set_value(item.doctype,item.name,"session_total",total)
	}
}
var set_total_package_price = function(frm){
	var total_amount=0
	var total_session=0
	var total_discount=0
	for (var i in frm.doc.items) {
		var item = frm.doc.items[i];
		if(item.session_total ){
			total_amount=total_amount+item.session_total
		}
	}
	frappe.model.set_value(frm.doc.doctype, frm.doc.name, "rate", total_amount)
	calculate_totals(frm)
	frm.refresh_fields()
};

frappe.ui.form.on('Package Payment Reference',{
	edit:function(frm, cdt, cdn){
		update_payment_reference(frm, cdt, cdn)
	},
	btn_cancel:function(frm, cdt, cdn){
		cancel_existing_payment_reference(frm, cdt, cdn)
	},

});

var update_payment_reference = function(frm, cdt, cdn){
	var child = locals[cdt][cdn];
		frappe.call({
			method: "sehacloud_healthcare.sehacloud_healthcare.doctype.healthcare_package_assignment.healthcare_package_assignment.get_mode_of_payment_from_pos_profile",
			callback: function(r) {
				var mode_of_payment_filter = (r.message && r.message.length>0) ? r.message : false
			var d = new frappe.ui.Dialog({
				title: __("Amount To Be Paid"),
				fields: [
					{ fieldtype: 'Date', fieldname: 'posting_date', label: 'Posting Date', default: frappe.datetime.now_date() },
					{ fieldtype: 'Data', fieldname: 'paid_amount', label: 'Paid Amount'},
					{ fieldtype: 'Link', fieldname: 'mode_of_payment', label: 'Mode of Payment',  options: 'Mode of Payment',
						get_query: function(){
							return{
								filters: mode_of_payment_filter ? {name: ["in", mode_of_payment_filter]} : {}
							};
						}
					},
					{ fieldtype: 'Data', fieldname: 'mode_of_payment_type', label: 'Payment Mode Type', hidden:1},
					{ fieldtype: 'Data', fieldname: 'reference_no', label: 'Cheque/Reference No', depends_on:'eval:doc.mode_of_payment_type == `Bank`'},
					{ fieldtype: 'Date', fieldname: 'reference_date', label: 'Cheque/Reference Date', depends_on:'eval:doc.mode_of_payment_type == `Bank`'},
					{ fieldtype: 'Column Break'},
					{ fieldtype: 'Data', read_only:1, fieldname: 'package_price', label: 'Package Price'},
					// { fieldtype: 'Data', read_only:1, fieldname: 'balance_due', label: 'Balance Due'},
					{ fieldtype: 'Link', read_only:1, fieldname: 'account_paid_to', label: 'Account Paid To',  options: 'Account'}
				],
				primary_action_label: __("Amend"),
				primary_action: function() {
					d.hide();
					frappe.call({
								method: "sehacloud_healthcare.sehacloud_healthcare.doctype.healthcare_package_assignment.healthcare_package_assignment.update_payment_reference",
								args: {
									name:frm.doc.name,
									posting_date:d.get_value('posting_date'),
									paid_amount:d.get_value('paid_amount'),
									mode_of_payment:d.get_value('mode_of_payment'),
									account_paid_to:d.get_value('account_paid_to'),
									reference_no:d.get_value('reference_no'),
									reference_date:d.get_value('reference_date'),
									child:child.name
								},
								callback: function(r) {
									frappe.show_alert({message:__("Payment Successfully"), indicator:'green'});
									frm.refresh();
								},
								freeze: true,
								freeze_message: __("Payment Processing....")
							})
				}
			});
				d.show();
				d.set_values({
					'package_price': frm.doc.package_price,
					'paid_amount': child.paid_amount
				});
				d.fields_dict["mode_of_payment"].df.onchange = () => {
					if(d.get_value('mode_of_payment')){
						var mode_of_payment = d.get_value('mode_of_payment');
						get_payment_mode_account(frm, mode_of_payment, function(account){
							d.set_values({
								'account_paid_to': account
							});
						});
						frappe.model.get_value('Mode of Payment', {'name':d.get_value('mode_of_payment')}, 'type',
							function(data) {
								if(data.type){
									d.set_values({
										'mode_of_payment_type': data.type
									});
								}
							}
						);
					}
				}
			}
		});
	}
var cancel_existing_payment_reference = function(frm, cdt, cdn){
	var child = locals[cdt][cdn];
	var confirmation_msg = 'Do you want to cancel this Journal Entry ?'
	frappe.confirm( confirmation_msg,function(){
		window.close();
		if(child.package_payment_reference && child.package_transfer_reference){
			frappe.call({
						method: "sehacloud_healthcare.sehacloud_healthcare.doctype.healthcare_package_assignment.healthcare_package_assignment.cancel_existing_payment_reference",
						args: {
							child:child.name
						},
						callback: function(r) {
							frappe.show_alert({message:__("Cancelled"), indicator:'red'});
							frm.refresh_fields()
						},
						freeze: true,
						freeze_message: __("Processing....")
				})
		}
	})
}
