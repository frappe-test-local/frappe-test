# -*- coding: utf-8 -*-
# Copyright (c) 2019, SehaCloud and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe
from frappe import _
from frappe.model.document import Document

class HealthcarePackage(Document):
	def validate(self):
		if self.duration == 0:
			frappe.throw(_("Duration must be greater than zero"))

@frappe.whitelist()
def get_package_item_list(package):
	query = """select item_code from `tabHealthcare Package Item` where parent = '{parent}' order by name"""
	return frappe.db.sql(query.format(parent=package))
