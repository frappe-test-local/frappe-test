// Copyright (c) 2019, SehaCloud and contributors
// For license information, please see license.txt

frappe.ui.form.on('Healthcare Package', {
	package_name: function(frm) {
		if(!frm.doc.description)
			frm.set_value("description", frm.doc.package_name);
	},
	setup:function(frm){
		frm.add_fetch("company", "default_income_account", "income_account");
		frm.set_query("package_advance", function() {
			return {
				filters: {
					'root_type': 'Liability',
					'company': frm.doc.company,
					"is_group": 0
				}
			};
		});
		frm.set_query("unavailed_package_balance_account", function() {
			return {
				filters: {
					'root_type': ["in", ['Liability', 'Income']],
					'company': frm.doc.company,
					"is_group": 0
				}
			};
		});
		frm.set_query("income_account", function(doc) {
			return {
				filters: {
					'account_type': 'Income Account',
					'is_group': 0,
					'company': doc.company
				}
			};
		});
		frm.set_query("refundable_charges_income_account", function(doc) {
			return {
				filters: {
					'account_type': 'Income Account',
					'is_group': 0,
					'company': doc.company
				}
			};
		});
		frm.set_query("temporary_package_income_account", function() {
			return {
				filters: {
					'company': frm.doc.company,
					"is_group": 0
				}
			};
		});
	},
	refresh: function(frm){
		frm.set_query("item_code", "items", function(doc, cdt, cdn) {
			var item_list = "";
			var child = locals[cdt][cdn];
			doc.items.forEach(function(row) {
				if(row.healthcare_practitioner_level == child.healthcare_practitioner_level){
					item_list += ", "+row.item_code
				}
			});
			return{
				filters: [
					['Item', 'name', 'not in', item_list],
					["Item", "is_stock_item", "!=", 1]
				]
			}
		});
		if(frm.doc.__islocal){
			frappe.db.get_value('Selling Settings', {name: 'Selling Settings'}, 'selling_price_list', (r) => {
				if(r && r.selling_price_list){
					frm.set_value("price_list",r.selling_price_list);
				}
			});
			frappe.db.get_value('Sales Taxes and Charges Template', {is_default: 1}, 'name', (r) => {
				if(r && r.name){
					frm.set_value("taxes_and_charges",r.name);
				}
			});
		}

	},
	refunding_percentage: function(frm) {
		if (frm.doc.refunding_percentage > 0){
			frm.set_value("refunding_amount",0);
		}
	},
	refunding_amount: function(frm) {
		if (frm.doc.refunding_amount > 0){
			frm.set_value("refunding_percentage",0);
		}
	},
	is_refundable:function(frm){
		income_account_req_toggling(frm)
	},
	refresh:function(frm){
		income_account_req_toggling(frm)
	}
});
var calculate_totals = function(frm) {
	// Changing sequence can cause rounding_adjustmentng issue and on-screen discrepency
	frm.set_value("package_price",  frm.doc.rate);
	frappe.model.round_floats_in(frm.doc, ["package_price"]);
}

frappe.ui.form.on('Healthcare Package Item',{
	item_code:  function(frm, cdt, cdn) {
		if(frm.doc.doctype == "Healthcare Package"){
			var d = locals[cdt][cdn];
			if(d.item_code){
				frappe.call({
					"method": "frappe.client.get_value",
					args: {
						"doctype": "Item Price",
						'filters':{
							"item_code": d.item_code,
							"price_list": frm.doc.price_list
						},
						'fieldname':['price_list_rate']
					},
					callback: function (data) {
						if(data.message){
							frappe.model.set_value(cdt, cdn,"session_rate",data.message.price_list_rate)
							frappe.model.set_value(cdt, cdn,"no_of_session",1)
						}
					}
				});
			}
		}
	},
	session_rate:function(frm, cdt, cdn){
		if(frm.doc.doctype == "Healthcare Package"){
			set_session_amount(cdt, cdn);
		}
	},
	no_of_session:function(frm, cdt, cdn){
		if(frm.doc.doctype == "Healthcare Package"){
			set_session_amount(cdt, cdn);
		}
	},
	session_amount:function(frm,cdt,cdn){
		if(frm.doc.doctype == "Healthcare Package"){
			var  item=frappe.get_doc(cdt,cdn)
			set_session_discount(cdt,cdn,item.discount_percentage)
		}
	},
	discount_amount:function(frm,cdt,cdn) {
		if(frm.doc.doctype == "Healthcare Package"){
			var  item=frappe.get_doc(cdt,cdn)
			let disc_per= (item.discount_amount * 100)/item.session_amount
			frappe.model.set_value(item.doctype,item.name,"discount_percentage",disc_per)
			set_session_discount(cdt,cdn,disc_per)
		}
	},
	discount_percentage:function(frm,cdt,cdn) {
		if(frm.doc.doctype == "Healthcare Package"){
			var  item=frappe.get_doc(cdt,cdn)
			set_session_discount(cdt,cdn,item.discount_percentage)
			if(item.discount_percentage){
			}
		}
	},
	session_total:function(frm){
		if(frm.doc.doctype == "Healthcare Package"){
			set_total_package_price(frm)
		}
	}
});

var set_session_discount = function(cdt,cdn, discount_perc){
	var item = frappe.get_doc(cdt, cdn);
	if(item.session_amount && discount_perc){
		let disc_amount=((item.session_amount * discount_perc)/100)
		let total=item.session_amount-disc_amount
		frappe.model.set_value(item.doctype,item.name,"discount_amount",disc_amount);
		frappe.model.set_value(item.doctype,item.name,"session_total",total);
	}
}
var set_session_amount = function(cdt,cdn){
	var item = frappe.get_doc(cdt, cdn);
	if(item.no_of_session && item.session_rate){
		let total= item.no_of_session * item.session_rate
		frappe.model.set_value(item.doctype,item.name,"session_amount",total)
		frappe.model.set_value(item.doctype,item.name,"session_total",total)
	}
}

var set_total_package_price = function(frm){
	var total_amount=0
	var total_session=0
	var total_discount=0
	for (var i in frm.doc.items) {
		var item = frm.doc.items[i];
		if(item.session_total ){
			total_amount=total_amount+item.session_total
		}
		if(item.discount_amount){
			total_discount=total_discount+item.discount_amount
		}
		if(item.session_amount){
			total_session=total_session+item.session_amount
		}
	}
	frappe.model.set_value(frm.doc.doctype, frm.doc.name, "rate", total_amount)
	calculate_totals(frm)
	frappe.model.set_value(frm.doc.doctype, frm.doc.name, "total_session_price", total_session)
	frappe.model.set_value(frm.doc.doctype, frm.doc.name, "total_discount", total_discount)
	frm.refresh_fields()
};

var income_account_req_toggling = function(frm){
	if (frm.doc.is_refundable == 1){
		frm.set_df_property('refundable_charges_income_account', 'reqd', 1)
	}
	else{
		frm.set_df_property('refundable_charges_income_account', 'reqd', 0)
	}
}
