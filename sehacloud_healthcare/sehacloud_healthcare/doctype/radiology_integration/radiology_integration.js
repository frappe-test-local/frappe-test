// Copyright (c) 2019, SehaCloud and contributors
// For license information, please see license.txt

frappe.ui.form.on('Radiology Integration', {
	active: function(frm) {
		if(!frm.doc.active){
			frm.set_value("default", false);
		}
	}
});
