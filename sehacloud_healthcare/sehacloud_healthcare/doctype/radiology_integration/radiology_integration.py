# -*- coding: utf-8 -*-
# Copyright (c) 2019, SehaCloud and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe
from frappe.model.document import Document

class RadiologyIntegration(Document):
	def validate(self):
		if self.active and self.default:
			for d in frappe.get_list("Radiology Integration", fields=("name"), filters={"active": 1, "default": 1}):
				frappe.db.set_value("Radiology Integration", d.name, 'default', 0)
			self.default = True
		elif not self.active:
			self.default = False
