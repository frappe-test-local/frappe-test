# -*- coding: utf-8 -*-
# Copyright (c) 2018, ESS LLP and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe
from frappe import _
from frappe.utils import getdate

@frappe.whitelist()
def get_invoices(date, patient=None, sales_invoice=None):
	query = """
		select
			si.name, si.patient, si.patient_name , si.posting_date, si.ref_practitioner, si.status
		from
			`tabSales Invoice` si, `tabSales Invoice Item` item
		where
			si.name = item.parent and item.reference_dt='Drug Prescription'
			and si.docstatus = 1 and item.delivered_qty < item.qty
	"""

	if patient:
		query += " and si.patient='{0}'".format(patient)

	if date:
		query += " and si.posting_date='{0}'".format(getdate(date))

	if sales_invoice:
		query += " and si.name='{0}'".format(sales_invoice)

	return frappe.db.sql(query, as_dict=True)

@frappe.whitelist()
def create_delivery_note(sales_invoice_id):
	from erpnext.accounts.doctype.sales_invoice.sales_invoice import make_delivery_note
	return make_delivery_note(sales_invoice_id)
