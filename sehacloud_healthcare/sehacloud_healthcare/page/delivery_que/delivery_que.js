frappe.provide('frappe.healthcare');

frappe.pages['delivery_que'].on_page_load = function(wrapper) {
	frappe.healthcare.appointment = new frappe.healthcare.Appointment(wrapper);
	frappe.breadcrumbs.add("Healthcare");
}

frappe.healthcare.Appointment = Class.extend({
	init: function(parent) {
		this.parent = parent;
		this.make_page();
		this.make_toolbar();
		this.get_invoices();
	},

	make_page: function() {
		if (this.page)
			return;

		frappe.ui.make_app_page({
			parent: this.parent,
			title: __('Delivery Desk'),
			single_column: true
		});
		this.page = this.parent.page;
		this.wrapper = $('<div></div>').appendTo(this.page.main);
	},

	make_toolbar: function () {
		var me = this;
		var selected_patient = '';

		this.patient = this.page.add_field({label: __("Patient"), fieldtype: 'Link',
		options: 'Patient',
		change: function(){
			if(me.patient.get_value() && me.patient.get_value() != selected_patient){
				selected_patient = me.patient.get_value();
				me.get_invoices();
			}
		}
		});
		this.date = this.page.add_date(__("Date"), frappe.datetime.get_today())
		this.date.on('change', function(){
			me.get_invoices()
		});
		this.sales_invoice = this.page.add_field({label: __("Sales Invoice"), fieldtype: 'Link',
		options: 'Sales Invoice',
		change: function(){
					me.get_invoices();
			}
		});
		this.page.set_primary_action(__("Refresh"), function() {
			me.get_invoices();
		}, "fa fa-refresh");
	},

	get_invoices: function() {
		var me = this;
		me.wrapper.empty();
		var date = '';
		if (me.date.val() !=''){
			date = frappe.datetime.user_to_str(me.date.val());
		}
		if(!me.date.val() && !me.patient.get_value()){
			frappe.msgprint({
				message: __("No sufficient filters to query Appoinments"),
				indicator: 'red'
			});
			return;
		}
		return frappe.call({
			method: "sehacloud_healthcare.sehacloud_healthcare.page.delivery_que.delivery_que.get_invoices",
			args: { patient: me.patient.get_value(), date: date,  sales_invoice: me.sales_invoice.get_value() },
			callback: function (r) {
				me.wrapper.empty();
				if(r.message.length > 0){
					me.make_list(r.message)
				}else{
					let msg_html = '<p class="text-muted" style="padding: 15px;">No Sales Invoice	 found </p>';
					$(msg_html).appendTo(me.wrapper);
				}
			},
			freeze: true
		});
	},
	make_list: function (sales_invoices) {
		var me = this;
		$(`<div class="col-sm-12" style='background-color:#D3D3D3;'><div class="col-sm-12"><b>
			<div class="col-sm-3">Sales Invoice</div>
			<div class="col-sm-3">Patient Name</div>
			<div class="col-sm-2">Date</div>
			<div class="col-sm-2">Referring Practitioner</div>
			<div class="col-sm-1">Status</div>
			<div class="col-sm-1"></div>
		</div></div>`).appendTo(me.wrapper)
		$.each(sales_invoices, function(i, sales_invoice){
			$(frappe.render_template("delivery_que", {data: sales_invoice})).appendTo(me.wrapper);
		});
		this.wrapper.find(".make-delivery-note").on("click", function() {
			me.create_delivery_note($(this).attr("data-name"))
		})
	},
	create_delivery_note: function(sales_invoice_id){
		frappe.call({
			method:"sehacloud_healthcare.sehacloud_healthcare.page.delivery_que.delivery_que.create_delivery_note",
			args: {sales_invoice_id :sales_invoice_id},
			callback: function(data){
				if(!data.exc){
					var doclist = frappe.model.sync(data.message);
					frappe.set_route("Form", doclist[0].doctype, doclist[0].name);
				}
			},
			freeze: true
		});
	},

	});
