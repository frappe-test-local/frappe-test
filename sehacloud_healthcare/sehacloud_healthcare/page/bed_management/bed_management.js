frappe.provide('frappe.healthcare');

frappe.pages['bed_management'].on_page_load = function (wrapper) {
	frappe.healthcare.bed_management = new frappe.healthcare.BedManagement(wrapper);
	frappe.breadcrumbs.add("Healthcare");

};

frappe.healthcare.BedManagement = Class.extend({
	init: function (parent) {
		this.parent = parent;
		this.make_page();
		this.make_toolbar();
		this.get_bed_details();
	},
	make_page: function () {
		if (this.page)
			return;

		my_page = frappe.ui.make_app_page({
			parent: this.parent,
			title: __('Bed Management'),
			single_column: true
		});
		this.page = this.parent.page;
		this.wrapper = $('<div class="a"></div>').appendTo(this.page.main);
	},
	get_bed_details: function () {
		var me = this;
		me.wrapper.empty();
		var status_value = me.status.get_value();
		if (me.status.get_value() == "Select status") {
			status_value = null;
		}
		return frappe.call({
			method: "sehacloud_healthcare.sehacloud_healthcare.page.bed_management.bed_management.get_bed_details",
			args: {
				service_unit_type: me.service_unit_type.get_value(),
				status: status_value,
				service_unit_group: me.group.get_value()
			},
			callback: function (r) {
				me.prepare_service_unit_list(r.message)
			},
			freeze: true
		});
	},
	make_toolbar: function () {
		var me = this;
		var selected_service_unit_type = "";
		this.service_unit_type = this.page.add_field({
			fieldtype: 'Link',
			placeholder: __('Healthcare Service Unit Type'),
			options: 'Healthcare Service Unit Type',
			change: function () {
				if (me.service_unit_type.get_value() != selected_service_unit_type) {
					selected_service_unit_type = me.service_unit_type.get_value();
					me.get_bed_details();
				}
			},

			get_query: function () {
				return {
					filters: {
						"inpatient_occupancy": 1
					}
				};
			}
		});

		this.status = this.page.add_field({
			fieldtype: 'Select',
			options: 'Select status\nVacant\nMaintenance\nBlocked\nOccupied',
			default: "Select status",
			change: function () {
				me.get_bed_details();
			},
		})

		var selected_group = "";
		this.group = this.page.add_field({
			fieldtype: 'Link',
			placeholder: __('Select group'),
			options: 'Healthcare Service Unit',
			change: function () {
				if (me.group.get_value() != selected_group) {
					selected_group = me.group.get_value();
					me.get_bed_details();
				}
			},
			get_query: function () {
				return {
					filters: {
						"is_group": 1,
						"lft": ["!=", 1]
					}
				};
			}
		})
	},
	update_service_unit_status: function (service_unit, status, me) {
		frappe.call({
			method: "sehacloud_healthcare.sehacloud_healthcare.page.bed_management.bed_management.change_service_unit_status",
			args: {
				service_unit: service_unit,
				status: status
			},
			callback: function (r) {
				me.get_bed_details();
			}
		});
	},
	prepare_service_unit_list(data) {
		var me = this;
		var total_bed = 0;
		var total_occupied = 0;
		var total_vaccant = 0;
		var status_value = "";
		var bed_list_html = "";
		$.each(data, function (i, bed) {

			total_bed += 1;
			if (bed.status == "Vacant") {
				total_vaccant += 1;
			}
			if (bed.status == "Occupied") {
				total_occupied += 1;
			}
		});

		bed_list_html += `<section id="panel-head">
							<span class="total-bed" id="available">Total Available Beds : ${total_vaccant}/${total_bed} </span>
							<span class="total-bed" id="occupied">Total Occupied Beds : ${total_occupied}/${total_bed} </span>
							</section>`
		bed_list_html += `<div class="bed-listings">
    						<div class="bed">
								<div class="container">`
		var parent_healthcare_service_unit = "";
		var j = 0;
		$.each(data, function (i, bed) {

			if (parent_healthcare_service_unit != bed.parent_healthcare_service_unit) {
				parent_healthcare_service_unit = bed.parent_healthcare_service_unit
				if (i != 0) {

					bed_list_html += `</div>`
					bed_list_html += `<hr>`
				}
				bed_list_html += `<label>${bed.parent_healthcare_service_unit}</label>`
				j = 0;
			}
			if (i == 0) {
				parent_healthcare_service_unit = bed.parent_healthcare_service_unit;
			}
			if ((j % 12) == 0) {
				bed_list_html += `<div class="row">`
			}

			bed_list_html += frappe.render_template("bed_management", {
				bed
			})
			if ((j % 12) == 11 && j != 0) {
				bed_list_html += `</div>`;
			}
			j += 1
		});
		bed_list_html += `</div></div></div>`

		me.wrapper.append(bed_list_html);
		$(window).bind('ChangeView', function (e, data) {
			$('.change-project').popover({
				placement: 'bottom',
				title: 'Change',
				trigger: 'click',
				html: true,
				content: function () {
					var content = '';
					content = $('#select-div').html();
					return content;
				}
			}).on('shown.bs.popover', function () {});
		});

		$(document).ready(function () {
			$(window).trigger('ChangeView', {});
			$('.my_bed').click(function () {
				$('.my_bed').not(this).popover('hide');

				var $btn = $(this);
				var popover_html = "";
				var disabled = "disabled";

				if ($btn.attr("data-occupancy") != "Occupied") {
					var selected_vacant = "";
					var selected_maint = "";
					var selected_blocked = "";
					if ($btn.attr("data-occupancy") == "Vacant") {
						selected_vacant = "selected  disabled";
					}
					if ($btn.attr("data-occupancy") == "Maintenance") {
						selected_maint = "selected disabled";
					}
					if ($btn.attr("data-occupancy") == "Blocked") {
						selected_blocked = "selected disabled";
					}

					popover_html += `<section id="dd-head">
						<span> Service Unit : ${$btn.attr("data-service-unit")}</span>
						<br/>
						<span> Service Unit Type : ${$btn.attr("data-unit-type")}</span>
						<br/>
						<span> Status : ${$btn.attr("data-occupancy")}</span>
						</section>
						<section id="dd-body">
						<div class="container">

						<form>
						<div id="select-div">
						<select class="form-control" value="Maintenance">
							<option ${selected_vacant}  value="Vacant">Vacant</option>
							<option ${selected_maint}  value="Maintenance">Maintenance</option>
							<option ${selected_blocked} value="Blocked">Blocked</option>
						</select>
						<br>
						</div>
						</form>
						</div>
						</section>

						<section id="dd-foot">
							<button id="status_btn" type="button" class="btn btn-default btn-go disabled">Save</button>
						</section>
					</section>`

					$('.bed').find('h3.popover-title').text('Bed Management')
					$(".bed").find(".popover-content").html(popover_html);
					$('select').on('change', function () {
						status_value = this.value;
						$('button#status_btn').removeClass('disabled');
					});
					$('button#status_btn').on('click', function () {
						me.update_service_unit_status($btn.attr("data-service-unit-name"), status_value, me);
					});
				} else {
					frappe.call({
						method: "sehacloud_healthcare.sehacloud_healthcare.page.bed_management.bed_management.get_patient_on_the_service_unit",
						args: {
							service_unit: $btn.attr("data-service-unit-name")
						},
						callback: function (r) {
							if (r.message && r.message.length > 0) {
								var data = r.message[0]
								popover_html += `<section id="dd-head">
								<span> Patient : ${data.patient}</span>
								<br/>
								<span> Patient Name : ${data.patient_name}</span>
								<br/>
								<span> Gender : ${data.gender}</span>
								<br/>
								<span> Date of Birth : ${data.dob}</span>
								<br/>
								<span> Inpatient Record : ${data.name}</span>
								<br/>
								<span> Primary Practitioner : ${data.practitioner_name_primary}</span>
								</section>`
							} else {
								popover_html += `No Patient Record find for this service unit.`
							}
							$('.bed').find('h3.popover-title').text('Patient Details');
							$(".bed").find(".popover-content").html(popover_html);
						},
						freeze: true
					})
				}
			});
		});
	}
})
