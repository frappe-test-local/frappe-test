# -*- coding: utf-8 -*-
# Copyright (c) 2018, ESS LLP and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe
from frappe import _
from frappe.utils import getdate
from erpnext.healthcare.doctype.healthcare_settings.healthcare_settings import get_receivable_account, get_income_account
from erpnext.healthcare.utils import validity_exists, service_item_and_practitioner_charge


@frappe.whitelist()
def get_bed_details(service_unit_type, status, service_unit_group):
    query = """
		select
			name, healthcare_service_unit_name, service_unit_type, status, parent_healthcare_service_unit
		from
			`tabHealthcare Service Unit`
		where
			inpatient_occupancy=1 and is_group!=1
	"""

    if service_unit_type:
        query += "and service_unit_type='{0}'".format(service_unit_type)
    if status:
        query += "and status='{0}'".format(status)
    if service_unit_group:
        query += "and parent_healthcare_service_unit='{0}'".format(
            service_unit_group)
    query += "order by parent_healthcare_service_unit"
    return frappe.db.sql(query, as_dict=1)


@frappe.whitelist()
def get_patient_on_the_service_unit(service_unit, left=0):
    query = """
		select
			io.service_unit, io.check_in, io.check_out, io.invoiced, io.left,
			ir.name, ir.patient, ir.patient_name, ir.blood_group, ir.dob, ir.gender, ir.practitioner_name_primary
		from
			`tabInpatient Occupancy` io, `tabInpatient Record` ir
		where
			io.parent = ir.name and io.left={0} and io.service_unit = '{1}'
	"""
    return frappe.db.sql(query.format(left, service_unit), as_dict=1)


@frappe.whitelist()
def change_service_unit_status(service_unit, status):
    frappe.db.set_value("Healthcare Service Unit",
                        service_unit, "status", status)
