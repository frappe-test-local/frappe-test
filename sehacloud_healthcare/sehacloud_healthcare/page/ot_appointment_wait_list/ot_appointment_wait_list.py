# -*- coding: utf-8 -*-
# Copyright (c) 2018, ESS LLP and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe
from frappe import _
from frappe.utils import getdate
from erpnext.healthcare.doctype.healthcare_settings.healthcare_settings import get_receivable_account,get_income_account
from erpnext.healthcare.utils import validity_exists, service_item_and_practitioner_charge, get_sales_invoice_for_healthcare_doc, get_doc_for_appointment

@frappe.whitelist()
def get_appointments(date, status_values, practitioner=None, patient=None, company=None, service_unit=None):
	if status_values !='' or not status_values:
		statuses = status_values.split(',')
	else:
		statuses = False

	query = """
        select
            pa.name, pa.patient_name, pa.patient, pa.practitioner, pa.appointment_date, pa.duration, pa.service_unit,
            pa.appointment_time, pa.status, pa.department, pa.procedure_template, pa.notes, pa.invoiced, pa.healthcare_practitioner_name,
            pp.comments, pa.company,  pa.schc_package_assignment, pi.item_code, pi.completed_session, pi.balance_sessions, pa.schc_package_item,
			pa.appointment_type
        from
            `tabPatient Appointment` pa left join
            `tabProcedure Prescription` pp on pp.name=pa.procedure_prescription  left join
			`tabHealthcare Package Assignment` hp on hp.name=pa.schc_package_assignment left join
			`tabHealthcare Package Item` pi on pi.parent=pa.schc_package_assignment and pi.item_code=pa.schc_package_item
        where
            pa.docstatus < 2 and pa.procedure_template IS NOT NULL"""

	if practitioner:
		query += " and pa.practitioner='{0}'".format(practitioner)
	if date:
		query += " and pa.appointment_date='{0}'".format(getdate(date))
	if patient:
		query += " and pa.patient='{0}'".format(patient)
	if company:
		query += " and pa.company='{0}'".format(company)
	if service_unit:
		query += " and pa.service_unit='{0}'".format(service_unit)

	status_str = False
	if statuses:
		for status in statuses:
			status = status.split()
			if status:
				if not status_str:
					status_str = "(pa.status = '{0}'".format(status[0])
				else:
					status_str += "or pa.status = '{0}'".format(status[0])
	if status_str:
		status_str += ")"
		query += " and {0}".format(status_str)
	else:
		query += " and pa.status != 'Cancelled'"

	query += " order by pa.status, pa.appointment_time"

	appointment_list = frappe.db.sql(query, as_dict=1)
	for app in appointment_list:
		payment = _("Not Invoiced")
		if app.invoiced:
			payment = _("Unpaid")
			si = get_sales_invoice_for_healthcare_doc("Patient Appointment", app.name)
			if si:
				payment = si.status
		app['payment'] = payment
		if app.status == "Closed" or app.status == "In Progress":
			encounter = get_doc_for_appointment("Patient Encounter", app.name)
			if encounter:
				app['encounter'] = encounter
			else:
				app['encounter'] = {'name': ''}
		app['practitioner_filter'] = True if practitioner else False
		app['date_filter'] = True if date else False
		app['patient_filter'] = True if patient else False
		app['service_unit_filter'] = True if service_unit else False
	return appointment_list

@frappe.whitelist()
def create_encounter(appointment):
	appointment = frappe.get_doc("Patient Appointment",appointment)
	encounter = frappe.new_doc("Patient Encounter")
	encounter.appointment = appointment.name
	encounter.patient = appointment.patient
	encounter.practitioner = appointment.practitioner
	encounter.visit_department = appointment.department
	encounter.patient_sex = appointment.patient_sex
	encounter.source = appointment.source
	if appointment.source and appointment.source != "Direct":
		encounter.referring_practitioner = appointment.referring_practitioner
	encounter.encounter_date = appointment.appointment_date
	return encounter.as_dict()

@frappe.whitelist()
def update_encounter(appointment):
	encounter = get_doc_for_appointment("Patient Encounter", appointment)
	return encounter.as_dict() if encounter else False

@frappe.whitelist()
def create_procedure(appointment):
	appointment = frappe.get_doc("Patient Appointment",appointment)
	procedure = frappe.new_doc("Clinical Procedure")
	procedure.appointment = appointment.name
	procedure.patient = appointment.patient
	procedure.patient_age = appointment.patient_age
	procedure.patient_sex = appointment.patient_sex
	procedure.procedure_template = appointment.procedure_template
	procedure.prescription = appointment.procedure_prescription
	procedure.practitioner = appointment.practitioner
	procedure.invoiced = appointment.invoiced
	procedure.medical_department = appointment.department
	procedure.start_date = appointment.appointment_date
	procedure.start_time = appointment.appointment_time
	procedure.notes = appointment.notes
	procedure.service_unit = appointment.service_unit
	procedure.company = appointment.company
	procedure.source = appointment.source
	if appointment.source and appointment.source != "Direct":
		procedure.referring_practitioner = appointment.referring_practitioner
	consume_stock = frappe.db.get_value("Clinical Procedure Template", appointment.procedure_template, "consume_stock")
	if consume_stock == 1:
		procedure.consume_stock = True
		warehouse = False
		if appointment.service_unit:
			warehouse = frappe.db.get_value("Healthcare Service Unit", appointment.service_unit, "warehouse")
		if not warehouse:
			warehouse = frappe.db.get_value("Stock Settings", None, "default_warehouse")
		if warehouse:
			procedure.warehouse = warehouse
	return procedure.as_dict()

@frappe.whitelist()
def update_procedure(appointment):
	procedure = get_doc_for_appointment("Clinical Procedure", appointment)
	return procedure.as_dict() if procedure else False

@frappe.whitelist()
def reschedule_appointment(appointment_id, practitioner, appointment_date, appointment_time, department, service_unit, duration):
	appointment = frappe.get_doc("Patient Appointment", appointment_id)
	appointment.practitioner = practitioner
	appointment.appointment_date = appointment_date
	appointment.appointment_time = appointment_time
	appointment.department = department
	appointment.service_unit = service_unit
	appointment.duration = duration
	# If appointment created for today set as open
	if getdate() == getdate(appointment_date):
		appointment.status = "Open"
	else:
		appointment.status = "Scheduled"
	appointment.save()

@frappe.whitelist()
def invoice_appointment(appointment_id):
	if not appointment_id:
		return False
	appointment_doc = frappe.get_doc("Patient Appointment", appointment_id)
	sales_invoice = frappe.new_doc("Sales Invoice")
	sales_invoice.patient = appointment_doc.patient
	sales_invoice.customer = frappe.get_value("Patient", appointment_doc.patient, "customer")
	sales_invoice.appointment = appointment_doc.name
	sales_invoice.due_date = getdate()
	sales_invoice.is_pos = False
	sales_invoice.company = appointment_doc.company
	sales_invoice.ref_practitioner = appointment_doc.referring_practitioner
	sales_invoice.debit_to = get_receivable_account(appointment_doc.company)

	item_line = sales_invoice.append("items")
	service_item, practitioner_charge = service_item_and_practitioner_charge(appointment_doc)
	item_line.item_code = service_item
	item_line.description = "Consulting Charges:  " + appointment_doc.practitioner
	item_line.income_account = get_income_account(appointment_doc.practitioner, appointment_doc.company)
	item_line.rate = practitioner_charge
	item_line.amount = practitioner_charge
	item_line.qty = 1
	item_line.reference_dt = "Patient Appointment"
	item_line.reference_dn = appointment_doc.name

	sales_invoice.set_missing_values(for_validate = True)

	return sales_invoice.as_dict()

@frappe.whitelist()
def view_invoice(appointment_id):
	if not appointment_id:
		return False
	sales_invoice = get_sales_invoice_for_healthcare_doc("Patient Appointment", appointment_id)
	return sales_invoice if sales_invoice else False
