frappe.provide('frappe.healthcare');

frappe.pages['OT Appointment-Wait list'].on_page_load = function(wrapper) {
	// FONT AWESOME from CDN
	var font_awesome_5 = document.createElement('link');
	font_awesome_5.rel = "stylesheet";
	font_awesome_5.href = "https://use.fontawesome.com/releases/v5.7.2/css/all.css";
	font_awesome_5.integrigty = "sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr";
	font_awesome_5.crossorigin = "anonymous";
	document.getElementsByTagName('head')[0].appendChild(font_awesome_5);
	//END FONT AWESOME from CDN
	frappe.healthcare.appointment = new frappe.healthcare.Appointment(wrapper);
	frappe.breadcrumbs.add("Healthcare");
};

frappe.healthcare.Appointment = Class.extend({
	init: function(parent) {
		this.parent = parent;
		this.make_page();
		this.make_toolbar();
		this.get_appointments();
	},
	get_appointments: function() {
		var me = this;
		me.wrapper.empty();
		var date = '';
		if (me.date.val() !=''){
			date = frappe.datetime.user_to_str(me.date.val());
		}
		if(!me.date.val() && !me.practitioner.get_value() && !me.patient.get_value()){
			frappe.msgprint({
				message: __("No sufficient filters to query Appoinments"),
				indicator: 'red'
			});
			return;
		}
		return frappe.call({
			method: "sehacloud_healthcare.sehacloud_healthcare.page.ot_appointment_wait_list.ot_appointment_wait_list.get_appointments",
			args: {date: date, practitioner: me.practitioner.get_value(), status_values: me.status.get_value(),
				patient: me.patient.get_value(), company: me.company.get_value(), service_unit:me.service_unit.get_value()
			},
			callback: function (r) {
				me.wrapper.empty();
				if(r.message.length > 0){
					me.make_list(r.message)
				}else{
					let msg_html = '<p class="text-muted" style="padding: 15px;">No '+ me.status.get_value() +' appointments  found for the day</p>';
					$(msg_html).appendTo(me.wrapper);
				}
			},
			freeze: true
		});
	},
	make_list: function (appointments) {
		var me = this;
		// $(`<div class="col-sm-12" style='background-color:#D3D3D3;'><div class="col-sm-12"><b>
		// 	<div class="col-sm-2">Patient</div>
		// 	<div class="col-sm-1">Practitioner</div>
		// 	<div class="col-sm-1">Time</div>
		// 	<div class="col-sm-1">Status</div>
		// 	<div class="col-sm-1">Procedure</div>
		// 	<div class="col-sm-2">Appointment</div>
		// 	<div class="col-sm-3"></div>
		// 	<div class="col-sm-1">Orders</div>
		// 	</b>
		// </div></div>`).appendTo(me.wrapper)
		$.each(appointments, function(i, appointment){
			$(frappe.render_template("ot_appointment_wait_list", {data: appointment, encounter: appointment.encounter})).appendTo(me.wrapper);
		});
		this.wrapper.find(".record-vitals").on("click", function() {
			me.create_vitals($(this).attr("data-patient"));
		});
		this.wrapper.find(".attend").on("click", function() {
			// $(this).closest(".app-listing").hide();
			me.create_encounter($(this).attr("data-name"))
		});
		this.wrapper.find(".cancel").on("click", function() {
			me.update_appointment_status($(this).attr("data-name"), me, "Cancelled");
		});
		this.wrapper.find(".arrived").on("click", function() {
			me.update_appointment_status($(this).attr("data-name"), me, "Arrived");
		});
		this.wrapper.find(".update-encounter").on("click", function() {
			me.update_encounter($(this).attr("data-name"));
		});
		this.wrapper.find(".reschedule").on("click", function() {
			me.check_availability($(this).attr("data-department"), $(this).attr("data-practitioner"), $(this).attr("data-name"), $(this).attr("data-appointment-date"), me);
		});
		this.wrapper.find(".start-procedure").on("click", function() {
			me.create_procedure($(this).attr("data-name"))
		});
		this.wrapper.find(".update-procedure").on("click", function() {
			me.update_procedure($(this).attr("data-name"))
		});
		this.wrapper.find(".make-invoice").on("click", function() {
			me.make_invoice($(this).attr("data-name"), me);
		});
		this.wrapper.find(".view-invoice").on("click", function() {
			me.view_invoice($(this).attr("data-name"), me);
		});
		this.wrapper.find(".open").on("click", function() {
			me.update_appointment_status($(this).attr("data-name"), me, "Open");
		});
	},
	update_appointment_status: function (appointment, me, status) {
		frappe.call({
			method: "erpnext.healthcare.doctype.patient_appointment.patient_appointment.update_status",
			args: {appointment_id: appointment, status: status},
			callback: function (r) {
				if(!r.exc){
					me.get_appointments();
				}
			}
		});
	},
	create_encounter: function (appointment) {
		frappe.call({
			method: "sehacloud_healthcare.sehacloud_healthcare.page.patient_wait_list.patient_wait_list.create_encounter",
			args: {appointment: appointment},
			callback: function (r) {
				var doclist = frappe.model.sync(r.message);
				frappe.set_route("Form", doclist[0].doctype, doclist[0].name);
			}
		});
	},
	update_encounter: function (appointment) {
		frappe.call({
			method: "sehacloud_healthcare.sehacloud_healthcare.page.patient_wait_list.patient_wait_list.update_encounter",
			args: {appointment: appointment},
			callback: function (r) {
				if(r.message){
					var doclist = frappe.model.sync(r.message);
					frappe.set_route("Form", doclist[0].doctype, doclist[0].name);
				}
			}
		});
	},
	create_vitals:function (patient) {
		frappe.route_options = {
			"patient": patient,
		}
		frappe.new_doc("Vital Signs")
		// window.open('_blank');
	},
	create_procedure: function(appointment){
		frappe.call({
			method:"sehacloud_healthcare.sehacloud_healthcare.page.patient_wait_list.patient_wait_list.create_procedure",
			args: {appointment: appointment},
			callback: function(data){
				if(!data.exc){
					var doclist = frappe.model.sync(data.message);
					frappe.set_route("Form", doclist[0].doctype, doclist[0].name);
				}
			},
			freeze: true
		});
	},
	update_procedure: function(appointment){
		frappe.call({
			method:"sehacloud_healthcare.sehacloud_healthcare.page.patient_wait_list.patient_wait_list.update_procedure",
			args: {appointment: appointment},
			callback: function(data){
				if(!data.exc && data.message){
					var doclist = frappe.model.sync(data.message);
					frappe.set_route("Form", doclist[0].doctype, doclist[0].name);
				}
			},
			freeze: true
		});
	},
	make_invoice: function(appointment){
		frappe.call({
			method:"sehacloud_healthcare.sehacloud_healthcare.page.patient_wait_list.patient_wait_list.invoice_appointment",
			args: {appointment_id: appointment},
			callback: function(data){
				if(!data.exc){
					var doclist = frappe.model.sync(data.message);
					frappe.set_route("Form", doclist[0].doctype, doclist[0].name);
				}
			}
		});
	},
	view_invoice: function(appointment){
		frappe.call({
			method:"sehacloud_healthcare.sehacloud_healthcare.page.patient_wait_list.patient_wait_list.view_invoice",
			args: {appointment_id: appointment},
			callback: function(data){
				if(!data.exc && data.message){
					var doclist = frappe.model.sync(data.message);
					frappe.set_route("Form", doclist[0].doctype, doclist[0].name);
				}
			}
		});
	},
	check_availability:function(department, practitioner, appointment, appointment_date, me) {
		check_and_set_availability(department, practitioner, appointment, appointment_date, me)
	},
	make_toolbar: function () {
		var me = this;
		var selected_practitioner = '';
		var selected_patient = '';
		var selected_service_unit = '';
		var selected_company = '';
		this.practitioner = this.page.add_field({label: __("Healthcare Practitioner"), fieldtype: 'Link',
		options: 'Healthcare Practitioner',
		change: function(){
			if(me.practitioner.get_value() && me.practitioner.get_value() != selected_practitioner){
				selected_practitioner = me.practitioner.get_value();
				me.get_appointments();
			}
				}
		})
		if(frappe.user.has_role("Physician")){
			frappe.db.get_value("Healthcare Practitioner", {user_id: frappe.session.user}, 'name', function(r){
				if(r){
					me.practitioner.set_value(r.name)
				}
			});
		}
		this.date = this.page.add_date(__("Date"), frappe.datetime.get_today())
		this.date.on('change', function(){
			me.get_appointments();
		});
		this.status = this.page.add_field({label: __("Status"), fieldtype: 'MultiSelect', options: 'Open\nScheduled\nCancelled\nClosed\nPending\nArrived'})
		this.status.$input.on('change', function(){
			me.get_appointments();
		});
		this.patient = this.page.add_field({label: __("Patient"), fieldtype: 'Link',
		options: 'Patient',
		change: function(){
			if(me.patient.get_value() && me.patient.get_value() != selected_patient){
				selected_patient = me.patient.get_value();
				me.get_appointments();
			}
		}
		});
		this.service_unit = this.page.add_field({label: __("Healthcare Service Unit"), fieldtype: 'Link',
			options: 'Healthcare Service Unit',
			get_query: function(){
				return {
					filters: {
						"is_group": false,
						"allow_appointments": true
					}
				};
			},
			change: function(){
				if(me.service_unit.get_value() && me.service_unit.get_value() != selected_service_unit){
					selected_service_unit = me.service_unit.get_value();
					me.get_appointments();
				}
				else if(!me.service_unit.get_value()){
					selected_service_unit = ''
					me.get_appointments();
				}
			}
		});
		this.company = this.page.add_field({label: __("Company"), fieldtype: 'Link',
		options: 'Company',
		change: function(){
			if(me.company.get_value() && me.company.get_value() != selected_company){
				selected_company = me.company.get_value();
				me.get_appointments();
			}
		}
		});
		this.page.set_primary_action(__("Refresh"), function() {
			me.get_appointments();
		}, "fa fa-refresh");
	},
	make_page: function() {
		if (this.page)
			return;

		frappe.ui.make_app_page({
			parent: this.parent,
			title: __('OT Appointment-Wait list'),
			single_column: true
		});
		this.page = this.parent.page;
		this.wrapper = $('<div></div>').appendTo(this.page.main);
	},
});

var check_and_set_availability = function(department, practitioner, appointment, appointment_date, me) {
	var selected_slot = null;
	var service_unit = null;
	var duration = null;

	show_availability()

	function show_empty_state(practitioner, appointment_date) {
		frappe.msgprint({
			title: __('Not Available'),
			message: __("Healthcare Practitioner {0} not available on {1}", [practitioner.bold(), appointment_date.bold()]),
			indicator: 'red'
		});
	}

	function show_availability(data) {
		let selected_practitioner = '';
		var d = new frappe.ui.Dialog({
			title: __("Available slots"),
			fields: [
				{ fieldtype: 'Link', options: 'Medical Department', reqd:1, fieldname: 'department', label: 'Medical Department'},
				{ fieldtype: 'Column Break'},
				{ fieldtype: 'Link', options: 'Healthcare Practitioner', reqd:1, fieldname: 'practitioner', label: 'Healthcare Practitioner'},
				{ fieldtype: 'Column Break'},
				{ fieldtype: 'Date', reqd:1, fieldname: 'appointment_date', label: 'Date'},
				{ fieldtype: 'Section Break'},
				{ fieldtype: 'HTML', fieldname: 'available_slots'}
			],
			primary_action_label: __("Book"),
			primary_action: function() {
				frappe.call({
					method: "sehacloud_healthcare.sehacloud_healthcare.page.patient_wait_list.patient_wait_list.reschedule_appointment",
					args:{
						appointment_details: {
							practitioner: d.get_value('practitioner'),
							department: d.get_value('department'),
							appointment_date: d.get_value('appointment_date'),
							appointment_time: selected_slot,
							service_unit: service_unit || '',
							duration: duration
						},
						appointment_id: appointment
					},
					callback: function(r) {
						if(!r.exc){
							me.get_appointments();
						}
					},
					freeze: true
				})
				d.hide();
				d.get_primary_btn().attr('disabled', true);
			}
		});

		d.set_values({
			'department': department,
			'practitioner': practitioner,
			'appointment_date': appointment_date
		});

		d.fields_dict["department"].df.onchange = () => {
			d.set_values({
				'practitioner': ''
			});
			var department = d.get_value('department');
			if(department){
				d.fields_dict.practitioner.get_query = function() {
					return {
						filters: {
							"department": department
						}
					}
				}
			}
		}

		// disable dialog action initially
		d.get_primary_btn().attr('disabled', true);

		// Field Change Handler

		var fd = d.fields_dict;

		d.fields_dict["appointment_date"].df.onchange = () => {
			show_slots(d, fd);
		}
		d.fields_dict["practitioner"].df.onchange = () => {
			if(d.get_value('practitioner') && d.get_value('practitioner') != selected_practitioner){
				selected_practitioner = d.get_value('practitioner');
				show_slots(d, fd);
			}
		}
		d.show();
	}

	function show_slots(d, fd) {
		if (d.get_value('appointment_date') && d.get_value('practitioner')){
			fd.available_slots.html("")
			frappe.call({
				method: 'erpnext.healthcare.doctype.patient_appointment.patient_appointment.get_availability_data',
				args: {
					practitioner: d.get_value('practitioner'),
					date: d.get_value('appointment_date')
				},
				callback: (r) => {
					var data = r.message;
					if(data.slot_details.length > 0) {
						var $wrapper = d.fields_dict.available_slots.$wrapper;

						// make buttons for each slot
						var slot_details = data.slot_details;
						var slot_html = "";
						for (let i = 0; i < slot_details.length; i++) {
							slot_html = slot_html + `<br/>` + slot_details[i].avail_slot.map(slot => {
								let disabled = '';
								let start_str = slot.from_time;
								let slot_start_time = moment(slot.from_time, 'HH:mm:ss');
								let slot_to_time = moment(slot.to_time, 'HH:mm:ss');
								let interval = (slot_to_time - slot_start_time)/60000 | 0;
								//iterate in all booked appointments, update the start time and duration
								slot_details[i].appointments.forEach(function(booked) {
									let booked_moment = moment(booked.appointment_time, 'HH:mm:ss');
									let end_time = booked_moment.clone().add(booked.duration, 'minutes');
									// Deal with 0 duration appointments
									if(booked_moment.isSame(slot_start_time) || booked_moment.isBetween(slot_start_time, slot_to_time)){
										if(booked.duration == 0){
											disabled = 'disabled="disabled"';
											return false;
										}
									}
									if(end_time.isSame(slot_start_time) || end_time.isBetween(slot_start_time, slot_to_time)){
										start_str = end_time.format("HH:mm")+":00";
										interval = (slot_to_time - end_time)/60000 | 0;
										return false;
									}
									// Check for overlaps considering appointment duration
									if(slot_start_time.isBefore(end_time) && slot_to_time.isAfter(booked_moment)){
										// There is an overlap
										disabled = 'disabled="disabled"';
										return false;
									}
								});
								return `<button class="btn btn-default"
									data-name=${start_str}
									data-duration=${interval}
									data-service-unit="${slot_details[i].service_unit || ''}"
									style="margin: 0 10px 10px 0; width: 72px;" ${disabled}>
									${start_str.substring(0, start_str.length - 3)}
								</button>`;
							}).join("");
							slot_html = slot_html + `<br/>`;
						}

						$wrapper
							.css('margin-bottom', 0)
							.addClass('text-center')
							.html(slot_html);

						// blue button when clicked
						$wrapper.on('click', 'button', function() {
							var $btn = $(this);
							$wrapper.find('button').removeClass('btn-primary active');
							$btn.addClass('btn-primary active');
							selected_slot = $btn.attr('data-name');
							service_unit = $btn.attr('data-service-unit')
							duration = $btn.attr('data-duration')
							// enable dialog action
							d.get_primary_btn().attr('disabled', null);
						});

					}else {
						//	fd.available_slots.html("Please select a valid date.".bold())
						show_empty_state(d.get_value('practitioner'), d.get_value('appointment_date'));
					}
				},
				freeze: true,
				freeze_message: __("Fetching records......")
			});
		}else{
			fd.available_slots.html("Appointment date and Healthcare Practitioner are Mandatory".bold())
		}
	}
}
