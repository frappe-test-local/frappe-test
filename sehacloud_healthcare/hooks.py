# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from . import __version__ as app_version

app_name = "sehacloud_healthcare"
app_title = "Sehacloud Healthcare"
app_publisher = "SehaCloud"
app_description = "Healthcare customization for SehaCloud"
app_icon = "octicon octicon-file-directory"
app_color = "grey"
app_email = "info@sehacloud.com"
app_license = "MIT"

app_logo_url = '/assets/sehacloud_healthcare/img/nixpend.PNG'
before_install = "sehacloud_healthcare.before_install"

fixtures = [{"dt":"Custom Field", "filters": [["fieldname", "in",
("source", "medical_code", "medical_code_standard", "supplier",
"source", "referring_practitioner", "clinical_procedures", "first_name", "middle_name", "last_name",
"sb_patient_vitals_summary", "sehacloud_patient_vitals", "sb_second", "schc_btn_transfer", "schc_sb_radiology_details",
"schc_requested_procedure_id", "schc_requested_procedure_description", "schc_requested_procedure_priority", "schc_scheduled_ae_station",
"schc_scheduled_procedure_id", "schc_scheduled_procedure_description", "schc_scheduled_procedure_location",
"schc_scheduled_procedure_premedication", "schc_scheduled_procedure_contrast_agent", "schc_cb_radiology_details",
"schc_procedure_code_value", "schc_procedure_code_meaning", "schc_procedure_code_scheme", "schc_procedure_code_scheme_version",
"schc_protocol_code_value", "schc_protocol_code_meaning", "schc_protocol_code_scheme_designator", "schc_protocol_code_scheme",
"schc_protocol_code_scheme_version", "study_instance_uid", "schc_package_assignment", "schc_doctor_fee", "doctor_fee", "total_procedure_rate",
"schc_patient_selling_rate", "schc_encounter", "schc_package_assignment_name", "nationality",
"actual_qty", "sch_invoice_type", "schc_amount_in_words_arabic", "sch_sales_invoice", "sch_patient_name", "sch_patient",
"schc_total_valuation_rate", "schc_total_add_valuation_rate", "sch_insurance_company_name", "sch_plan_name",
"sch_insurance_card", "schc_per_purchase_invoice_taxes", "manufacturer_code", "item_orders", "item_prescription", "schc_package_item", "sch_healthcare_practitioner_level",
"schc_package_item_name", "sch_mobile", "has_drug_prescription", "has_item_prescription", "has_lab_prescription", "has_procedure_prescription", "has_radiology_prescription", "nid_or_iqama_id")]]},
"Custom Script", {"dt":"Print Format", "filters": [["doc_type", "in", ("Patient", "Inpatient Record", "Delivery Note", "Sales Invoice")]]}]

doc_events = {
	"Sales Invoice": {
		"on_submit": "sehacloud_healthcare.sehacloud_healthcare.utils.manage_sales_invoice_submit_cancel",
		"on_cancel": "sehacloud_healthcare.sehacloud_healthcare.utils.manage_sales_invoice_submit_cancel",
		"before_cancel": "sehacloud_healthcare.sehacloud_healthcare.healthcare_service_to_invoice.cancel_je_for_package",
		"validate": "sehacloud_healthcare.sehacloud_healthcare.utils.set_invoice_amount_in_word"
	},
	"Patient Appointment": {
		"after_insert": "sehacloud_healthcare.sehacloud_healthcare.utils.appointment_hook_action",
		"on_update": "sehacloud_healthcare.sehacloud_healthcare.utils.create_ini_file_for_radiology_appointments",
		"validate": "sehacloud_healthcare.sehacloud_healthcare.utils.validate_package_assignment"
	},
	"Clinical Procedure": {
		"on_update_after_submit": "sehacloud_healthcare.sehacloud_healthcare.utils.clinical_procedure_hook_action",
		"after_insert": "sehacloud_healthcare.sehacloud_healthcare.utils.after_insert_clinical_procedure"
	},
	"Lab Test": {
		"on_submit": "sehacloud_healthcare.sehacloud_healthcare.utils.auto_invoice_hook_action"
	},
	"Radiology Examination": {
		"on_submit": "sehacloud_healthcare.sehacloud_healthcare.utils.auto_invoice_hook_action"
	},
	"Patient Encounter": {
		"on_submit": "sehacloud_healthcare.sehacloud_healthcare.utils.encounter_on_submit_cancel",
		"on_update": "sehacloud_healthcare.sehacloud_healthcare.utils.set_encounter_details_in_item_prescription",
		"on_cancel": "sehacloud_healthcare.sehacloud_healthcare.utils.encounter_on_submit_cancel",
		"validate": "sehacloud_healthcare.sehacloud_healthcare.utils.encounter_validate"
	},
	"Insurance Approval": {
		"after_insert": "sehacloud_healthcare.sehacloud_healthcare.utils.update_item_prescription_details_in_insurance_approval",
	}
}

scheduler_events = {
	"all": [
		"erpnext.healthcare.doctype.patient_appointment.patient_appointment.remind_appointment"
	],
	"daily": [
		"erpnext.healthcare.doctype.patient_appointment.patient_appointment.set_open_appointments",
		"erpnext.healthcare.doctype.patient_appointment.patient_appointment.set_pending_appointments",
		"erpnext.sehacloud_healthcare.doctype.healthcare_package_assignment.healthcare_package_assignment.expiring_package"
	]
}

standard_queries = {
	"Healthcare Practitioner": "sehacloud_healthcare.queries.get_practitioner_list"
}

website_context = {
	"splash_image": "/assets/sehacloud_healthcare/img/nixpend.PNG"
}

# Includes in <head>
# ------------------

# include js, css files in header of desk.html
# app_include_css = "/assets/sehacloud_healthcare/css/sehacloud_healthcare.css"
# app_include_js = "/assets/sehacloud_healthcare/js/sehacloud_healthcare.js"

# include js, css files in header of web template
# web_include_css = "/assets/sehacloud_healthcare/css/sehacloud_healthcare.css"
# web_include_js = "/assets/sehacloud_healthcare/js/sehacloud_healthcare.js"

# include js in page
# page_js = {"page" : "public/js/file.js"}

# include js in doctype views
# doctype_js = {"doctype" : "public/js/doctype.js"}
# doctype_list_js = {"doctype" : "public/js/doctype_list.js"}
# doctype_tree_js = {"doctype" : "public/js/doctype_tree.js"}
# doctype_calendar_js = {"doctype" : "public/js/doctype_calendar.js"}

# Home Pages
# ----------

# application home page (will override Website Settings)
# home_page = "login"

# website user home page (by Role)
# role_home_page = {
#	"Role": "home_page"
# }

# Website user home page (by function)
# get_website_user_home_page = "sehacloud_healthcare.utils.get_home_page"

# Generators
# ----------

# automatically create page for each record of this doctype
# website_generators = ["Web Page"]

# Installation
# ------------

# before_install = "sehacloud_healthcare.install.before_install"
# after_install = "sehacloud_healthcare.install.after_install"

# Desk Notifications
# ------------------
# See frappe.core.notifications.get_notification_config

# notification_config = "sehacloud_healthcare.notifications.get_notification_config"

# Permissions
# -----------
# Permissions evaluated in scripted ways

# permission_query_conditions = {
# 	"Event": "frappe.desk.doctype.event.event.get_permission_query_conditions",
# }
#
# has_permission = {
# 	"Event": "frappe.desk.doctype.event.event.has_permission",
# }

# Document Events
# ---------------
# Hook on document methods and events

# doc_events = {
# 	"*": {
# 		"on_update": "method",
# 		"on_cancel": "method",
# 		"on_trash": "method"
#	}
# }

# Scheduled Tasks
# ---------------

# scheduler_events = {
# 	"all": [
# 		"sehacloud_healthcare.tasks.all"
# 	],
# 	"daily": [
# 		"sehacloud_healthcare.tasks.daily"
# 	],
# 	"hourly": [
# 		"sehacloud_healthcare.tasks.hourly"
# 	],
# 	"weekly": [
# 		"sehacloud_healthcare.tasks.weekly"
# 	]
# 	"monthly": [
# 		"sehacloud_healthcare.tasks.monthly"
# 	]
# }

# Testing
# -------

# before_tests = "sehacloud_healthcare.install.before_tests"

# Overriding Whitelisted Methods
# ------------------------------
#
# override_whitelisted_methods = {
# 	"frappe.desk.doctype.event.event.get_events": "sehacloud_healthcare.event.get_events"
# }
