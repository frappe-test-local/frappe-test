from __future__ import unicode_literals
import frappe

def execute():
	if "Healthcare" not in frappe.get_active_domains():
		return

	frappe.reload_doc('healthcare', 'doctype', "patient_encounter")

	patient_encounter_list = frappe.get_list("Patient Encounter",  fields=["name"])
	for d in patient_encounter_list:
		doc = frappe.get_doc("Patient Encounter", d.name)
		if doc.appointment and doc.docstatus < 2:
			appointment_status=frappe.db.get_value("Patient Appointment", doc.appointment, 'status')
			if appointment_status == 'In Progress' or appointment_status == 'Closed':
				has_drug_prescription = True if doc.drug_prescription else False
				has_item_prescription = True if doc.item_prescription else False
				has_lab_prescription = True if doc.lab_test_prescription else False
				has_procedure_prescription = True if doc.procedure_prescription else False
				has_radiology_prescription = True if doc.radiology_procedure_prescription else False
				frappe.db.set_value("Patient Encounter", d.name, 'has_drug_prescription', has_drug_prescription)
				frappe.db.set_value("Patient Encounter", d.name, 'has_item_prescription', has_item_prescription)
				frappe.db.set_value("Patient Encounter", d.name, 'has_lab_prescription', has_lab_prescription)
				frappe.db.set_value("Patient Encounter", d.name, 'has_procedure_prescription', has_procedure_prescription)
				frappe.db.set_value("Patient Encounter", d.name, 'has_radiology_prescription', has_radiology_prescription)
