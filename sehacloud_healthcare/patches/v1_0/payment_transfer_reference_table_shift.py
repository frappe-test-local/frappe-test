from __future__ import unicode_literals
import frappe

def execute():
	if "Healthcare" not in frappe.get_active_domains():
		return

	doctypes = ['healthcare_package_assignment', 'package_payment_reference', 'package_account_transfer_reference']
	for doctype in doctypes:
		frappe.reload_doc('sehacloud_healthcare', 'doctype', doctype)

	package_assignment_list = frappe.get_list("Healthcare Package Assignment", filters={"docstatus": 1}, fields=["name"])
	for d in package_assignment_list:
		package_assignment = frappe.get_doc("Healthcare Package Assignment", d.name)
		if package_assignment.package_payment_reference and package_assignment.package_account_transfer_reference:
			for p in package_assignment.package_payment_reference:
				if p.package_payment_reference:
					posting_date, total_credit = frappe.db.get_value('Journal Entry', p.package_payment_reference, ['posting_date', 'total_credit'])
					if total_credit:
						frappe.db.set_value('Package Payment Reference', p.name, "paid_amount", total_credit)
					transfer_jv = None;
					for t in package_assignment.package_account_transfer_reference:
						transfer_jv = frappe.db.get_value('Journal Entry', {'posting_date': posting_date, 'total_credit': total_credit, 'name':t.package_account_transfer_reference}, 'name')
						if transfer_jv:
							frappe.db.set_value('Package Payment Reference', p.name, "package_transfer_reference", transfer_jv)
