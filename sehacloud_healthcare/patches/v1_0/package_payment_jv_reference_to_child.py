from __future__ import unicode_literals
import frappe

def execute():
	if "Healthcare" not in frappe.get_active_domains():
		return

	doctypes = ['healthcare_package_assignment', 'package_payment_reference', 'package_account_transfer_reference']
	for doctype in doctypes:
		frappe.reload_doc('sehacloud_healthcare', 'doctype', doctype)

	package_assignment_list = frappe.get_list("Healthcare Package Assignment", filters={"docstatus": 1}, fields=["name"])
	for d in package_assignment_list:
		package_assignment = frappe.get_doc("Healthcare Package Assignment", d.name)
		if frappe.db.has_column("Healthcare Package Assignment", "payment_reference") and package_assignment.payment_reference:
			payment_reference = package_assignment.append("package_payment_reference")
			payment_reference.package_payment_reference = package_assignment.payment_reference
		if frappe.db.has_column("Healthcare Package Assignment", "account_transfer_reference") and package_assignment.account_transfer_reference:
			account_transfer_reference = package_assignment.append("package_account_transfer_reference")
			account_transfer_reference.package_account_transfer_reference = package_assignment.account_transfer_reference
		package_assignment.save(ignore_permissions=True)
