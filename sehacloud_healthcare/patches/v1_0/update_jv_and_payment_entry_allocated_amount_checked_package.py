from __future__ import unicode_literals
import frappe
from erpnext.accounts.party import get_party_account
from frappe.utils import nowdate, flt

def execute():
	if "Healthcare" not in frappe.get_active_domains():
		return

	doctypes = ['payment_entry_reference', 'journal_entry', 'payment_entry']
	for doctype in doctypes:
		frappe.reload_doc('accounts', 'doctype', doctype)
	frappe.reload_doc('sehacloud_healthcare', 'doctype', 'healthcare_package_assignment')

	package_list  = frappe.get_list("Healthcare Package Assignment", filters= {'allocate_patient_credit_balance': 1, 'docstatus': 1}, fields=['name','patient_credit_balance_payment'])
	for package in package_list:
		if package.patient_credit_balance_payment:
			print('package:'+ package.name)
			print('journal entry:' + package.patient_credit_balance_payment)

			package_doc = frappe.get_doc("Healthcare Package Assignment", package.name)
			package_jv = package.patient_credit_balance_payment
			payment_entry_list = frappe.get_list('Payment Entry Reference', filters= {'reference_doctype': 'Journal Entry', 'reference_name': package_jv}, fields=['parent'])

			journal_entry_cancel = frappe.get_doc('Journal Entry', package_jv)
			paid_amount = journal_entry_cancel.total_debit
			posting_date = journal_entry_cancel.posting_date
			if package_doc.patient_credit_balance_payment:
				frappe.db.set_value(package_doc.doctype, package_doc.name, "patient_credit_balance_payment","")
			if journal_entry_cancel.docstatus != 2:
				journal_entry_cancel.cancel()
			journal_entry = frappe.new_doc('Journal Entry')
			if package_jv:
				journal_entry.amended_from = package_jv
			party_type = "Customer"
			party_account = get_party_account(party_type, package_doc.customer, package_doc.company)
			journal_entry.posting_date =  posting_date
			accounts = []
			accounts.append({
				"account": package_doc.package_advance,
				"credit_in_account_currency": paid_amount
			})
			accounts.append({
				"account": party_account,
				"debit_in_account_currency": paid_amount,
				"party_type": "Customer",
				"party": package_doc.customer
			})
			journal_entry.set("accounts", accounts)
			journal_entry.save(ignore_permissions = True)
			journal_entry.submit()

			payment_entry = ''
			reconciled_amount = 0
			for payment in payment_entry_list:
				payment_entry = payment.parent
				print('payment_entry:' + payment_entry)
				from erpnext.accounts.utils import update_reference_in_payment_entry
				payment_entry = frappe.get_doc("Payment Entry", payment_entry)
				unallocated_amount = payment_entry.unallocated_amount if payment_entry.unallocated_amount <= paid_amount else paid_amount
				if reconciled_amount > 0 and reconciled_amount < paid_amount:
					unallocated_amount = payment_entry.unallocated_amount if payment_entry.unallocated_amount <= paid_amount - reconciled_amount else paid_amount - reconciled_amount
					d = get_payment_details(package_doc, payment_entry, unallocated_amount, journal_entry)
					update_reference_in_payment_entry(d, payment_entry)
				elif reconciled_amount <= 0:
					d = get_payment_details(package_doc, payment_entry, unallocated_amount, journal_entry)
					update_reference_in_payment_entry(d, payment_entry)
				reconciled_amount = reconciled_amount + unallocated_amount
			print('new journal_entry :' + journal_entry.name)
			frappe.db.set_value("Healthcare Package Assignment", package_doc.name, 'patient_credit_balance_payment', journal_entry.name)

def get_payment_details(package_doc, unallocated_entry, allocated_amount, transfer_jv):
	party_type = "Customer"
	party_account = get_party_account(party_type, package_doc.customer, package_doc.company)
	party = package_doc.customer
	voucher_detail_no = ''
	journal_entry = frappe.get_doc("Journal Entry", transfer_jv.name)
	return frappe._dict({
		'voucher_type': 'Payment Entry',
		'voucher_no' : unallocated_entry.name,
		'voucher_detail_no' : voucher_detail_no,
		'against_voucher_type' : journal_entry.voucher_type,
		'against_voucher'  : journal_entry.name,
		'account' : party_account,
		'party_type': party_type,
		'party': party,
		'is_advance' : '',
		'dr_or_cr' : 'credit_in_account_currency',
		'unadjusted_amount' : flt(unallocated_entry.unallocated_amount),
		'allocated_amount' : flt(allocated_amount),
		'grand_total': journal_entry.total_debit,
		'outstanding_amount': journal_entry.total_debit
	})
