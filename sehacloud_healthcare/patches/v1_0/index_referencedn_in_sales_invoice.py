from __future__ import unicode_literals
import frappe

def execute():
	if "Healthcare" not in frappe.get_active_domains():
		return

	if frappe.get_meta('Sales Invoice Item').has_field("reference_dn"):
		frappe.db.sql("ALTER TABLE `tabSales Invoice Item` ADD INDEX (reference_dn)")
