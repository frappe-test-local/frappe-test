from __future__ import unicode_literals
import frappe

def execute():
	if "Healthcare" not in frappe.get_active_domains():
		return

	frappe.reload_doc('healthcare', 'doctype', "patient")

	patient_nid_list = frappe.get_list("Patient", fields=["name"])
	for d in patient_nid_list:
		patient = frappe.get_doc("Patient", d.name)
		if frappe.db.has_column("Patient", "nid") and patient.nid:
			patient.patient_identification = patient.nid
		patient.save(ignore_permissions=True)
