from __future__ import unicode_literals
from frappe import _

def get_data():

	return [
		{
			"label": _("Patient"),
			"items": [
				{
					"type": "doctype",
					"name": "Patient",
					"label": _("New Registration"),
					"route": "Form/Patient/New Patient"
				},
				{
					"type": "report",
					"name": "Patient Status Report",
					"is_query_report": True,
					"label": _("Patient Status Report"),
				}
			]
		},
		{
			"label": _("Outpatient"),
			"items": [
				{
					"type": "page",
					"name": "patient-appointment-desk",
					"label": _("Patient Appointment Desk"),
				},
				{
					"type": "page",
					"name": "patient-wait-list",
					"label": _("Patient Appointment List"),
				},
				{
					"type": "report",
					"name": "Appointment Report",
					"is_query_report": True,
					"label": _("Appointment Report"),
				}
			]
		},
		{
			"label": _("Inpatient"),
			"items": [
				{
					"type": "page",
					"name": "bed_management",
					"label": _("Bed Management"),
				},
			]
		},
		{
			"label": _("Laboratory"),
			"items": [
				{
					"type": "doctype",
					"name": "Patient",
					"label": _("Patient Registration"),
					"route": "Form/Patient/New Patient"
				},
				{
					"type": "doctype",
					"name": "Lab Test",
					"label": _("Lab Order List"),
					"route_options": {
						"docstatus": 0
					}
				},
				{
					"type": "doctype",
					"name": "Lab Test",
					"label": _("Lab Result"),
					"route_options": {
						"docstatus": 1
					}
				},
				{
					"type": "doctype",
					"name": "Sales Invoice",
					"label": _("Billing")
				}
			]
		},
		{
			"label": _("Radiology"),
			"items": [
				{
					"type": "page",
					"name": "radiology-appointment",
					"label": _("Radiology Appointment"),
				}
			]
		},
		{
			"label": _("Operation Theature"),
			"items": [
				{
					"type": "page",
					"name": "ot_management",
					"label": _("OT Management"),
				},
				{
					"type": "page",
					"name": "ot-appointment-desk",
					"label": _("OT Appointment Desk"),
				},
			]
		},
		{
			"label": _("Billing and Insurance"),
			"items": [
				{
					"type": "doctype",
					"name": "Sales Invoice",
					"label": _("Sales Invoice")
				},
				{
					"type": "report",
					"name": "Invoice Report",
					"is_query_report": True,
					"label": _("Invoice Report"),
				},
				{
					"type": "report",
					"name": "Collection Report",
					"is_query_report": True,
					"label": _("Collection Report"),
				}
			]
		},
		{
			"label": _("Pharmacy"),
			"items": [
				{
					"type": "page",
					"name": "delivery_que",
					"label": _("Delivery Desk"),
				},
				{
					"type": "page",
					"name": "pos",
					"label": _("Pharmacy POS"),
					"description": _("Pharmacy Point of Sale")
				},
				{
					"type": "doctype",
					"name": "Sales Invoice",
					"label": _("Billing")
				},
				{
					"type": "report",
					"is_query_report": True,
					"name": "Stock Ledger",
					"label": "Pharmacy Stock Ledger",
					"doctype": "Stock Ledger Entry",
					"route_options": {
						"item_group": "Drug"
					}
				},
				{
					"type": "report",
					"is_query_report": True,
					"name": "Stock Balance",
					"label": "Pharmacy Stock Balance",
					"doctype": "Stock Ledger Entry",
					"route_options": {
						"item_group": "Drug"
					}
				},
				{
					"type": "report",
					"is_query_report": True,
					"label": "Pharmacy Projected Qty",
					"name": "Stock Projected Qty",
					"doctype": "Item",
					"route_options": {
						"item_group": "Drug"
					}
				},
				{
					"type": "page",
					"name": "stock-balance",
					"label": _("Pharmacy Stock Summary"),
					"route_options": {
						"item_group": "Drug"
					}
				},
				{
					"type": "report",
					"is_query_report": True,
					"name": "Stock Ageing",
					"label": "Pharmacy Stock Ageing",
					"doctype": "Item",
				},
				{
					"type": "report",
					"is_query_report": True,
					"name": "Item Price Stock",
					"label": "Medication Price Stock",
					"doctype": "Item",
				},
				{
					"type": "doctype",
					"name": "Item",
					"label": _("Item"),
					"route_options": {
						"item_group": "Drug"
					}
				},
			]
		},
		{
			"label": _("Practitioner Management"),
			"items": [
				{
					"type": "report",
					"name": "Utilization Report",
					"is_query_report": True,
					"label": _("Utilization Report"),
				}
			]
		},
		{
			"label": _("Facility Management"),
			"items": []
		},
		{
			"label": _("Medical Code"),
			"items": []
		},
		{
			"label": _("Setup"),
			"items": []
		},
		{
			"label": _("Additional Setup"),
			"items": [
				{
					"type": "doctype",
					"name": "Radiology Integration",
					"label": _("Radiology Integration")
				},
			]
		},
		{
			"label": _("Package"),
			"items": [
				{
					"type": "doctype",
					"name": "Healthcare Package",
					"label": _("Healthcare Package")
				},
				{
					"type": "doctype",
					"name": "Healthcare Package Assignment",
					"label": _("Healthcare Package Assignment")
				},
				{
					"type": "doctype",
					"name": "Healthcare Practitioner Level",
					"label": _("Healthcare Practitioner Level")
				}
			]
		}
	]
